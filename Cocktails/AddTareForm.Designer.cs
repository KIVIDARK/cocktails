﻿namespace Cocktails
{
	partial class AddTareForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.textNameTare = new System.Windows.Forms.TextBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.textPriceTare = new System.Windows.Forms.NumericUpDown();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.textQuantityTare = new System.Windows.Forms.NumericUpDown();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.textReloadTare = new System.Windows.Forms.NumericUpDown();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.cancel = new System.Windows.Forms.Button();
			this.importCSV = new System.Windows.Forms.Button();
			this.addTare = new System.Windows.Forms.Button();
			this.tableLayoutPanel1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textPriceTare)).BeginInit();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textQuantityTare)).BeginInit();
			this.groupBox4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textReloadTare)).BeginInit();
			this.tableLayoutPanel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.groupBox4, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 4);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 5;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(412, 244);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.textNameTare);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(406, 45);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Тип тары (Название)";
			// 
			// textNameTare
			// 
			this.textNameTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textNameTare.Location = new System.Drawing.Point(3, 16);
			this.textNameTare.Name = "textNameTare";
			this.textNameTare.Size = new System.Drawing.Size(400, 20);
			this.textNameTare.TabIndex = 0;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.textPriceTare);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(3, 54);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(406, 45);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Цена";
			// 
			// textPriceTare
			// 
			this.textPriceTare.DecimalPlaces = 2;
			this.textPriceTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textPriceTare.Location = new System.Drawing.Point(3, 16);
			this.textPriceTare.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
			this.textPriceTare.Name = "textPriceTare";
			this.textPriceTare.Size = new System.Drawing.Size(400, 20);
			this.textPriceTare.TabIndex = 1;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.textQuantityTare);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox3.Location = new System.Drawing.Point(3, 105);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(406, 45);
			this.groupBox3.TabIndex = 2;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Количество";
			// 
			// textQuantityTare
			// 
			this.textQuantityTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textQuantityTare.Location = new System.Drawing.Point(3, 16);
			this.textQuantityTare.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
			this.textQuantityTare.Name = "textQuantityTare";
			this.textQuantityTare.Size = new System.Drawing.Size(400, 20);
			this.textQuantityTare.TabIndex = 1;
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.textReloadTare);
			this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox4.Location = new System.Drawing.Point(3, 156);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(406, 45);
			this.groupBox4.TabIndex = 3;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Время для повторного использования";
			// 
			// textReloadTare
			// 
			this.textReloadTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textReloadTare.Location = new System.Drawing.Point(3, 16);
			this.textReloadTare.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
			this.textReloadTare.Name = "textReloadTare";
			this.textReloadTare.Size = new System.Drawing.Size(400, 20);
			this.textReloadTare.TabIndex = 1;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 3;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.tableLayoutPanel2.Controls.Add(this.cancel, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.importCSV, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.addTare, 2, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 207);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 1;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(406, 34);
			this.tableLayoutPanel2.TabIndex = 4;
			// 
			// cancel
			// 
			this.cancel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cancel.Location = new System.Drawing.Point(3, 3);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(129, 28);
			this.cancel.TabIndex = 0;
			this.cancel.Text = "Отмена";
			this.cancel.UseVisualStyleBackColor = true;
			this.cancel.Click += new System.EventHandler(this.cancel_Click);
			// 
			// importCSV
			// 
			this.importCSV.Dock = System.Windows.Forms.DockStyle.Fill;
			this.importCSV.Location = new System.Drawing.Point(138, 3);
			this.importCSV.Name = "importCSV";
			this.importCSV.Size = new System.Drawing.Size(129, 28);
			this.importCSV.TabIndex = 1;
			this.importCSV.Text = "Импорт с CSV";
			this.importCSV.UseVisualStyleBackColor = true;
			this.importCSV.Click += new System.EventHandler(this.importCSV_Click);
			// 
			// addTare
			// 
			this.addTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addTare.Location = new System.Drawing.Point(273, 3);
			this.addTare.Name = "addTare";
			this.addTare.Size = new System.Drawing.Size(130, 28);
			this.addTare.TabIndex = 2;
			this.addTare.Text = "Сохранить";
			this.addTare.UseVisualStyleBackColor = true;
			this.addTare.Click += new System.EventHandler(this.addTare_Click);
			// 
			// AddTareForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(412, 244);
			this.Controls.Add(this.tableLayoutPanel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "AddTareForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Добавление Тары";
			this.Shown += new System.EventHandler(this.AddTareForm_Shown);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.textPriceTare)).EndInit();
			this.groupBox3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.textQuantityTare)).EndInit();
			this.groupBox4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.textReloadTare)).EndInit();
			this.tableLayoutPanel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.Button importCSV;
		private System.Windows.Forms.Button addTare;
		private System.Windows.Forms.TextBox textTypeTare;
		private System.Windows.Forms.NumericUpDown textPriceTare;
		private System.Windows.Forms.NumericUpDown textQuantityTare;
		private System.Windows.Forms.NumericUpDown textReloadTare;
		private System.Windows.Forms.TextBox textNameTare;
	}
}