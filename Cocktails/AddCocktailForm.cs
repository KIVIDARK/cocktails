﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace Cocktails
{
	public partial class AddCocktailForm : Form
	{
		#region Global

		DataBaseWork database = new DataBaseWork();

		string tempEditTypeQuantity = String.Empty;
		decimal tempEditQuantity = 1.0M;

		List<int> listProductItemsId = new List<int>();
		List<string> listProductItemsUnit = new List<string>();
		List<decimal> listProductItemsPrice = new List<decimal>();
		
		int productIdForAdded = -1;
		decimal productPriceForAdded = 0.0M;

		public AddCocktailForm(DataBaseWork database)
		{
			InitializeComponent();
			this.database = database;
		}

		private void ReloadDataFromDatabase()
		{
			List<Product> productList = database.GetProductsList();

			int productRowNumber = 0;
			dataGridViewProducts.Rows.Clear();
			foreach (Product product in productList)
			{
				dataGridViewProducts.Rows.Add();
				dataGridViewProducts.Rows[productRowNumber].Cells[dgvListProductsId.Name].Value = product.Id;
				dataGridViewProducts.Rows[productRowNumber].Cells[dgvListProductsType.Name].Value =
					product.TypeProduct.name.TrimEnd();
				dataGridViewProducts.Rows[productRowNumber].Cells[dgvListProductsName.Name].Value =
					product.name.TrimEnd();
				dataGridViewProducts.Rows[productRowNumber].Cells[dgvListProductsQuantity.Name].Value =
					product.quantity.ToString() + " " + product.TypeProduct.unit.TrimEnd();
				dataGridViewProducts.Rows[productRowNumber].Cells[dgvListProductsTypeId.Name].Value =
					product.typeId;
				dataGridViewProducts.Rows[productRowNumber].Cells[dgvListProductsPrice.Name].Value =
					product.price;
				productRowNumber++;
			}

			List<Tare> tareList = database.GetTareList();

			int tareRowNumber = 0;
			dataGridViewTare.Rows.Clear();
			foreach (Tare tare in tareList)
			{
				dataGridViewTare.Rows.Add();
				dataGridViewTare.Rows[tareRowNumber].Cells[dgvListTareId.Name].Value = tare.Id;
				dataGridViewTare.Rows[tareRowNumber].Cells[dgvListTareType.Name].Value =
					tare.type.TrimEnd();
				dataGridViewTare.Rows[tareRowNumber].Cells[dgvListTareQuantity.Name].Value =
					tare.quantity;
				dataGridViewTare.Rows[tareRowNumber].Cells[dgvListTarePrice.Name].Value =
					tare.price;
				tareRowNumber++;
			}
		}

		private void AddCocktailForm_Load(object sender, EventArgs e)
		{
			ReloadDataFromDatabase();
		}

		private void CalculatePrice()
		{
			decimal tempPrice = 0.0M;
			foreach (DataGridViewRow row in dataGridViewReadyProducts.Rows)
			{
				tempPrice += GetQuantityValue(row.Cells[dgvReadyProductsQuantity.Name].Value.ToString())
					* (decimal)row.Cells[dgvReadyProductsPrice.Name].Value;
			}
			foreach (DataGridViewRow row in dataGridViewReadyTare.Rows)
			{
				tempPrice += (decimal)row.Cells[dgvReadyTarePrice.Name].Value
					* int.Parse(row.Cells[dgvReadyTareQuantity.Name].Value.ToString());
			}
			tempPrice += textAdditionalCharges.Value;
			labelPriceCocktail.Text = tempPrice.ToString("#.00");
		}

		private void dataGridViewReadyProducts_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
		{
			CalculatePrice();
		}

		private void dataGridViewReadyTare_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
		{
			CalculatePrice();
		}

		private void dataGridViewReadyProducts_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
		{
			CalculatePrice();
		}

		private void dataGridViewReadyTare_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
		{
			CalculatePrice();
		}

		private void textAdditionalCharges_ValueChanged(object sender, EventArgs e)
		{
			CalculatePrice();
		}

		private void textAdditionalCharges_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				CalculatePrice();
			}
		}

		private void cancelCreate_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}

		private void createCocktail_Click(object sender, EventArgs e)
		{
			if (dataGridViewReadyProducts.Rows.Count == 0
				|| dataGridViewReadyTare.Rows.Count == 0
				|| textNameCocktail.Text.Length == 0
				|| textDescriptionCocktail.Text.Length == 0)
			{
				CocktailsForm.ShowMessage("Не все поля заполнены!");
			}
			else
			{
				XDocument xDocument = new XDocument();
				XElement xElementRecipe = new XElement("recipe");
				XElement xElementProducts = new XElement("products");
				foreach (DataGridViewRow row in dataGridViewReadyProducts.Rows)
				{
					XElement xElementProduct = new XElement("product");

					XAttribute xAtributeId = new XAttribute("id",
						row.Cells[dgvReadyProductsId.Name].Value.ToString());
					xElementProduct.Add(xAtributeId);

					XAttribute xAtributeQuantity = new XAttribute("quantity",
						GetQuantityValue(
						row.Cells[dgvReadyProductsQuantity.Name].Value.ToString()).ToString());
					xElementProduct.Add(xAtributeQuantity);

					xElementProducts.Add(xElementProduct);
				}

				XElement xElementTares = new XElement("tares");
				foreach (DataGridViewRow row in dataGridViewReadyTare.Rows)
				{
					XElement xElementTare = new XElement("tare");

					XAttribute xAtributeId = new XAttribute("id",
						row.Cells[dgvReadyTareId.Name].Value.ToString());
					xElementTare.Add(xAtributeId);

					XAttribute xAtributeQuantity = new XAttribute("quantity",
						row.Cells[dgvReadyTareQuantity.Name].Value.ToString());
					xElementTare.Add(xAtributeQuantity);

					xElementTares.Add(xElementTare);
				}
				xElementRecipe.Add(xElementProducts);
				xElementRecipe.Add(xElementTares);
				xDocument.Add(xElementRecipe);

				int complication = 0;

				if (dataGridViewReadyProducts.Rows.Count <= 3)
				{
					complication = 1;
				}
				if (dataGridViewReadyProducts.Rows.Count > 3 && dataGridViewReadyProducts.Rows.Count <= 6)
				{
					complication = 2;
				}
				if (dataGridViewReadyProducts.Rows.Count > 6)
				{
					complication = 3;
				}

				Recipe recipe = new Recipe();
				recipe.name = textNameCocktail.Text;
				recipe.popularity = 0;
				recipe.process = xElementRecipe;
				recipe.description = textDescriptionCocktail.Text;
				recipe.complication = complication;
				recipe.additional_charges = textAdditionalCharges.Value;
				if (database.IsExistRecipeInDatabase(textNameCocktail.Text) == false)
				{
					database.AddRecipeToDatabase(recipe);
				}
				else
				{
					CocktailsForm.ShowMessage("Такой коктейль уже существует!");
				}
				DialogResult = System.Windows.Forms.DialogResult.OK;
			}
		}

		#endregion

		#region Product

		private string GetQuantityString(decimal dec)
		{
			return dec.ToString("G") + " " + tempEditTypeQuantity;
		}

		private decimal GetQuantityValue(string str)
		{
			string[] mas = str.Split(new char[] { ' ' }, 2);
			tempEditTypeQuantity = mas[1];
			return decimal.Parse(mas[0]);
		}

		private void AddToReadyProduct(decimal quantity = 1.0M)
		{
			if (dataGridViewProducts.SelectedRows.Count != 0)
			{
				bool isExist = false;
				foreach (DataGridViewRow row in dataGridViewReadyProducts.Rows)
				{
					if (row.Cells[dgvReadyProductsId.Name].Value ==
						dataGridViewProducts.SelectedRows[0].Cells[dgvListProductsId.Name].Value)
					{
						isExist = true;
					}
				}
				if (isExist == false)
				{
					string[] mas = dataGridViewProducts.SelectedRows[0].Cells[
						dgvListProductsQuantity.Name].Value.ToString().Split(new char[] { ' ' }, 2);
					tempEditTypeQuantity = mas[1];

					dataGridViewReadyProducts.Rows.Add(
						dataGridViewProducts.SelectedRows[0].Cells[dgvListProductsId.Name].Value,
						dataGridViewProducts.SelectedRows[0].Cells[dgvListProductsName.Name].Value,
						dataGridViewProducts.SelectedRows[0].Cells[dgvListProductsType.Name].Value,
						GetQuantityString(quantity),
						dataGridViewProducts.SelectedRows[0].Cells[dgvListProductsTypeId.Name].Value,
						dataGridViewProducts.SelectedRows[0].Cells[dgvListProductsPrice.Name].Value
						);
				}
			}
		}

		private void addToReadyProducts_Click(object sender, EventArgs e)
		{
			AddToReadyProduct();
		}

		private void dataGridViewReadyProducts_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			dataGridViewReadyProducts.Rows[e.RowIndex].Cells[e.ColumnIndex].Value =
				tempEditQuantity = GetQuantityValue(
				dataGridViewReadyProducts.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
		}

		private void dataGridViewReadyProducts_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				dataGridViewReadyProducts.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = GetQuantityString(
					decimal.Parse(dataGridViewReadyProducts.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()));
			}
			catch (System.FormatException)
			{
				dataGridViewReadyProducts.Rows[e.RowIndex].Cells[e.ColumnIndex].Value =
					GetQuantityString(tempEditQuantity);
			}
			ViewPriceCurrentReadyProduct();
			CalculatePrice();
		}

		private void ViewPriceCurrentReadyProduct()
		{
			if (dataGridViewReadyProducts.IsCurrentCellInEditMode == false)
			{
				labelPriceProduct.Text = "Цена выбраного:\n" + (
					(decimal)dataGridViewReadyProducts.SelectedRows[0].Cells[dgvReadyProductsPrice.Name].Value *
					GetQuantityValue(
						dataGridViewReadyProducts.SelectedRows[0].Cells[
							dgvReadyProductsQuantity.Name].Value.ToString())
					).ToString("#.00");
			}
		}

		private void dataGridViewReadyProducts_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			ViewPriceCurrentReadyProduct();
		}

		private void dataGridViewProducts_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			AddToReadyProduct();
		}

		private void dataGridViewProducts_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (dataGridViewProducts.SelectedRows.Count != 0)
			{
				labelPriceProduct.Text = "Цена выбраного:\n" + (
					(decimal)dataGridViewProducts.SelectedRows[0].Cells[dgvListProductsPrice.Name].Value
					).ToString("#.00");
			}
		}

		private void DeleteInReadyProduct()
		{
			if (dataGridViewReadyProducts.SelectedRows.Count != 0)
			{
				dataGridViewReadyProducts.Rows.RemoveAt(dataGridViewReadyProducts.SelectedRows[0].Index);
			}
		}

		private void deleteInReadyProducts_Click(object sender, EventArgs e)
		{
			DeleteInReadyProduct();
		}

		#endregion

		#region Tare

		private void AddToReadyTare()
		{
			if (dataGridViewTare.SelectedRows.Count != 0)
			{
				bool isExist = false;
				foreach (DataGridViewRow row in dataGridViewReadyTare.Rows)
				{
					if (row.Cells[dgvReadyTareId.Name].Value ==
						dataGridViewTare.SelectedRows[0].Cells[dgvListTareId.Name].Value)
					{
						isExist = true;
					}
				}
				if (isExist == false)
				{
					dataGridViewReadyTare.Rows.Add(
						dataGridViewTare.SelectedRows[0].Cells[dgvListTareId.Name].Value,
						dataGridViewTare.SelectedRows[0].Cells[dgvListTarePrice.Name].Value,
						dataGridViewTare.SelectedRows[0].Cells[dgvListTareType.Name].Value,
						1
						);
				}
			}
		}

		private void ViewPriceCurrentReadyTare()
		{
			if (dataGridViewReadyTare.IsCurrentCellInEditMode == false)
			{
				labelPriceTare.Text = "Цена выбраного:\n" + (
					(decimal)dataGridViewReadyTare.SelectedRows[0].Cells[dgvReadyTarePrice.Name].Value *
						int.Parse(dataGridViewReadyTare.SelectedRows[0].Cells[
						dgvReadyTareQuantity.Name].Value.ToString())
					).ToString("#.00");
			}
		}

		private void dataGridViewReadyTare_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			ViewPriceCurrentReadyTare();
		}

		private void addToReadyTare_Click(object sender, EventArgs e)
		{
			AddToReadyTare();
		}

		private void dataGridViewTare_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			AddToReadyTare();
		}

		private void dataGridViewReadyTare_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			int res = 0;
			if (int.TryParse(dataGridViewReadyTare.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),
				out res) == false)
			{
				dataGridViewReadyTare.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 1;
			}
			ViewPriceCurrentReadyTare();
			CalculatePrice();
		}

		private void DeleteInReadyTare()
		{
			if (dataGridViewReadyTare.SelectedRows.Count != 0)
			{
				dataGridViewReadyTare.Rows.RemoveAt(dataGridViewReadyTare.SelectedRows[0].Index);
			}
		}

		private void deleteInReadyTare_Click(object sender, EventArgs e)
		{
			DeleteInReadyTare();
		}

		private void dataGridViewTare_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (dataGridViewTare.SelectedRows.Count != 0)
			{
				labelPriceTare.Text = "Цена выбраного:\n" + (
					(decimal)dataGridViewTare.SelectedRows[0].Cells[dgvListTarePrice.Name].Value
					).ToString("#.00");
			}
		}

		#endregion

		#region ProductSearch

		private void comboBoxSearchProducts_TextUpdate(object sender, EventArgs e)
		{
			comboBoxSearchProducts.Items.Clear();
			listProductItemsId.Clear();
			listProductItemsPrice.Clear();
			listProductItemsUnit.Clear();
			comboBoxSearchProducts.DroppedDown = true;
			int counter = 10;
			foreach (DataGridViewRow row in dataGridViewProducts.Rows)
			{
				if ((row.Cells[dgvListProductsName.Name].Value.ToString().ToLower()
					+ " " + row.Cells[dgvListProductsType.Name].Value.ToString().ToLower()).Contains(
					comboBoxSearchProducts.Text.ToLower()))
				{
					comboBoxSearchProducts.Items.Add(row.Cells[dgvListProductsName.Name].Value.ToString()
						+ " " + row.Cells[dgvListProductsType.Name].Value.ToString());
					listProductItemsId.Add((int)row.Cells[dgvListProductsId.Name].Value);
					listProductItemsUnit.Add(
						row.Cells[dgvListProductsQuantity.Name].Value.ToString().Split(new char[] {' '}, 2)[1]);
					listProductItemsPrice.Add((decimal)row.Cells[dgvListProductsPrice.Name].Value);
				}
				if (counter == 0)
				{
					break;
				}
				counter--;
			}
			comboBoxSearchProducts.SelectionStart = comboBoxSearchProducts.Text.Length;
			if (comboBoxSearchProducts.Text.Length == 0)
			{
				labelCountProducts.Text = "Единиц";
				productIdForAdded = -1;
			}
		}

		private void comboBoxSearchProducts_Leave(object sender, EventArgs e)
		{
			comboBoxSearchProducts.DroppedDown = false;
		}

		private void comboBoxSearchProducts_KeyDown(object sender, KeyEventArgs e)
		{
			if (comboBoxSearchProducts.DroppedDown == false && e.KeyCode == Keys.Enter)
			{
				textCountProducts.Focus();
			}
		}

		private void comboBoxSearchProducts_DropDownClosed(object sender, EventArgs e)
		{
			if (comboBoxSearchProducts.Items.Count == 0)
			{
				comboBoxSearchProducts.SelectedIndex = -1;
				comboBoxSearchProducts.Text = String.Empty;
			}
		}

		private void comboBoxSearchProducts_SelectionChangeCommitted(object sender, EventArgs e)
		{
			productIdForAdded = listProductItemsId[comboBoxSearchProducts.SelectedIndex];
			labelCountProducts.Text = listProductItemsUnit[comboBoxSearchProducts.SelectedIndex];
			productPriceForAdded = listProductItemsPrice[comboBoxSearchProducts.SelectedIndex];
		}

		private void addProductToCocktail_Click(object sender, EventArgs e)
		{
			if (productIdForAdded != -1)
			{
				foreach (DataGridViewRow row in dataGridViewProducts.Rows)
				{
					if ((int)row.Cells[dgvListProductsId.Name].Value == productIdForAdded)
					{
						row.Selected = true;
						AddToReadyProduct(textCountProducts.Value);
						comboBoxSearchProducts.Items.Clear();
						textCountProducts.Value = 1.0M;
						comboBoxSearchProducts.Text = String.Empty;
						break;
					}
				}
			}
		}

		private void textCountProducts_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				addProductToCocktail.Focus();
				labelPriceProduct.Text = "Цена выбраного:\n" + (
					productPriceForAdded * textCountProducts.Value
					).ToString("#.00");	
			}
		}

		#endregion

	}
}
