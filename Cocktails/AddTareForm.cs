﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cocktails
{
	public partial class AddTareForm : Form
	{
		bool isTareAddState = false;
		string editTypeTare = String.Empty;
		int idEditTare = 0;
		DataBaseWork database = new DataBaseWork();
		string tempType = String.Empty;
		decimal tempPrice = 0.0M;
		int tempQuantity = 0;
		int tempReloading = 0;

		public AddTareForm(DataBaseWork database,
			string tempType = "", decimal tempPrice = 0.0M, int tempQuantity = 0,
			int tempReloading = 0, int idEditTare = 0, bool isTareAddState = true)
		{
			InitializeComponent();
			this.isTareAddState = isTareAddState;
			this.database = database;
			this.idEditTare = idEditTare;
			this.tempType = tempType;
			this.tempPrice = tempPrice;
			this.tempQuantity = tempQuantity;
			this.tempReloading = tempReloading;
		}

		private Tare GetTareFromForm()
		{
			Tare tare = new Tare();
			tare.type = textNameTare.Text;
			tare.quantity = (int)textQuantityTare.Value;
			tare.reloading = (int)textReloadTare.Value;
			tare.price = textPriceTare.Value;
			return tare;
		}

		private void addTare_Click(object sender, EventArgs e)
		{
			if (textNameTare.Text.Length != 0)
			{
				if (isTareAddState == true)
				{
					if (database.IsExistTareInDatabase(textNameTare.Text) == false)
					{
						database.AddTareToDatabase(GetTareFromForm());
						DialogResult = System.Windows.Forms.DialogResult.OK;
					}
					else
					{
						CocktailsForm.ShowMessage("Такая тара уже существует!");
					}
				}
				else
				{
					if (editTypeTare.ToLower() != textNameTare.Text.ToLower())
					{
						if (database.IsExistTareInDatabase(textNameTare.Text) == false)
						{
							Tare tare = GetTareFromForm();
							tare.Id = idEditTare;
							database.UpdateTareToDatabase(tare);
							DialogResult = System.Windows.Forms.DialogResult.OK;
						}
						else
						{
							CocktailsForm.ShowMessage("Такая тара уже существует!");
						}
					}
					else
					{
						Tare tare = GetTareFromForm();
						tare.Id = idEditTare;
						database.UpdateTareToDatabase(tare);
						DialogResult = System.Windows.Forms.DialogResult.OK;
					}
				}
			}
			else
			{
				CocktailsForm.ShowMessage("Не все поля заполнены!");
			}
		}

		private void cancel_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}

		private void importCSV_Click(object sender, EventArgs e)
		{

		}

		private void AddTareForm_Shown(object sender, EventArgs e)
		{
			textNameTare.Text = tempType;
			textPriceTare.Value = tempPrice;
			textQuantityTare.Value = tempQuantity;
			textReloadTare.Value = tempReloading;
			editTypeTare = tempType;
		}
	}
}
