﻿namespace Cocktails
{
	partial class AddCocktailForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
			this.dataGridViewTare = new System.Windows.Forms.DataGridView();
			this.dgvListTareId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvListTarePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvListTareType = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvListTareQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewReadyTare = new System.Windows.Forms.DataGridView();
			this.dgvReadyTareId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvReadyTarePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvReadyTareType = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvReadyTareQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
			this.addToReadyTare = new System.Windows.Forms.Button();
			this.deleteInReadyTare = new System.Windows.Forms.Button();
			this.panel6 = new System.Windows.Forms.Panel();
			this.labelPriceTare = new System.Windows.Forms.Label();
			this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.comboBoxSearchProducts = new System.Windows.Forms.ComboBox();
			this.addProductToCocktail = new System.Windows.Forms.Button();
			this.panel5 = new System.Windows.Forms.Panel();
			this.labelCountProducts = new System.Windows.Forms.Label();
			this.textCountProducts = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.comboBoxSearchTare = new System.Windows.Forms.ComboBox();
			this.addTareToCocktail = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label2 = new System.Windows.Forms.Label();
			this.textCountTare = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
			this.dataGridViewProducts = new System.Windows.Forms.DataGridView();
			this.dgvListProductsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvListProductsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvListProductsType = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvListProductsQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvListProductsTypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvListProductsPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
			this.addToReadyProducts = new System.Windows.Forms.Button();
			this.deleteInReadyProducts = new System.Windows.Forms.Button();
			this.panel3 = new System.Windows.Forms.Panel();
			this.labelPriceProduct = new System.Windows.Forms.Label();
			this.dataGridViewReadyProducts = new System.Windows.Forms.DataGridView();
			this.dgvReadyProductsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvReadyProductsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvReadyProductsType = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvReadyProductsQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvReadyProductsTypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvReadyProductsPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
			this.createCocktail = new System.Windows.Forms.Button();
			this.cancelCreate = new System.Windows.Forms.Button();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.labelPriceCocktail = new System.Windows.Forms.Label();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.textAdditionalCharges = new System.Windows.Forms.NumericUpDown();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.textDescriptionCocktail = new System.Windows.Forms.TextBox();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.textNameCocktail = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.tableLayoutPanel5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewTare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewReadyTare)).BeginInit();
			this.tableLayoutPanel7.SuspendLayout();
			this.panel6.SuspendLayout();
			this.tableLayoutPanel19.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textCountProducts)).BeginInit();
			this.tableLayoutPanel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textCountTare)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.tableLayoutPanel4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts)).BeginInit();
			this.tableLayoutPanel6.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewReadyProducts)).BeginInit();
			this.tableLayoutPanel3.SuspendLayout();
			this.tableLayoutPanel8.SuspendLayout();
			this.tableLayoutPanel9.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textAdditionalCharges)).BeginInit();
			this.groupBox5.SuspendLayout();
			this.groupBox6.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel19, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 4);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 4;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(581, 502);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.tableLayoutPanel5);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(3, 344);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(575, 155);
			this.groupBox2.TabIndex = 5;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Тара для коктейля";
			// 
			// tableLayoutPanel5
			// 
			this.tableLayoutPanel5.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel5.ColumnCount = 3;
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel5.Controls.Add(this.dataGridViewTare, 2, 0);
			this.tableLayoutPanel5.Controls.Add(this.dataGridViewReadyTare, 0, 0);
			this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel7, 1, 0);
			this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel5.Name = "tableLayoutPanel5";
			this.tableLayoutPanel5.RowCount = 1;
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel5.Size = new System.Drawing.Size(569, 136);
			this.tableLayoutPanel5.TabIndex = 3;
			// 
			// dataGridViewTare
			// 
			this.dataGridViewTare.AllowUserToAddRows = false;
			this.dataGridViewTare.AllowUserToResizeRows = false;
			this.dataGridViewTare.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewTare.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvListTareId,
            this.dgvListTarePrice,
            this.dgvListTareType,
            this.dgvListTareQuantity});
			this.dataGridViewTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewTare.Location = new System.Drawing.Point(328, 4);
			this.dataGridViewTare.MultiSelect = false;
			this.dataGridViewTare.Name = "dataGridViewTare";
			this.dataGridViewTare.RowHeadersVisible = false;
			this.dataGridViewTare.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewTare.Size = new System.Drawing.Size(237, 128);
			this.dataGridViewTare.TabIndex = 2;
			this.dataGridViewTare.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTare_CellClick);
			this.dataGridViewTare.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTare_CellDoubleClick);
			// 
			// dgvListTareId
			// 
			this.dgvListTareId.HeaderText = "Id";
			this.dgvListTareId.Name = "dgvListTareId";
			this.dgvListTareId.ReadOnly = true;
			this.dgvListTareId.Visible = false;
			// 
			// dgvListTarePrice
			// 
			this.dgvListTarePrice.HeaderText = "TarePrice";
			this.dgvListTarePrice.Name = "dgvListTarePrice";
			this.dgvListTarePrice.ReadOnly = true;
			this.dgvListTarePrice.Visible = false;
			// 
			// dgvListTareType
			// 
			this.dgvListTareType.HeaderText = "Тип";
			this.dgvListTareType.Name = "dgvListTareType";
			this.dgvListTareType.ReadOnly = true;
			// 
			// dgvListTareQuantity
			// 
			this.dgvListTareQuantity.HeaderText = "Количество";
			this.dgvListTareQuantity.Name = "dgvListTareQuantity";
			this.dgvListTareQuantity.ReadOnly = true;
			// 
			// dataGridViewReadyTare
			// 
			this.dataGridViewReadyTare.AllowUserToAddRows = false;
			this.dataGridViewReadyTare.AllowUserToResizeRows = false;
			this.dataGridViewReadyTare.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewReadyTare.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvReadyTareId,
            this.dgvReadyTarePrice,
            this.dgvReadyTareType,
            this.dgvReadyTareQuantity});
			this.dataGridViewReadyTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewReadyTare.Location = new System.Drawing.Point(4, 4);
			this.dataGridViewReadyTare.MultiSelect = false;
			this.dataGridViewReadyTare.Name = "dataGridViewReadyTare";
			this.dataGridViewReadyTare.RowHeadersVisible = false;
			this.dataGridViewReadyTare.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewReadyTare.Size = new System.Drawing.Size(236, 128);
			this.dataGridViewReadyTare.TabIndex = 2;
			this.dataGridViewReadyTare.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewReadyTare_CellClick);
			this.dataGridViewReadyTare.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewReadyTare_CellEndEdit);
			this.dataGridViewReadyTare.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewReadyTare_RowsAdded);
			this.dataGridViewReadyTare.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridViewReadyTare_RowsRemoved);
			// 
			// dgvReadyTareId
			// 
			this.dgvReadyTareId.Frozen = true;
			this.dgvReadyTareId.HeaderText = "Id";
			this.dgvReadyTareId.Name = "dgvReadyTareId";
			this.dgvReadyTareId.ReadOnly = true;
			this.dgvReadyTareId.Visible = false;
			// 
			// dgvReadyTarePrice
			// 
			this.dgvReadyTarePrice.Frozen = true;
			this.dgvReadyTarePrice.HeaderText = "Price";
			this.dgvReadyTarePrice.Name = "dgvReadyTarePrice";
			this.dgvReadyTarePrice.ReadOnly = true;
			this.dgvReadyTarePrice.Visible = false;
			// 
			// dgvReadyTareType
			// 
			this.dgvReadyTareType.Frozen = true;
			this.dgvReadyTareType.HeaderText = "Тип";
			this.dgvReadyTareType.Name = "dgvReadyTareType";
			this.dgvReadyTareType.ReadOnly = true;
			this.dgvReadyTareType.Width = 120;
			// 
			// dgvReadyTareQuantity
			// 
			dataGridViewCellStyle1.Format = "N0";
			dataGridViewCellStyle1.NullValue = "1";
			this.dgvReadyTareQuantity.DefaultCellStyle = dataGridViewCellStyle1;
			this.dgvReadyTareQuantity.Frozen = true;
			this.dgvReadyTareQuantity.HeaderText = "Количество";
			this.dgvReadyTareQuantity.Name = "dgvReadyTareQuantity";
			this.dgvReadyTareQuantity.Width = 80;
			// 
			// tableLayoutPanel7
			// 
			this.tableLayoutPanel7.ColumnCount = 1;
			this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel7.Controls.Add(this.addToReadyTare, 0, 1);
			this.tableLayoutPanel7.Controls.Add(this.deleteInReadyTare, 0, 2);
			this.tableLayoutPanel7.Controls.Add(this.panel6, 0, 0);
			this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel7.Location = new System.Drawing.Point(244, 1);
			this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel7.Name = "tableLayoutPanel7";
			this.tableLayoutPanel7.RowCount = 4;
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
			this.tableLayoutPanel7.Size = new System.Drawing.Size(80, 134);
			this.tableLayoutPanel7.TabIndex = 1;
			// 
			// addToReadyTare
			// 
			this.addToReadyTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addToReadyTare.Location = new System.Drawing.Point(3, 54);
			this.addToReadyTare.Name = "addToReadyTare";
			this.addToReadyTare.Size = new System.Drawing.Size(74, 24);
			this.addToReadyTare.TabIndex = 0;
			this.addToReadyTare.Text = "Добавить";
			this.addToReadyTare.UseVisualStyleBackColor = true;
			this.addToReadyTare.Click += new System.EventHandler(this.addToReadyTare_Click);
			// 
			// deleteInReadyTare
			// 
			this.deleteInReadyTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.deleteInReadyTare.Location = new System.Drawing.Point(3, 84);
			this.deleteInReadyTare.Name = "deleteInReadyTare";
			this.deleteInReadyTare.Size = new System.Drawing.Size(74, 24);
			this.deleteInReadyTare.TabIndex = 1;
			this.deleteInReadyTare.Text = "Убрать";
			this.deleteInReadyTare.UseVisualStyleBackColor = true;
			this.deleteInReadyTare.Click += new System.EventHandler(this.deleteInReadyTare_Click);
			// 
			// panel6
			// 
			this.panel6.Controls.Add(this.labelPriceTare);
			this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel6.Location = new System.Drawing.Point(3, 3);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(74, 45);
			this.panel6.TabIndex = 3;
			// 
			// labelPriceTare
			// 
			this.labelPriceTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.labelPriceTare.Location = new System.Drawing.Point(0, 0);
			this.labelPriceTare.Name = "labelPriceTare";
			this.labelPriceTare.Size = new System.Drawing.Size(74, 45);
			this.labelPriceTare.TabIndex = 0;
			this.labelPriceTare.Text = "Цена выбраного:";
			this.labelPriceTare.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// tableLayoutPanel19
			// 
			this.tableLayoutPanel19.BackColor = System.Drawing.Color.Gainsboro;
			this.tableLayoutPanel19.ColumnCount = 3;
			this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
			this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel19.Controls.Add(this.panel4, 0, 0);
			this.tableLayoutPanel19.Controls.Add(this.addProductToCocktail, 2, 0);
			this.tableLayoutPanel19.Controls.Add(this.panel5, 1, 0);
			this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel19.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel19.Name = "tableLayoutPanel19";
			this.tableLayoutPanel19.RowCount = 1;
			this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel19.Size = new System.Drawing.Size(575, 44);
			this.tableLayoutPanel19.TabIndex = 1;
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.Gainsboro;
			this.panel4.Controls.Add(this.comboBoxSearchProducts);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel4.Location = new System.Drawing.Point(3, 3);
			this.panel4.Name = "panel4";
			this.panel4.Padding = new System.Windows.Forms.Padding(6);
			this.panel4.Size = new System.Drawing.Size(219, 38);
			this.panel4.TabIndex = 0;
			// 
			// comboBoxSearchProducts
			// 
			this.comboBoxSearchProducts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.comboBoxSearchProducts.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.comboBoxSearchProducts.FormattingEnabled = true;
			this.comboBoxSearchProducts.Location = new System.Drawing.Point(6, 6);
			this.comboBoxSearchProducts.Name = "comboBoxSearchProducts";
			this.comboBoxSearchProducts.Size = new System.Drawing.Size(207, 28);
			this.comboBoxSearchProducts.TabIndex = 0;
			this.comboBoxSearchProducts.SelectionChangeCommitted += new System.EventHandler(this.comboBoxSearchProducts_SelectionChangeCommitted);
			this.comboBoxSearchProducts.TextUpdate += new System.EventHandler(this.comboBoxSearchProducts_TextUpdate);
			this.comboBoxSearchProducts.DropDownClosed += new System.EventHandler(this.comboBoxSearchProducts_DropDownClosed);
			this.comboBoxSearchProducts.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBoxSearchProducts_KeyDown);
			this.comboBoxSearchProducts.Leave += new System.EventHandler(this.comboBoxSearchProducts_Leave);
			// 
			// addProductToCocktail
			// 
			this.addProductToCocktail.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addProductToCocktail.Location = new System.Drawing.Point(478, 3);
			this.addProductToCocktail.Name = "addProductToCocktail";
			this.addProductToCocktail.Size = new System.Drawing.Size(94, 38);
			this.addProductToCocktail.TabIndex = 2;
			this.addProductToCocktail.Text = "Добавить";
			this.addProductToCocktail.UseVisualStyleBackColor = true;
			this.addProductToCocktail.Click += new System.EventHandler(this.addProductToCocktail_Click);
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.labelCountProducts);
			this.panel5.Controls.Add(this.textCountProducts);
			this.panel5.Controls.Add(this.label1);
			this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel5.Location = new System.Drawing.Point(228, 3);
			this.panel5.Name = "panel5";
			this.panel5.Padding = new System.Windows.Forms.Padding(5);
			this.panel5.Size = new System.Drawing.Size(244, 38);
			this.panel5.TabIndex = 1;
			// 
			// labelCountProducts
			// 
			this.labelCountProducts.AutoSize = true;
			this.labelCountProducts.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelCountProducts.Location = new System.Drawing.Point(143, 9);
			this.labelCountProducts.Name = "labelCountProducts";
			this.labelCountProducts.Size = new System.Drawing.Size(73, 20);
			this.labelCountProducts.TabIndex = 6;
			this.labelCountProducts.Text = "Единиц";
			// 
			// textCountProducts
			// 
			this.textCountProducts.DecimalPlaces = 2;
			this.textCountProducts.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textCountProducts.Location = new System.Drawing.Point(36, 7);
			this.textCountProducts.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
			this.textCountProducts.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.textCountProducts.Name = "textCountProducts";
			this.textCountProducts.Size = new System.Drawing.Size(101, 26);
			this.textCountProducts.TabIndex = 5;
			this.textCountProducts.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.textCountProducts.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textCountProducts_KeyDown);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(9, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(20, 20);
			this.label1.TabIndex = 4;
			this.label1.Text = "X";
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.BackColor = System.Drawing.Color.Gainsboro;
			this.tableLayoutPanel2.ColumnCount = 3;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.addTareToCocktail, 2, 0);
			this.tableLayoutPanel2.Controls.Add(this.panel2, 1, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 294);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 1;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(575, 44);
			this.tableLayoutPanel2.TabIndex = 2;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Gainsboro;
			this.panel1.Controls.Add(this.comboBoxSearchTare);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(3, 3);
			this.panel1.Name = "panel1";
			this.panel1.Padding = new System.Windows.Forms.Padding(6);
			this.panel1.Size = new System.Drawing.Size(219, 38);
			this.panel1.TabIndex = 0;
			// 
			// comboBoxSearchTare
			// 
			this.comboBoxSearchTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.comboBoxSearchTare.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.comboBoxSearchTare.FormattingEnabled = true;
			this.comboBoxSearchTare.Location = new System.Drawing.Point(6, 6);
			this.comboBoxSearchTare.Name = "comboBoxSearchTare";
			this.comboBoxSearchTare.Size = new System.Drawing.Size(207, 28);
			this.comboBoxSearchTare.TabIndex = 0;
			this.comboBoxSearchTare.Text = "DEMO";
			// 
			// addTareToCocktail
			// 
			this.addTareToCocktail.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addTareToCocktail.Location = new System.Drawing.Point(478, 3);
			this.addTareToCocktail.Name = "addTareToCocktail";
			this.addTareToCocktail.Size = new System.Drawing.Size(94, 38);
			this.addTareToCocktail.TabIndex = 2;
			this.addTareToCocktail.Text = "Добавить";
			this.addTareToCocktail.UseVisualStyleBackColor = true;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.label2);
			this.panel2.Controls.Add(this.textCountTare);
			this.panel2.Controls.Add(this.label4);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(228, 3);
			this.panel2.Name = "panel2";
			this.panel2.Padding = new System.Windows.Forms.Padding(5);
			this.panel2.Size = new System.Drawing.Size(244, 38);
			this.panel2.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label2.Location = new System.Drawing.Point(143, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(73, 20);
			this.label2.TabIndex = 6;
			this.label2.Text = "Единиц";
			// 
			// textCountTare
			// 
			this.textCountTare.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textCountTare.Location = new System.Drawing.Point(36, 7);
			this.textCountTare.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.textCountTare.Name = "textCountTare";
			this.textCountTare.Size = new System.Drawing.Size(101, 26);
			this.textCountTare.TabIndex = 5;
			this.textCountTare.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label4.Location = new System.Drawing.Point(9, 9);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(20, 20);
			this.label4.TabIndex = 4;
			this.label4.Text = "X";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.tableLayoutPanel4);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(3, 53);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(575, 235);
			this.groupBox1.TabIndex = 4;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Продукты для коктейля";
			// 
			// tableLayoutPanel4
			// 
			this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel4.ColumnCount = 3;
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel4.Controls.Add(this.dataGridViewProducts, 2, 0);
			this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel6, 1, 0);
			this.tableLayoutPanel4.Controls.Add(this.dataGridViewReadyProducts, 0, 0);
			this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel4.Name = "tableLayoutPanel4";
			this.tableLayoutPanel4.RowCount = 1;
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel4.Size = new System.Drawing.Size(569, 216);
			this.tableLayoutPanel4.TabIndex = 3;
			// 
			// dataGridViewProducts
			// 
			this.dataGridViewProducts.AllowUserToAddRows = false;
			this.dataGridViewProducts.AllowUserToResizeRows = false;
			this.dataGridViewProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvListProductsId,
            this.dgvListProductsName,
            this.dgvListProductsType,
            this.dgvListProductsQuantity,
            this.dgvListProductsTypeId,
            this.dgvListProductsPrice});
			this.dataGridViewProducts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewProducts.Location = new System.Drawing.Point(328, 4);
			this.dataGridViewProducts.MultiSelect = false;
			this.dataGridViewProducts.Name = "dataGridViewProducts";
			this.dataGridViewProducts.RowHeadersVisible = false;
			this.dataGridViewProducts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewProducts.Size = new System.Drawing.Size(237, 208);
			this.dataGridViewProducts.TabIndex = 2;
			this.dataGridViewProducts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewProducts_CellClick);
			this.dataGridViewProducts.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewProducts_CellDoubleClick);
			// 
			// dgvListProductsId
			// 
			this.dgvListProductsId.Frozen = true;
			this.dgvListProductsId.HeaderText = "Id";
			this.dgvListProductsId.Name = "dgvListProductsId";
			this.dgvListProductsId.ReadOnly = true;
			this.dgvListProductsId.Visible = false;
			// 
			// dgvListProductsName
			// 
			this.dgvListProductsName.Frozen = true;
			this.dgvListProductsName.HeaderText = "Наименование";
			this.dgvListProductsName.Name = "dgvListProductsName";
			this.dgvListProductsName.ReadOnly = true;
			// 
			// dgvListProductsType
			// 
			this.dgvListProductsType.Frozen = true;
			this.dgvListProductsType.HeaderText = "Тип";
			this.dgvListProductsType.Name = "dgvListProductsType";
			this.dgvListProductsType.ReadOnly = true;
			this.dgvListProductsType.Width = 50;
			// 
			// dgvListProductsQuantity
			// 
			this.dgvListProductsQuantity.Frozen = true;
			this.dgvListProductsQuantity.HeaderText = "Количество";
			this.dgvListProductsQuantity.Name = "dgvListProductsQuantity";
			this.dgvListProductsQuantity.ReadOnly = true;
			this.dgvListProductsQuantity.Width = 80;
			// 
			// dgvListProductsTypeId
			// 
			this.dgvListProductsTypeId.Frozen = true;
			this.dgvListProductsTypeId.HeaderText = "IDType";
			this.dgvListProductsTypeId.Name = "dgvListProductsTypeId";
			this.dgvListProductsTypeId.ReadOnly = true;
			this.dgvListProductsTypeId.Visible = false;
			// 
			// dgvListProductsPrice
			// 
			this.dgvListProductsPrice.Frozen = true;
			this.dgvListProductsPrice.HeaderText = "Price";
			this.dgvListProductsPrice.Name = "dgvListProductsPrice";
			this.dgvListProductsPrice.ReadOnly = true;
			this.dgvListProductsPrice.Visible = false;
			// 
			// tableLayoutPanel6
			// 
			this.tableLayoutPanel6.ColumnCount = 1;
			this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel6.Controls.Add(this.addToReadyProducts, 0, 1);
			this.tableLayoutPanel6.Controls.Add(this.deleteInReadyProducts, 0, 2);
			this.tableLayoutPanel6.Controls.Add(this.panel3, 0, 0);
			this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel6.Location = new System.Drawing.Point(244, 1);
			this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel6.Name = "tableLayoutPanel6";
			this.tableLayoutPanel6.RowCount = 4;
			this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel6.Size = new System.Drawing.Size(80, 214);
			this.tableLayoutPanel6.TabIndex = 0;
			// 
			// addToReadyProducts
			// 
			this.addToReadyProducts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addToReadyProducts.Location = new System.Drawing.Point(3, 80);
			this.addToReadyProducts.Name = "addToReadyProducts";
			this.addToReadyProducts.Size = new System.Drawing.Size(74, 24);
			this.addToReadyProducts.TabIndex = 0;
			this.addToReadyProducts.Text = "Добавить";
			this.addToReadyProducts.UseVisualStyleBackColor = true;
			this.addToReadyProducts.Click += new System.EventHandler(this.addToReadyProducts_Click);
			// 
			// deleteInReadyProducts
			// 
			this.deleteInReadyProducts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.deleteInReadyProducts.Location = new System.Drawing.Point(3, 110);
			this.deleteInReadyProducts.Name = "deleteInReadyProducts";
			this.deleteInReadyProducts.Size = new System.Drawing.Size(74, 24);
			this.deleteInReadyProducts.TabIndex = 1;
			this.deleteInReadyProducts.Text = "Убрать";
			this.deleteInReadyProducts.UseVisualStyleBackColor = true;
			this.deleteInReadyProducts.Click += new System.EventHandler(this.deleteInReadyProducts_Click);
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.labelPriceProduct);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(3, 3);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(74, 71);
			this.panel3.TabIndex = 2;
			// 
			// labelPriceProduct
			// 
			this.labelPriceProduct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.labelPriceProduct.Location = new System.Drawing.Point(0, 0);
			this.labelPriceProduct.Name = "labelPriceProduct";
			this.labelPriceProduct.Size = new System.Drawing.Size(74, 71);
			this.labelPriceProduct.TabIndex = 0;
			this.labelPriceProduct.Text = "Цена выбраного:";
			this.labelPriceProduct.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// dataGridViewReadyProducts
			// 
			this.dataGridViewReadyProducts.AllowUserToAddRows = false;
			this.dataGridViewReadyProducts.AllowUserToResizeRows = false;
			this.dataGridViewReadyProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewReadyProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvReadyProductsId,
            this.dgvReadyProductsName,
            this.dgvReadyProductsType,
            this.dgvReadyProductsQuantity,
            this.dgvReadyProductsTypeId,
            this.dgvReadyProductsPrice});
			this.dataGridViewReadyProducts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewReadyProducts.Location = new System.Drawing.Point(4, 4);
			this.dataGridViewReadyProducts.MultiSelect = false;
			this.dataGridViewReadyProducts.Name = "dataGridViewReadyProducts";
			this.dataGridViewReadyProducts.RowHeadersVisible = false;
			this.dataGridViewReadyProducts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewReadyProducts.Size = new System.Drawing.Size(236, 208);
			this.dataGridViewReadyProducts.TabIndex = 1;
			this.dataGridViewReadyProducts.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridViewReadyProducts_CellBeginEdit);
			this.dataGridViewReadyProducts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewReadyProducts_CellClick);
			this.dataGridViewReadyProducts.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewReadyProducts_CellEndEdit);
			this.dataGridViewReadyProducts.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewReadyProducts_RowsAdded);
			this.dataGridViewReadyProducts.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridViewReadyProducts_RowsRemoved);
			// 
			// dgvReadyProductsId
			// 
			this.dgvReadyProductsId.Frozen = true;
			this.dgvReadyProductsId.HeaderText = "Id";
			this.dgvReadyProductsId.Name = "dgvReadyProductsId";
			this.dgvReadyProductsId.ReadOnly = true;
			this.dgvReadyProductsId.Visible = false;
			// 
			// dgvReadyProductsName
			// 
			this.dgvReadyProductsName.Frozen = true;
			this.dgvReadyProductsName.HeaderText = "Наименование";
			this.dgvReadyProductsName.Name = "dgvReadyProductsName";
			this.dgvReadyProductsName.ReadOnly = true;
			// 
			// dgvReadyProductsType
			// 
			this.dgvReadyProductsType.Frozen = true;
			this.dgvReadyProductsType.HeaderText = "Тип";
			this.dgvReadyProductsType.Name = "dgvReadyProductsType";
			this.dgvReadyProductsType.ReadOnly = true;
			this.dgvReadyProductsType.Width = 50;
			// 
			// dgvReadyProductsQuantity
			// 
			dataGridViewCellStyle2.NullValue = "1";
			this.dgvReadyProductsQuantity.DefaultCellStyle = dataGridViewCellStyle2;
			this.dgvReadyProductsQuantity.Frozen = true;
			this.dgvReadyProductsQuantity.HeaderText = "Количество";
			this.dgvReadyProductsQuantity.Name = "dgvReadyProductsQuantity";
			this.dgvReadyProductsQuantity.Width = 80;
			// 
			// dgvReadyProductsTypeId
			// 
			this.dgvReadyProductsTypeId.Frozen = true;
			this.dgvReadyProductsTypeId.HeaderText = "IdType";
			this.dgvReadyProductsTypeId.Name = "dgvReadyProductsTypeId";
			this.dgvReadyProductsTypeId.ReadOnly = true;
			this.dgvReadyProductsTypeId.Visible = false;
			// 
			// dgvReadyProductsPrice
			// 
			this.dgvReadyProductsPrice.HeaderText = "Price";
			this.dgvReadyProductsPrice.Name = "dgvReadyProductsPrice";
			this.dgvReadyProductsPrice.ReadOnly = true;
			this.dgvReadyProductsPrice.Visible = false;
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel3.ColumnCount = 2;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
			this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel1, 0, 0);
			this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel8, 1, 0);
			this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 1;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(842, 510);
			this.tableLayoutPanel3.TabIndex = 1;
			// 
			// tableLayoutPanel8
			// 
			this.tableLayoutPanel8.ColumnCount = 1;
			this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel9, 0, 4);
			this.tableLayoutPanel8.Controls.Add(this.groupBox3, 0, 3);
			this.tableLayoutPanel8.Controls.Add(this.groupBox4, 0, 2);
			this.tableLayoutPanel8.Controls.Add(this.groupBox5, 0, 1);
			this.tableLayoutPanel8.Controls.Add(this.groupBox6, 0, 0);
			this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel8.Location = new System.Drawing.Point(592, 4);
			this.tableLayoutPanel8.Name = "tableLayoutPanel8";
			this.tableLayoutPanel8.RowCount = 5;
			this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
			this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
			this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
			this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
			this.tableLayoutPanel8.Size = new System.Drawing.Size(246, 502);
			this.tableLayoutPanel8.TabIndex = 1;
			// 
			// tableLayoutPanel9
			// 
			this.tableLayoutPanel9.ColumnCount = 2;
			this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel9.Controls.Add(this.createCocktail, 1, 0);
			this.tableLayoutPanel9.Controls.Add(this.cancelCreate, 0, 0);
			this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 425);
			this.tableLayoutPanel9.Name = "tableLayoutPanel9";
			this.tableLayoutPanel9.RowCount = 1;
			this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel9.Size = new System.Drawing.Size(240, 74);
			this.tableLayoutPanel9.TabIndex = 5;
			// 
			// createCocktail
			// 
			this.createCocktail.Dock = System.Windows.Forms.DockStyle.Fill;
			this.createCocktail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.createCocktail.Location = new System.Drawing.Point(123, 3);
			this.createCocktail.Name = "createCocktail";
			this.createCocktail.Size = new System.Drawing.Size(114, 68);
			this.createCocktail.TabIndex = 1;
			this.createCocktail.Text = "Создать коктейль";
			this.createCocktail.UseVisualStyleBackColor = true;
			this.createCocktail.Click += new System.EventHandler(this.createCocktail_Click);
			// 
			// cancelCreate
			// 
			this.cancelCreate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cancelCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.cancelCreate.Location = new System.Drawing.Point(3, 3);
			this.cancelCreate.Name = "cancelCreate";
			this.cancelCreate.Size = new System.Drawing.Size(114, 68);
			this.cancelCreate.TabIndex = 1;
			this.cancelCreate.Text = "Отмена";
			this.cancelCreate.UseVisualStyleBackColor = true;
			this.cancelCreate.Click += new System.EventHandler(this.cancelCreate_Click);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.labelPriceCocktail);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox3.Location = new System.Drawing.Point(3, 365);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(240, 54);
			this.groupBox3.TabIndex = 6;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Цена коктейля";
			// 
			// labelPriceCocktail
			// 
			this.labelPriceCocktail.Dock = System.Windows.Forms.DockStyle.Fill;
			this.labelPriceCocktail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelPriceCocktail.Location = new System.Drawing.Point(3, 16);
			this.labelPriceCocktail.Name = "labelPriceCocktail";
			this.labelPriceCocktail.Size = new System.Drawing.Size(234, 35);
			this.labelPriceCocktail.TabIndex = 0;
			this.labelPriceCocktail.Text = "00,00";
			this.labelPriceCocktail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.textAdditionalCharges);
			this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox4.Location = new System.Drawing.Point(3, 305);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
			this.groupBox4.Size = new System.Drawing.Size(240, 54);
			this.groupBox4.TabIndex = 2;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Наценка";
			// 
			// textAdditionalCharges
			// 
			this.textAdditionalCharges.DecimalPlaces = 2;
			this.textAdditionalCharges.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textAdditionalCharges.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textAdditionalCharges.Location = new System.Drawing.Point(10, 16);
			this.textAdditionalCharges.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
			this.textAdditionalCharges.Name = "textAdditionalCharges";
			this.textAdditionalCharges.Size = new System.Drawing.Size(220, 26);
			this.textAdditionalCharges.TabIndex = 6;
			this.textAdditionalCharges.ValueChanged += new System.EventHandler(this.textAdditionalCharges_ValueChanged);
			this.textAdditionalCharges.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textAdditionalCharges_KeyDown);
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.textDescriptionCocktail);
			this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox5.Location = new System.Drawing.Point(3, 63);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Padding = new System.Windows.Forms.Padding(10);
			this.groupBox5.Size = new System.Drawing.Size(240, 236);
			this.groupBox5.TabIndex = 3;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Описание Коктейля";
			// 
			// textDescriptionCocktail
			// 
			this.textDescriptionCocktail.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textDescriptionCocktail.Location = new System.Drawing.Point(10, 23);
			this.textDescriptionCocktail.MaxLength = 1024;
			this.textDescriptionCocktail.Multiline = true;
			this.textDescriptionCocktail.Name = "textDescriptionCocktail";
			this.textDescriptionCocktail.Size = new System.Drawing.Size(220, 203);
			this.textDescriptionCocktail.TabIndex = 0;
			// 
			// groupBox6
			// 
			this.groupBox6.Controls.Add(this.textNameCocktail);
			this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox6.Location = new System.Drawing.Point(3, 3);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 10, 3, 10);
			this.groupBox6.Size = new System.Drawing.Size(240, 54);
			this.groupBox6.TabIndex = 7;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Название Коктейля";
			// 
			// textNameCocktail
			// 
			this.textNameCocktail.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textNameCocktail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textNameCocktail.Location = new System.Drawing.Point(3, 23);
			this.textNameCocktail.Name = "textNameCocktail";
			this.textNameCocktail.Size = new System.Drawing.Size(234, 23);
			this.textNameCocktail.TabIndex = 0;
			// 
			// AddCocktailForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(842, 510);
			this.Controls.Add(this.tableLayoutPanel3);
			this.MinimumSize = new System.Drawing.Size(858, 548);
			this.Name = "AddCocktailForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Добавление Коктейлей";
			this.Load += new System.EventHandler(this.AddCocktailForm_Load);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.tableLayoutPanel5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewTare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewReadyTare)).EndInit();
			this.tableLayoutPanel7.ResumeLayout(false);
			this.panel6.ResumeLayout(false);
			this.tableLayoutPanel19.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel5.ResumeLayout(false);
			this.panel5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.textCountProducts)).EndInit();
			this.tableLayoutPanel2.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.textCountTare)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.tableLayoutPanel4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts)).EndInit();
			this.tableLayoutPanel6.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewReadyProducts)).EndInit();
			this.tableLayoutPanel3.ResumeLayout(false);
			this.tableLayoutPanel8.ResumeLayout(false);
			this.tableLayoutPanel9.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.textAdditionalCharges)).EndInit();
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			this.groupBox6.ResumeLayout(false);
			this.groupBox6.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.ComboBox comboBoxSearchProducts;
		private System.Windows.Forms.Button addProductToCocktail;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.NumericUpDown textCountProducts;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label labelCountProducts;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.ComboBox comboBoxSearchTare;
		private System.Windows.Forms.Button addTareToCocktail;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown textCountTare;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
		private System.Windows.Forms.Button addToReadyProducts;
		private System.Windows.Forms.Button deleteInReadyProducts;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
		private System.Windows.Forms.Button addToReadyTare;
		private System.Windows.Forms.Button deleteInReadyTare;
		private System.Windows.Forms.DataGridView dataGridViewReadyProducts;
		private System.Windows.Forms.DataGridView dataGridViewTare;
		private System.Windows.Forms.DataGridView dataGridViewReadyTare;
		private System.Windows.Forms.DataGridView dataGridViewProducts;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.Label labelPriceTare;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label labelPriceProduct;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.NumericUpDown textAdditionalCharges;
		private System.Windows.Forms.TextBox textDescriptionCocktail;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
		private System.Windows.Forms.Button createCocktail;
		private System.Windows.Forms.Button cancelCreate;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label labelPriceCocktail;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.TextBox textNameCocktail;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvListProductsId;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvListProductsName;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvListProductsType;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvListProductsQuantity;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvListProductsTypeId;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvListProductsPrice;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvReadyProductsId;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvReadyProductsName;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvReadyProductsType;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvReadyProductsQuantity;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvReadyProductsTypeId;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvReadyProductsPrice;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvListTareId;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvListTarePrice;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvListTareType;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvListTareQuantity;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvReadyTareId;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvReadyTarePrice;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvReadyTareType;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvReadyTareQuantity;
	}
}