﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using System.IO;
using System.Windows.Forms;

namespace Cocktails
{
	public class DataBaseWork
	{

		#region General

		public DataBaseWork()
		{
			String DbName = "DatabaseMain.mdf";
			String DbDirName = Path.GetDirectoryName(
				Path.GetDirectoryName(Path.GetDirectoryName(Application.ExecutablePath)));
			if (File.Exists(DbDirName + "\\" + DbName))
			{
				dataContext = new DataContext(String.Format(
					@"Data Source=(LocalDB)\v11.0;AttachDbFilename=""{0}\{1}"";Integrated Security=True",
					DbDirName,
					DbName
				));
			}
			else
			{
				dataContext = new DataContext(String.Format(
					@"Data Source=(LocalDB)\v11.0;AttachDbFilename=""{0}\{1}"";Integrated Security=True",
					Path.GetDirectoryName(Application.ExecutablePath),
					DbName
				));
			}
			if (!dataContext.DatabaseExists())
			{
				string message = "База данных не существует!";
				string caption = "Ошибка!";
				MessageBox.Show(message, caption, MessageBoxButtons.OK);
				Application.Exit();
			}
		}

		private DataContext dataContext;

		public DataContext GetDataContext()
		{
			return dataContext;
		}

		public WarehousemanClass GetUserWarehouseman(int id)
		{
			return (from elem in dataContext.GetTable<WarehousemanClass>()
				where elem.Id == id
				select elem).ToArray()[0];
		}

		#endregion

		#region Barman

		public BarmanClass GetUserBarman(int id)
		{
			return  (from elem in dataContext.GetTable<BarmanClass>()
				where elem.Id == id
				select elem).First();
		}

		public List<BarmanClass> GetBarmanList()
		{
			return (from elem in dataContext.GetTable<BarmanClass>()
					select elem).ToList<BarmanClass>();
		}

		public void AddBarmanToDatabase(BarmanClass barman)
		{
			dataContext.GetTable<BarmanClass>().InsertOnSubmit(barman);
			dataContext.SubmitChanges();
		}

		public void UpdateBarmanToDatabase(BarmanClass barman)
		{
			BarmanClass temp = (from elem in dataContext.GetTable<BarmanClass>()
					where elem.Id == barman.Id
					select elem).First();
			temp.name = barman.name;
			temp.password = barman.password;
			temp.salary = barman.salary;
			temp.tip = barman.tip;
			temp.skill = barman.skill;
			dataContext.SubmitChanges();
		}

		public bool IsExistBarmenNameInDatabase(string barmanName)
		{
			return ((from elem in dataContext.GetTable<BarmanClass>()
					 where elem.name.Trim().ToLower() == barmanName.Trim().ToLower()
					select elem).ToArray().Length != 0);
		}

		public void DeleteBarmanInDatabase(BarmanClass barman)
		{
			dataContext.GetTable<BarmanClass>().DeleteOnSubmit(
				(from elem in dataContext.GetTable<BarmanClass>()
					where elem.Id == barman.Id
					select elem).First());
			dataContext.SubmitChanges();
		}

		#endregion

		#region Products

		public List<Product> GetProductsList()
		{
			return (from elem in dataContext.GetTable<Product>()
					select elem).ToList<Product>();
		}

		public List<Product> GetProductsList(int typeId)
		{
			return (from elem in dataContext.GetTable<Product>()
					where elem.typeId == typeId
					select elem).ToList<Product>();
		}

		public Product GetProduct(int id)
		{
			return (from elem in dataContext.GetTable<Product>()
					where elem.Id == id
					select elem).First();
		}

		public void AddProductToDatabase(Product product)
		{
			dataContext.GetTable<Product>().InsertOnSubmit(product);
			dataContext.SubmitChanges();
		}

		public void UpdateProductToDatabase(Product product)
		{
			Product temp = (from elem in dataContext.GetTable<Product>()
							where elem.Id == product.Id
								select elem).First();
			temp.name = product.name;
			temp.price = product.price;
			temp.quantity = product.quantity;
			temp.TypeProduct = GetTypeProduct(product.typeId);
			dataContext.SubmitChanges();
		}

		public bool IsExistProductInDatabase(Product product)
		{
			return ((from elem in dataContext.GetTable<Product>()
					 where (elem.name.Trim().ToLower() == product.name.Trim().ToLower() &&
						elem.typeId == product.typeId)
					 select elem).ToArray().Length != 0);
		}

		public void DeleteProductInDatabase(Product product)
		{
			dataContext.GetTable<Product>().DeleteOnSubmit(
				(from elem in dataContext.GetTable<Product>()
				 where elem.Id == product.Id
				 select elem).First());
			dataContext.SubmitChanges();
		}

		public void DeleteProductInDatabaseByType(TypeProduct typeProduct)
		{
			dataContext.GetTable<Product>().DeleteOnSubmit(
				(from elem in typeProduct.Product
				 select elem).First());
			dataContext.SubmitChanges();
		}

		#endregion

		#region TypeProducts

		public int GetTypeProductId(string name)
		{
			return (from elem in dataContext.GetTable<TypeProduct>()
					where elem.name.Trim().ToLower() == name.Trim().ToLower()
					select elem).First().Id;
		}

		public TypeProduct GetTypeProduct(string name)
		{
			return (from elem in dataContext.GetTable<TypeProduct>()
					where elem.name.Trim().ToLower() == name.Trim().ToLower()
					select elem).First();
		}

		public TypeProduct GetTypeProduct(int id)
		{
			return (from elem in dataContext.GetTable<TypeProduct>()
					where elem.Id == id
					select elem).First();
		}

		public List<TypeProduct> GetTypeProductsList()
		{
			return (from elem in dataContext.GetTable<TypeProduct>()
					select elem).ToList<TypeProduct>();
		}

		public void AddTypeProductToDatabase(TypeProduct typeProduct)
		{
			dataContext.GetTable<TypeProduct>().InsertOnSubmit(typeProduct);
			dataContext.SubmitChanges();
		}

		public void UpdateTypeProductToDatabase(TypeProduct typeProduct)
		{
			TypeProduct temp = (from elem in dataContext.GetTable<TypeProduct>()
								where elem.Id == typeProduct.Id
								select elem).First();
			temp.name = typeProduct.name;
			temp.unit = typeProduct.unit;
			dataContext.SubmitChanges();
		}

		public bool IsExistTypeProductInDatabase(string typeProductName)
		{
			return ((from elem in dataContext.GetTable<TypeProduct>()
					 where elem.name.Trim().ToLower() == typeProductName.ToLower()
					 select elem).ToArray().Length != 0);
		}

		private bool IsPreepareDeleteTypeProduct(TypeProduct typeProduct)
		{
			typeProduct = GetTypeProduct(typeProduct.Id);
			if (typeProduct.Product.Count > 0)
			{
				string text = "Удаление этого типа продукта приведет к удалению" +
					" привязаных к нему продуктов, продолжить удаление?";
				if (CocktailsForm.ShowMessageQuestion(text) == DialogResult.Yes)
				{
					DeleteProductInDatabaseByType(typeProduct);
				}
				else
				{
					return false;
				}
			}
			return true;
		}

		public void DeleteTypeProductInDatabase(TypeProduct typeProduct)
		{
			if (IsPreepareDeleteTypeProduct(typeProduct))
			{
				dataContext.GetTable<TypeProduct>().DeleteOnSubmit(
					(from elem in dataContext.GetTable<TypeProduct>()
					 where elem.Id == typeProduct.Id
					 select elem).First());
				dataContext.SubmitChanges();
			}
		}

		#endregion

		#region Tare

		public List<Tare> GetTareList()
		{
			return (from elem in dataContext.GetTable<Tare>()
					select elem).ToList<Tare>();
		}

		public Tare GetTare(int id)
		{
			return (from elem in dataContext.GetTable<Tare>()
					where elem.Id == id
					select elem).First();
		}

		public void AddTareToDatabase(Tare tare)
		{
			dataContext.GetTable<Tare>().InsertOnSubmit(tare);
			dataContext.SubmitChanges();
		}

		public void UpdateTareToDatabase(Tare tare)
		{
			Tare temp = (from elem in dataContext.GetTable<Tare>()
							where elem.Id == tare.Id
							select elem).First();
			temp.type = tare.type;
			temp.price = tare.price;
			temp.quantity = tare.quantity;
			temp.reloading = tare.reloading;
			dataContext.SubmitChanges();
		}

		public bool IsExistTareInDatabase(string type)
		{
			return ((from elem in dataContext.GetTable<Tare>()
					 where elem.type.Trim().ToLower() == type.Trim().ToLower()
					 select elem).ToArray().Length != 0);
		}

		public void DeleteTareInDatabase(Tare tare)
		{
			dataContext.GetTable<Tare>().DeleteOnSubmit(
				(from elem in dataContext.GetTable<Tare>()
				 where elem.Id == tare.Id
				 select elem).First());
			dataContext.SubmitChanges();
		}

		#endregion

		#region Recipes

		public List<Recipe> GetRecipeList()
		{
			return (from elem in dataContext.GetTable<Recipe>()
					select elem).ToList<Recipe>();
		}

		public Recipe GetRecipe(int id)
		{
			return (from elem in dataContext.GetTable<Recipe>()
					where elem.Id == id
					select elem).First();
		}

		public void AddRecipeToDatabase(Recipe recipe)
		{
			dataContext.GetTable<Recipe>().InsertOnSubmit(recipe);
			dataContext.SubmitChanges();
		}

		public void UpdateRecipeToDatabase(Recipe recipe)
		{
			Recipe temp = (from elem in dataContext.GetTable<Recipe>()
						 where elem.Id == recipe.Id
						 select elem).First();
			temp.name = recipe.name;
			temp.popularity = recipe.popularity;
			temp.process = recipe.process;
			temp.description = recipe.description;
			temp.complication = recipe.complication;
			temp.additional_charges = temp.additional_charges;
			dataContext.SubmitChanges();
		}

		public bool IsExistRecipeInDatabase(string name)
		{
			return ((from elem in dataContext.GetTable<Recipe>()
					 where elem.name.Trim().ToLower() == name.Trim().ToLower()
					 select elem).ToArray().Length != 0);
		}

		public void DeleteRecipeInDatabase(Recipe recipe)
		{
			dataContext.GetTable<Recipe>().DeleteOnSubmit(
				(from elem in dataContext.GetTable<Recipe>()
				 where elem.Id == recipe.Id
				 select elem).First());
			dataContext.SubmitChanges();
		}

		#endregion

	}
}
