﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cocktails
{
	public partial class AddTypeProductForm : Form
	{

		bool isTypeProductAddState = false;
		string editTypeProductName = String.Empty;
		int idEditTypeProduct = 0;
		DataBaseWork database = new DataBaseWork();
		string tempName = String.Empty;
		string tempUnit = String.Empty;

		public AddTypeProductForm(DataBaseWork database, 
			string tempName = "", string tempUnit = "",
			int idEditTypeProduct = 0, bool isTypeProductAddState = true)
		{
			InitializeComponent();
			this.isTypeProductAddState = isTypeProductAddState;
			this.database = database;
			this.idEditTypeProduct = idEditTypeProduct;
			this.tempName = tempName;
			this.tempUnit = tempUnit;
		}

		private void cancel_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}

		private bool IsFullTypeProductForm()
		{
			return (textNameTypeProduct.Text.Length > 0 &&
				textUnitTypeProduct.Text.Length > 0);
		}

		private TypeProduct GetTypeProductFromForm()
		{
			TypeProduct typeProduct = new TypeProduct();
			typeProduct.name = textNameTypeProduct.Text;
			typeProduct.unit = textUnitTypeProduct.Text;
			return typeProduct;
		}

		private void addTare_Click(object sender, EventArgs e)
		{
			if (IsFullTypeProductForm())
			{
				if (isTypeProductAddState == true)
				{
					if (database.IsExistTypeProductInDatabase(textNameTypeProduct.Text) == false)
					{
						database.AddTypeProductToDatabase(GetTypeProductFromForm());
						DialogResult = System.Windows.Forms.DialogResult.OK;
					}
					else
					{
						CocktailsForm.ShowMessage("Такой тип уже существует!");
					}
				}
				else
				{
					if (editTypeProductName.ToLower() != textNameTypeProduct.Text.ToLower())
					{
						if (database.IsExistTypeProductInDatabase(textNameTypeProduct.Text) == false)
						{
							TypeProduct typeProduct = GetTypeProductFromForm();
							typeProduct.Id = idEditTypeProduct;
							database.UpdateTypeProductToDatabase(typeProduct);
							DialogResult = System.Windows.Forms.DialogResult.OK;
						}
						else
						{
							CocktailsForm.ShowMessage("Такой тип уже существует!");
						}
					}
					else
					{
						TypeProduct typeProduct = GetTypeProductFromForm();
						typeProduct.Id = idEditTypeProduct;
						database.UpdateTypeProductToDatabase(typeProduct);
						DialogResult = System.Windows.Forms.DialogResult.OK;
					}
				}
			}
			else
			{
				CocktailsForm.ShowMessage("Не все поля заполнены!");
			}
		}

		private void importCSV_Click(object sender, EventArgs e)
		{

		}

		private void AddTypeProductForm_Shown(object sender, EventArgs e)
		{
			textNameTypeProduct.Text = tempName;
			textUnitTypeProduct.Text = tempUnit;
			editTypeProductName = tempName;
		}
	}
}
