﻿namespace Cocktails
{
	partial class AddTypeProductForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.textNameTypeProduct = new System.Windows.Forms.TextBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.textUnitTypeProduct = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.cancel = new System.Windows.Forms.Button();
			this.importCSV = new System.Windows.Forms.Button();
			this.addTare = new System.Windows.Forms.Button();
			this.tableLayoutPanel1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(412, 148);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.textNameTypeProduct);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(406, 43);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Тип продукта";
			// 
			// textNameTypeProduct
			// 
			this.textNameTypeProduct.Location = new System.Drawing.Point(3, 16);
			this.textNameTypeProduct.Name = "textNameTypeProduct";
			this.textNameTypeProduct.Size = new System.Drawing.Size(400, 20);
			this.textNameTypeProduct.TabIndex = 0;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.textUnitTypeProduct);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(3, 52);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(406, 43);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Единица измерения типа";
			// 
			// textUnitTypeProduct
			// 
			this.textUnitTypeProduct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textUnitTypeProduct.Location = new System.Drawing.Point(3, 16);
			this.textUnitTypeProduct.Name = "textUnitTypeProduct";
			this.textUnitTypeProduct.Size = new System.Drawing.Size(400, 20);
			this.textUnitTypeProduct.TabIndex = 1;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 3;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.tableLayoutPanel2.Controls.Add(this.cancel, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.importCSV, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.addTare, 2, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 101);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 1;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(406, 44);
			this.tableLayoutPanel2.TabIndex = 4;
			// 
			// cancel
			// 
			this.cancel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cancel.Location = new System.Drawing.Point(3, 3);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(129, 38);
			this.cancel.TabIndex = 0;
			this.cancel.Text = "Отмена";
			this.cancel.UseVisualStyleBackColor = true;
			this.cancel.Click += new System.EventHandler(this.cancel_Click);
			// 
			// importCSV
			// 
			this.importCSV.Dock = System.Windows.Forms.DockStyle.Fill;
			this.importCSV.Location = new System.Drawing.Point(138, 3);
			this.importCSV.Name = "importCSV";
			this.importCSV.Size = new System.Drawing.Size(129, 38);
			this.importCSV.TabIndex = 1;
			this.importCSV.Text = "Импорт с CSV";
			this.importCSV.UseVisualStyleBackColor = true;
			this.importCSV.Click += new System.EventHandler(this.importCSV_Click);
			// 
			// addTare
			// 
			this.addTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addTare.Location = new System.Drawing.Point(273, 3);
			this.addTare.Name = "addTare";
			this.addTare.Size = new System.Drawing.Size(130, 38);
			this.addTare.TabIndex = 2;
			this.addTare.Text = "Сохранить";
			this.addTare.UseVisualStyleBackColor = true;
			this.addTare.Click += new System.EventHandler(this.addTare_Click);
			// 
			// AddTypeProductForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(412, 148);
			this.Controls.Add(this.tableLayoutPanel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "AddTypeProductForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Добавление Типа продукта";
			this.Shown += new System.EventHandler(this.AddTypeProductForm_Shown);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.tableLayoutPanel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.Button importCSV;
		private System.Windows.Forms.Button addTare;
		private System.Windows.Forms.TextBox textNameTypeProduct;
		private System.Windows.Forms.TextBox textUnitTypeProduct;
	}
}