﻿namespace Cocktails
{
	partial class ReplaceElementForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.dataGridViewReadyElements = new System.Windows.Forms.DataGridView();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.dataGridViewListElements = new System.Windows.Forms.DataGridView();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.buttonAddElement = new System.Windows.Forms.Button();
			this.buttonDeleteElement = new System.Windows.Forms.Button();
			this.labelMissingElement = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.checkBoxReExecute = new System.Windows.Forms.CheckBox();
			this.buttonExecute = new System.Windows.Forms.Button();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewReadyElements)).BeginInit();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewListElements)).BeginInit();
			this.tableLayoutPanel3.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.labelMissingElement, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 2);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(830, 410);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 3;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.44444F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.44444F));
			this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.groupBox2, 2, 0);
			this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 50);
			this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 1;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(830, 310);
			this.tableLayoutPanel2.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.dataGridViewReadyElements);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(362, 304);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Продукты замены";
			// 
			// dataGridViewReadyElements
			// 
			this.dataGridViewReadyElements.AllowUserToAddRows = false;
			this.dataGridViewReadyElements.AllowUserToResizeRows = false;
			this.dataGridViewReadyElements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewReadyElements.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewReadyElements.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dataGridViewReadyElements.Location = new System.Drawing.Point(3, 16);
			this.dataGridViewReadyElements.MultiSelect = false;
			this.dataGridViewReadyElements.Name = "dataGridViewReadyElements";
			this.dataGridViewReadyElements.ReadOnly = true;
			this.dataGridViewReadyElements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewReadyElements.Size = new System.Drawing.Size(356, 285);
			this.dataGridViewReadyElements.TabIndex = 0;
			this.dataGridViewReadyElements.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewReadyElements_CellDoubleClick);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.dataGridViewListElements);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(463, 3);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(364, 304);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Предложения для замены";
			// 
			// dataGridViewListElements
			// 
			this.dataGridViewListElements.AllowUserToAddRows = false;
			this.dataGridViewListElements.AllowUserToDeleteRows = false;
			this.dataGridViewListElements.AllowUserToResizeRows = false;
			this.dataGridViewListElements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewListElements.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewListElements.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dataGridViewListElements.Location = new System.Drawing.Point(3, 16);
			this.dataGridViewListElements.MultiSelect = false;
			this.dataGridViewListElements.Name = "dataGridViewListElements";
			this.dataGridViewListElements.ReadOnly = true;
			this.dataGridViewListElements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewListElements.Size = new System.Drawing.Size(358, 285);
			this.dataGridViewListElements.TabIndex = 0;
			this.dataGridViewListElements.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewListElements_CellDoubleClick);
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.ColumnCount = 1;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.Controls.Add(this.buttonAddElement, 0, 1);
			this.tableLayoutPanel3.Controls.Add(this.buttonDeleteElement, 0, 2);
			this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel3.Location = new System.Drawing.Point(371, 3);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 4;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(86, 304);
			this.tableLayoutPanel3.TabIndex = 2;
			// 
			// buttonAddElement
			// 
			this.buttonAddElement.Dock = System.Windows.Forms.DockStyle.Fill;
			this.buttonAddElement.Location = new System.Drawing.Point(3, 115);
			this.buttonAddElement.Name = "buttonAddElement";
			this.buttonAddElement.Size = new System.Drawing.Size(80, 34);
			this.buttonAddElement.TabIndex = 0;
			this.buttonAddElement.Text = "Добавить";
			this.buttonAddElement.UseVisualStyleBackColor = true;
			this.buttonAddElement.Click += new System.EventHandler(this.buttonAddElement_Click);
			// 
			// buttonDeleteElement
			// 
			this.buttonDeleteElement.Dock = System.Windows.Forms.DockStyle.Fill;
			this.buttonDeleteElement.Location = new System.Drawing.Point(3, 155);
			this.buttonDeleteElement.Name = "buttonDeleteElement";
			this.buttonDeleteElement.Size = new System.Drawing.Size(80, 34);
			this.buttonDeleteElement.TabIndex = 1;
			this.buttonDeleteElement.Text = "Удалить";
			this.buttonDeleteElement.UseVisualStyleBackColor = true;
			this.buttonDeleteElement.Click += new System.EventHandler(this.buttonDeleteElement_Click);
			// 
			// labelMissingElement
			// 
			this.labelMissingElement.Dock = System.Windows.Forms.DockStyle.Fill;
			this.labelMissingElement.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelMissingElement.Location = new System.Drawing.Point(3, 0);
			this.labelMissingElement.Name = "labelMissingElement";
			this.labelMissingElement.Size = new System.Drawing.Size(824, 50);
			this.labelMissingElement.TabIndex = 1;
			this.labelMissingElement.Text = "Отсутствующий продукт: ";
			this.labelMissingElement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.checkBoxReExecute);
			this.panel1.Controls.Add(this.buttonExecute);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(3, 363);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(824, 44);
			this.panel1.TabIndex = 2;
			// 
			// checkBoxReExecute
			// 
			this.checkBoxReExecute.AutoSize = true;
			this.checkBoxReExecute.Location = new System.Drawing.Point(9, 15);
			this.checkBoxReExecute.Name = "checkBoxReExecute";
			this.checkBoxReExecute.Size = new System.Drawing.Size(282, 17);
			this.checkBoxReExecute.TabIndex = 1;
			this.checkBoxReExecute.Text = "Выполнять везде где отсутствует данный продукт";
			this.checkBoxReExecute.UseVisualStyleBackColor = true;
			// 
			// buttonExecute
			// 
			this.buttonExecute.Dock = System.Windows.Forms.DockStyle.Right;
			this.buttonExecute.Location = new System.Drawing.Point(674, 0);
			this.buttonExecute.Name = "buttonExecute";
			this.buttonExecute.Size = new System.Drawing.Size(150, 44);
			this.buttonExecute.TabIndex = 0;
			this.buttonExecute.Text = "Заменить";
			this.buttonExecute.UseVisualStyleBackColor = true;
			this.buttonExecute.Click += new System.EventHandler(this.buttonExecute_Click);
			// 
			// ReplaceElementForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(830, 410);
			this.Controls.Add(this.tableLayoutPanel1);
			this.MinimumSize = new System.Drawing.Size(846, 448);
			this.Name = "ReplaceElementForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Замена отсутствуещего продукта | Коктейль: коктейль";
			this.Load += new System.EventHandler(this.ReplaceElementForm_Load);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewReadyElements)).EndInit();
			this.groupBox2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewListElements)).EndInit();
			this.tableLayoutPanel3.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label labelMissingElement;
		private System.Windows.Forms.DataGridView dataGridViewReadyElements;
		private System.Windows.Forms.DataGridView dataGridViewListElements;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button buttonExecute;
		private System.Windows.Forms.CheckBox checkBoxReExecute;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.Button buttonAddElement;
		private System.Windows.Forms.Button buttonDeleteElement;
	}
}