﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cocktails
{
	public partial class DoOrderForm : Form
	{
		ProcessingCocktail cocktail;
		bool infoMode = false;

		public DoOrderForm(bool infoMode = false)
		{
			InitializeComponent();
			this.infoMode = infoMode;
		}

		private void DoOrderForm_Load(object sender, EventArgs e)
		{
			this.AllowTransparency = true;
			this.BackColor = Color.Orange;
			this.TransparencyKey = BackColor;
			this.Dock = DockStyle.Fill;
			WindowState = FormWindowState.Maximized;
			this.KeyPreview = true;
			if (infoMode == false)
			{
				buttonCompleteOrder.Focus();
			}
			else
			{
				buttonCompleteOrder.Visible = false;
				buttonUndoOrder.Visible = false;
				buttonCancelOrder.Text = "Назад";
			}
			cocktail = CocktailsForm.currentCocktail;
		}

		private void tableLayoutPanel1_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.No;
		}

		private void DoOrderForm_KeyDown(object sender, KeyEventArgs e)
		{
			if (infoMode == false)
			{
				switch (e.KeyCode)
				{
					case Keys.Enter: DialogResult = System.Windows.Forms.DialogResult.OK;
						break;
					case Keys.Space: DialogResult = System.Windows.Forms.DialogResult.Cancel;
						break;
					case Keys.Escape: DialogResult = System.Windows.Forms.DialogResult.No;
						break;
					default:
						break;
				}
			}
			else
			{
				DialogResult = System.Windows.Forms.DialogResult.Cancel;
			}
		}

		private void buttonCompleteOrder_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.OK;
		}

		private void buttonCancelOrder_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}

		private void buttonUndoOrder_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.No;
		}

		private void DoOrderForm_Shown(object sender, EventArgs e)
		{
			labelCocktailName.Text = cocktail.GetCocktailName();
			labelCocktailPrice.Text = cocktail.GetCocktailPriceString();
			labelRecipeText.Text = cocktail.GetRecipeString();
			labelDiscriptionText.Text = cocktail.GetFullDiscription();
		}
	}
}
