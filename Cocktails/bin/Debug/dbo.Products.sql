﻿CREATE TABLE [dbo].[Products]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [name] NCHAR(100) NOT NULL, 
    [price] MONEY NOT NULL DEFAULT 0, 
    [type] NCHAR(100) NOT NULL, 
    [quantity] INT NOT NULL DEFAULT 0
)
