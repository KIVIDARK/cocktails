﻿namespace Cocktails
{
	partial class DoOrderForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.buttonUndoOrder = new System.Windows.Forms.Button();
			this.buttonCancelOrder = new System.Windows.Forms.Button();
			this.buttonCompleteOrder = new System.Windows.Forms.Button();
			this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
			this.labelCocktailName = new System.Windows.Forms.Label();
			this.labelCocktailPrice = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.labelRecipeText = new System.Windows.Forms.Label();
			this.labelDiscriptionText = new System.Windows.Forms.Label();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.tableLayoutPanel3.SuspendLayout();
			this.tableLayoutPanel4.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
			this.tableLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(1024, 600);
			this.tableLayoutPanel1.TabIndex = 0;
			this.tableLayoutPanel1.Click += new System.EventHandler(this.tableLayoutPanel1_Click);
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.BackColor = System.Drawing.SystemColors.ControlLight;
			this.tableLayoutPanel2.ColumnCount = 1;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 1);
			this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(153, 90);
			this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 2;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(716, 420);
			this.tableLayoutPanel2.TabIndex = 0;
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.ColumnCount = 3;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel3.Controls.Add(this.buttonUndoOrder, 0, 0);
			this.tableLayoutPanel3.Controls.Add(this.buttonCancelOrder, 1, 0);
			this.tableLayoutPanel3.Controls.Add(this.buttonCompleteOrder, 2, 0);
			this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 323);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 1;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(710, 94);
			this.tableLayoutPanel3.TabIndex = 0;
			// 
			// buttonUndoOrder
			// 
			this.buttonUndoOrder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.buttonUndoOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonUndoOrder.Location = new System.Drawing.Point(10, 10);
			this.buttonUndoOrder.Margin = new System.Windows.Forms.Padding(10);
			this.buttonUndoOrder.Name = "buttonUndoOrder";
			this.buttonUndoOrder.Size = new System.Drawing.Size(216, 74);
			this.buttonUndoOrder.TabIndex = 0;
			this.buttonUndoOrder.Text = "Отмена заказа (Esc)";
			this.buttonUndoOrder.UseVisualStyleBackColor = true;
			this.buttonUndoOrder.Click += new System.EventHandler(this.buttonUndoOrder_Click);
			// 
			// buttonCancelOrder
			// 
			this.buttonCancelOrder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.buttonCancelOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonCancelOrder.Location = new System.Drawing.Point(246, 10);
			this.buttonCancelOrder.Margin = new System.Windows.Forms.Padding(10);
			this.buttonCancelOrder.Name = "buttonCancelOrder";
			this.buttonCancelOrder.Size = new System.Drawing.Size(216, 74);
			this.buttonCancelOrder.TabIndex = 1;
			this.buttonCancelOrder.Text = "В очередь (Space)";
			this.buttonCancelOrder.UseVisualStyleBackColor = true;
			this.buttonCancelOrder.Click += new System.EventHandler(this.buttonCancelOrder_Click);
			// 
			// buttonCompleteOrder
			// 
			this.buttonCompleteOrder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.buttonCompleteOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonCompleteOrder.Location = new System.Drawing.Point(482, 10);
			this.buttonCompleteOrder.Margin = new System.Windows.Forms.Padding(10);
			this.buttonCompleteOrder.Name = "buttonCompleteOrder";
			this.buttonCompleteOrder.Size = new System.Drawing.Size(218, 74);
			this.buttonCompleteOrder.TabIndex = 2;
			this.buttonCompleteOrder.Text = "Заказ выполнен (Enter)";
			this.buttonCompleteOrder.UseVisualStyleBackColor = true;
			this.buttonCompleteOrder.Click += new System.EventHandler(this.buttonCompleteOrder_Click);
			// 
			// tableLayoutPanel4
			// 
			this.tableLayoutPanel4.ColumnCount = 2;
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel4.Controls.Add(this.labelCocktailPrice, 1, 0);
			this.tableLayoutPanel4.Controls.Add(this.labelCocktailName, 0, 0);
			this.tableLayoutPanel4.Controls.Add(this.groupBox1, 0, 1);
			this.tableLayoutPanel4.Controls.Add(this.groupBox2, 1, 1);
			this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel4.Name = "tableLayoutPanel4";
			this.tableLayoutPanel4.RowCount = 2;
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel4.Size = new System.Drawing.Size(716, 320);
			this.tableLayoutPanel4.TabIndex = 1;
			// 
			// labelCocktailName
			// 
			this.labelCocktailName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.labelCocktailName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelCocktailName.Location = new System.Drawing.Point(3, 3);
			this.labelCocktailName.Margin = new System.Windows.Forms.Padding(3);
			this.labelCocktailName.Name = "labelCocktailName";
			this.labelCocktailName.Size = new System.Drawing.Size(352, 34);
			this.labelCocktailName.TabIndex = 0;
			this.labelCocktailName.Text = "Коктейль: ";
			this.labelCocktailName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// labelCocktailPrice
			// 
			this.labelCocktailPrice.Dock = System.Windows.Forms.DockStyle.Fill;
			this.labelCocktailPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelCocktailPrice.Location = new System.Drawing.Point(361, 3);
			this.labelCocktailPrice.Margin = new System.Windows.Forms.Padding(3);
			this.labelCocktailPrice.Name = "labelCocktailPrice";
			this.labelCocktailPrice.Size = new System.Drawing.Size(352, 34);
			this.labelCocktailPrice.TabIndex = 1;
			this.labelCocktailPrice.Text = "Цена: ";
			this.labelCocktailPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.labelRecipeText);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.groupBox1.Location = new System.Drawing.Point(3, 43);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(352, 274);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Рецепт";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.labelDiscriptionText);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.groupBox2.Location = new System.Drawing.Point(361, 43);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(352, 274);
			this.groupBox2.TabIndex = 3;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Описание";
			// 
			// labelRecipeText
			// 
			this.labelRecipeText.Dock = System.Windows.Forms.DockStyle.Fill;
			this.labelRecipeText.Location = new System.Drawing.Point(3, 17);
			this.labelRecipeText.Name = "labelRecipeText";
			this.labelRecipeText.Size = new System.Drawing.Size(346, 254);
			this.labelRecipeText.TabIndex = 0;
			this.labelRecipeText.Text = "Рецепт Рецепт\r\nРецепт Рецепт\r\nРецепт Рецепт\r\nРецепт Рецепт\r\nРецепт Рецепт";
			// 
			// labelDiscriptionText
			// 
			this.labelDiscriptionText.Dock = System.Windows.Forms.DockStyle.Fill;
			this.labelDiscriptionText.Location = new System.Drawing.Point(3, 17);
			this.labelDiscriptionText.Name = "labelDiscriptionText";
			this.labelDiscriptionText.Size = new System.Drawing.Size(346, 254);
			this.labelDiscriptionText.TabIndex = 0;
			this.labelDiscriptionText.Text = "Описание Описание\r\nОписание Описание\r\nОписание Описание\r\nОписание Описание\r\nОписа" +
    "ние Описание";
			// 
			// DoOrderForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(1024, 600);
			this.Controls.Add(this.tableLayoutPanel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "DoOrderForm";
			this.Text = "DoOrderForm";
			this.TransparencyKey = System.Drawing.Color.Transparent;
			this.Load += new System.EventHandler(this.DoOrderForm_Load);
			this.Shown += new System.EventHandler(this.DoOrderForm_Shown);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DoOrderForm_KeyDown);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel3.ResumeLayout(false);
			this.tableLayoutPanel4.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.Button buttonUndoOrder;
		private System.Windows.Forms.Button buttonCancelOrder;
		private System.Windows.Forms.Button buttonCompleteOrder;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
		private System.Windows.Forms.Label labelCocktailName;
		private System.Windows.Forms.Label labelCocktailPrice;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label labelRecipeText;
		private System.Windows.Forms.Label labelDiscriptionText;
	}
}