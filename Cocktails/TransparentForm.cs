﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cocktails
{
	public partial class TransparentForm : Form
	{
		public TransparentForm()
		{
			InitializeComponent();
		}

		private void TransparentForm_Load(object sender, EventArgs e)
		{
			WindowState = FormWindowState.Maximized;
			DoOrderForm doOrderForm = new DoOrderForm();
			doOrderForm.ShowDialog();
			DialogResult = doOrderForm.DialogResult;
		}
	}
}
