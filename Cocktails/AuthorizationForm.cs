﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Cocktails
{
	public partial class AuthorizationForm : Form
	{
		public AuthorizationForm(DataContext dataContext)
		{
			InitializeComponent();
			this.dataContext = dataContext;
		}

		DataContext dataContext;

		Table<WarehousemanClass> warehousemans;
		Table<BarmanClass> barmanClass;

		bool isWarehouseman = false;
		int id = 0;

		private void AuthorizationForm_Load(object sender, EventArgs e)
		{
			warehousemans = dataContext.GetTable<WarehousemanClass>();
			barmanClass = dataContext.GetTable<BarmanClass>();
		}

		private void Authorization()
		{
			var compareWarehouseman = from elem in warehousemans
									  where (elem.name.Trim() == textNameUser.Text &&
										  elem.password.Trim() == textPasswordUser.Text)
									  select elem.Id;
			var compareBarman = from elem in barmanClass
								where (elem.name.Trim() == textNameUser.Text &&
									elem.password.Trim() == textPasswordUser.Text)
								select elem.Id;
			if (compareWarehouseman.ToArray().Length != 0)
			{
				isWarehouseman = true;
				id = compareWarehouseman.ToArray()[0];
				DialogResult = System.Windows.Forms.DialogResult.OK;
			}
			else
				if (compareBarman.ToArray().Length != 0)
				{
					isWarehouseman = false;
					id = compareBarman.ToArray()[0];
					DialogResult = System.Windows.Forms.DialogResult.OK;
				}
				else
				{
					labelError.Text = "Неверный логин или пароль!";
				}
		}

		private void comeIn_Click(object sender, EventArgs e)
		{
			Authorization();
		}

		public bool IsWarehouseman()
		{
			return isWarehouseman;
		}

		public int GetUserId()
		{
			return id;
		}

		private void textPasswordUser_KeyPress(object sender, KeyPressEventArgs e)
		{
			if ((Keys)e.KeyChar == Keys.Enter)
			{
				Authorization();
			}
		}
	}
}
