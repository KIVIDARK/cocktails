﻿namespace Cocktails
{
	partial class CocktailsForm
	{
		/// <summary>
		/// Требуется переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Обязательный метод для поддержки конструктора - не изменяйте
		/// содержимое данного метода при помощи редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabOrders = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.comboBoxSearch = new System.Windows.Forms.ComboBox();
			this.inQueue = new System.Windows.Forms.Button();
			this.completeOrder = new System.Windows.Forms.Button();
			this.panel5 = new System.Windows.Forms.Panel();
			this.textCountOrders = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.dataGridViewListCocktails = new System.Windows.Forms.DataGridView();
			this.dgvCocktailsListId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvCocktailsListName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvCocktailsListPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvCocktailsListPopularity = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
			this.dataGridViewQueue = new System.Windows.Forms.DataGridView();
			this.dgvOrderId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvOrderName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvOrderPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.panel7 = new System.Windows.Forms.Panel();
			this.currentCocktailLabel = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
			this.undoOrder = new System.Windows.Forms.Button();
			this.infoOrder = new System.Windows.Forms.Button();
			this.tabCocktails = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.dataGridViewCocktails = new System.Windows.Forms.DataGridView();
			this.dgvCocktailsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvCocktailsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
			this.addCocktail = new System.Windows.Forms.Button();
			this.editCocktail = new System.Windows.Forms.Button();
			this.deleteCocktail = new System.Windows.Forms.Button();
			this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.descriptionLabel = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.processLabel = new System.Windows.Forms.Label();
			this.tabProducts = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.dataGridViewProducts = new System.Windows.Forms.DataGridView();
			this.dgvProductId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvProductTypeProductId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvProductType = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvProductQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvProductPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panelEditProducts = new System.Windows.Forms.TableLayoutPanel();
			this.saveProduct = new System.Windows.Forms.Button();
			this.tableLayoutPanelProduct = new System.Windows.Forms.TableLayoutPanel();
			this.undoProduct = new System.Windows.Forms.Button();
			this.importProduct = new System.Windows.Forms.Button();
			this.groupBoxProductQuantity = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
			this.textQuantityProduct = new System.Windows.Forms.NumericUpDown();
			this.quantityTypeLabel = new System.Windows.Forms.Label();
			this.groupBoxProductTypeProduct = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
			this.textTypeProduct = new System.Windows.Forms.ComboBox();
			this.groupBoxProductPrice = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
			this.textPriceProduct = new System.Windows.Forms.NumericUpDown();
			this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
			this.addProduct = new System.Windows.Forms.Button();
			this.deleteProduct = new System.Windows.Forms.Button();
			this.groupBoxProductName = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
			this.textNameProduct = new System.Windows.Forms.TextBox();
			this.tabResurse = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
			this.panelEditTares = new System.Windows.Forms.TableLayoutPanel();
			this.addTare = new System.Windows.Forms.Button();
			this.editTare = new System.Windows.Forms.Button();
			this.deleteTare = new System.Windows.Forms.Button();
			this.dataGridViewTare = new System.Windows.Forms.DataGridView();
			this.dgvTareId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvTareType = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvTareQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvTarePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvTareReloading = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
			this.panelEditTypeProducts = new System.Windows.Forms.TableLayoutPanel();
			this.addType = new System.Windows.Forms.Button();
			this.editType = new System.Windows.Forms.Button();
			this.deleteType = new System.Windows.Forms.Button();
			this.dataGridViewTypeProducts = new System.Windows.Forms.DataGridView();
			this.dgvTypeProductId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvTypeProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvTypeProductUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tabAdmin = new System.Windows.Forms.TabPage();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBoxBarmanSkill = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel31 = new System.Windows.Forms.TableLayoutPanel();
			this.textBarmanSkill = new System.Windows.Forms.NumericUpDown();
			this.groupBoxBarmanTip = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
			this.textBarmanTip = new System.Windows.Forms.NumericUpDown();
			this.groupBoxBarmanSalary = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
			this.textBarmanSalary = new System.Windows.Forms.NumericUpDown();
			this.groupBoxBarmanPassword = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
			this.textBarmanPassword = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel29 = new System.Windows.Forms.TableLayoutPanel();
			this.newBarman = new System.Windows.Forms.Button();
			this.deleteBarman = new System.Windows.Forms.Button();
			this.groupBoxBarmanName = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel30 = new System.Windows.Forms.TableLayoutPanel();
			this.textBarmanName = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
			this.saveBarman = new System.Windows.Forms.Button();
			this.undoBarman = new System.Windows.Forms.Button();
			this.dataGridViewListBarmen = new System.Windows.Forms.DataGridView();
			this.dgvBarmanId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvBarmanName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvBarmanPassword = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvBarmanSalary = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvBarmanTip = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvBarmanSkill = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
			this.panel6 = new System.Windows.Forms.Panel();
			this.reloadData = new System.Windows.Forms.Button();
			this.userLabel = new System.Windows.Forms.Label();
			this.toolTipForm = new System.Windows.Forms.ToolTip(this.components);
			this.tabControl1.SuspendLayout();
			this.tabOrders.SuspendLayout();
			this.tableLayoutPanel11.SuspendLayout();
			this.tableLayoutPanel19.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textCountOrders)).BeginInit();
			this.tableLayoutPanel18.SuspendLayout();
			this.groupBox5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewListCocktails)).BeginInit();
			this.groupBox6.SuspendLayout();
			this.tableLayoutPanel21.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewQueue)).BeginInit();
			this.panel7.SuspendLayout();
			this.tableLayoutPanel22.SuspendLayout();
			this.tabCocktails.SuspendLayout();
			this.tableLayoutPanel8.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewCocktails)).BeginInit();
			this.tableLayoutPanel7.SuspendLayout();
			this.tableLayoutPanel9.SuspendLayout();
			this.tableLayoutPanel10.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.tabProducts.SuspendLayout();
			this.tableLayoutPanel4.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts)).BeginInit();
			this.panel2.SuspendLayout();
			this.panelEditProducts.SuspendLayout();
			this.tableLayoutPanelProduct.SuspendLayout();
			this.groupBoxProductQuantity.SuspendLayout();
			this.tableLayoutPanel17.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textQuantityProduct)).BeginInit();
			this.groupBoxProductTypeProduct.SuspendLayout();
			this.tableLayoutPanel16.SuspendLayout();
			this.groupBoxProductPrice.SuspendLayout();
			this.tableLayoutPanel15.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textPriceProduct)).BeginInit();
			this.tableLayoutPanel5.SuspendLayout();
			this.groupBoxProductName.SuspendLayout();
			this.tableLayoutPanel14.SuspendLayout();
			this.tabResurse.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.tableLayoutPanel12.SuspendLayout();
			this.panelEditTares.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewTare)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.tableLayoutPanel13.SuspendLayout();
			this.panelEditTypeProducts.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewTypeProducts)).BeginInit();
			this.tabAdmin.SuspendLayout();
			this.groupBox7.SuspendLayout();
			this.tableLayoutPanel23.SuspendLayout();
			this.tableLayoutPanel24.SuspendLayout();
			this.groupBoxBarmanSkill.SuspendLayout();
			this.tableLayoutPanel31.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textBarmanSkill)).BeginInit();
			this.groupBoxBarmanTip.SuspendLayout();
			this.tableLayoutPanel26.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textBarmanTip)).BeginInit();
			this.groupBoxBarmanSalary.SuspendLayout();
			this.tableLayoutPanel27.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textBarmanSalary)).BeginInit();
			this.groupBoxBarmanPassword.SuspendLayout();
			this.tableLayoutPanel28.SuspendLayout();
			this.tableLayoutPanel29.SuspendLayout();
			this.groupBoxBarmanName.SuspendLayout();
			this.tableLayoutPanel30.SuspendLayout();
			this.tableLayoutPanel25.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewListBarmen)).BeginInit();
			this.tableLayoutPanel20.SuspendLayout();
			this.panel6.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabOrders);
			this.tabControl1.Controls.Add(this.tabCocktails);
			this.tabControl1.Controls.Add(this.tabProducts);
			this.tabControl1.Controls.Add(this.tabResurse);
			this.tabControl1.Controls.Add(this.tabAdmin);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 30);
			this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(840, 488);
			this.tabControl1.TabIndex = 0;
			this.tabControl1.TabStop = false;
			// 
			// tabOrders
			// 
			this.tabOrders.Controls.Add(this.tableLayoutPanel11);
			this.tabOrders.Location = new System.Drawing.Point(4, 22);
			this.tabOrders.Name = "tabOrders";
			this.tabOrders.Size = new System.Drawing.Size(832, 462);
			this.tabOrders.TabIndex = 5;
			this.tabOrders.Text = "Заказы";
			this.tabOrders.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel11
			// 
			this.tableLayoutPanel11.ColumnCount = 1;
			this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel19, 0, 0);
			this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel18, 0, 1);
			this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel11.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel11.Name = "tableLayoutPanel11";
			this.tableLayoutPanel11.RowCount = 2;
			this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel11.Size = new System.Drawing.Size(832, 462);
			this.tableLayoutPanel11.TabIndex = 0;
			// 
			// tableLayoutPanel19
			// 
			this.tableLayoutPanel19.BackColor = System.Drawing.Color.Gainsboro;
			this.tableLayoutPanel19.ColumnCount = 4;
			this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel19.Controls.Add(this.panel4, 0, 0);
			this.tableLayoutPanel19.Controls.Add(this.inQueue, 3, 0);
			this.tableLayoutPanel19.Controls.Add(this.completeOrder, 2, 0);
			this.tableLayoutPanel19.Controls.Add(this.panel5, 1, 0);
			this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel19.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel19.Name = "tableLayoutPanel19";
			this.tableLayoutPanel19.RowCount = 1;
			this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel19.Size = new System.Drawing.Size(826, 44);
			this.tableLayoutPanel19.TabIndex = 0;
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.Gainsboro;
			this.panel4.Controls.Add(this.comboBoxSearch);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel4.Location = new System.Drawing.Point(3, 3);
			this.panel4.Name = "panel4";
			this.panel4.Padding = new System.Windows.Forms.Padding(6);
			this.panel4.Size = new System.Drawing.Size(500, 38);
			this.panel4.TabIndex = 0;
			// 
			// comboBoxSearch
			// 
			this.comboBoxSearch.Dock = System.Windows.Forms.DockStyle.Fill;
			this.comboBoxSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.comboBoxSearch.FormattingEnabled = true;
			this.comboBoxSearch.Location = new System.Drawing.Point(6, 6);
			this.comboBoxSearch.Name = "comboBoxSearch";
			this.comboBoxSearch.Size = new System.Drawing.Size(488, 28);
			this.comboBoxSearch.TabIndex = 0;
			// 
			// inQueue
			// 
			this.inQueue.Dock = System.Windows.Forms.DockStyle.Fill;
			this.inQueue.Location = new System.Drawing.Point(729, 3);
			this.inQueue.Name = "inQueue";
			this.inQueue.Size = new System.Drawing.Size(94, 38);
			this.inQueue.TabIndex = 3;
			this.inQueue.Text = "В очередь";
			this.inQueue.UseVisualStyleBackColor = true;
			this.inQueue.Click += new System.EventHandler(this.inQueue_Click);
			// 
			// completeOrder
			// 
			this.completeOrder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.completeOrder.Location = new System.Drawing.Point(629, 3);
			this.completeOrder.Name = "completeOrder";
			this.completeOrder.Size = new System.Drawing.Size(94, 38);
			this.completeOrder.TabIndex = 2;
			this.completeOrder.Text = "Выполнить";
			this.completeOrder.UseVisualStyleBackColor = true;
			this.completeOrder.Click += new System.EventHandler(this.completeOrder_Click);
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.textCountOrders);
			this.panel5.Controls.Add(this.label1);
			this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel5.Location = new System.Drawing.Point(509, 3);
			this.panel5.Name = "panel5";
			this.panel5.Padding = new System.Windows.Forms.Padding(5);
			this.panel5.Size = new System.Drawing.Size(114, 38);
			this.panel5.TabIndex = 1;
			// 
			// textCountOrders
			// 
			this.textCountOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textCountOrders.Location = new System.Drawing.Point(36, 7);
			this.textCountOrders.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.textCountOrders.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.textCountOrders.Name = "textCountOrders";
			this.textCountOrders.Size = new System.Drawing.Size(71, 26);
			this.textCountOrders.TabIndex = 5;
			this.textCountOrders.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(9, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(20, 20);
			this.label1.TabIndex = 4;
			this.label1.Text = "X";
			// 
			// tableLayoutPanel18
			// 
			this.tableLayoutPanel18.ColumnCount = 2;
			this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
			this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
			this.tableLayoutPanel18.Controls.Add(this.groupBox5, 0, 0);
			this.tableLayoutPanel18.Controls.Add(this.groupBox6, 1, 0);
			this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel18.Location = new System.Drawing.Point(3, 53);
			this.tableLayoutPanel18.Name = "tableLayoutPanel18";
			this.tableLayoutPanel18.RowCount = 1;
			this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel18.Size = new System.Drawing.Size(826, 406);
			this.tableLayoutPanel18.TabIndex = 1;
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.dataGridViewListCocktails);
			this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox5.Location = new System.Drawing.Point(3, 3);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(489, 400);
			this.groupBox5.TabIndex = 0;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Список";
			// 
			// dataGridViewListCocktails
			// 
			this.dataGridViewListCocktails.AllowUserToAddRows = false;
			this.dataGridViewListCocktails.AllowUserToDeleteRows = false;
			this.dataGridViewListCocktails.AllowUserToResizeRows = false;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.dataGridViewListCocktails.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
			this.dataGridViewListCocktails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewListCocktails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvCocktailsListId,
            this.dgvCocktailsListName,
            this.dgvCocktailsListPrice,
            this.dgvCocktailsListPopularity});
			this.dataGridViewListCocktails.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewListCocktails.Location = new System.Drawing.Point(3, 16);
			this.dataGridViewListCocktails.Margin = new System.Windows.Forms.Padding(0);
			this.dataGridViewListCocktails.MultiSelect = false;
			this.dataGridViewListCocktails.Name = "dataGridViewListCocktails";
			this.dataGridViewListCocktails.RowHeadersVisible = false;
			this.dataGridViewListCocktails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewListCocktails.Size = new System.Drawing.Size(483, 381);
			this.dataGridViewListCocktails.TabIndex = 1;
			this.dataGridViewListCocktails.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewListCocktails_CellClick);
			this.dataGridViewListCocktails.SelectionChanged += new System.EventHandler(this.dataGridViewListCocktails_SelectionChanged);
			// 
			// dgvCocktailsListId
			// 
			this.dgvCocktailsListId.Frozen = true;
			this.dgvCocktailsListId.HeaderText = "Id";
			this.dgvCocktailsListId.Name = "dgvCocktailsListId";
			this.dgvCocktailsListId.ReadOnly = true;
			this.dgvCocktailsListId.Visible = false;
			// 
			// dgvCocktailsListName
			// 
			this.dgvCocktailsListName.Frozen = true;
			this.dgvCocktailsListName.HeaderText = "Наименование";
			this.dgvCocktailsListName.Name = "dgvCocktailsListName";
			this.dgvCocktailsListName.ReadOnly = true;
			this.dgvCocktailsListName.Width = 200;
			// 
			// dgvCocktailsListPrice
			// 
			this.dgvCocktailsListPrice.Frozen = true;
			this.dgvCocktailsListPrice.HeaderText = "Цена";
			this.dgvCocktailsListPrice.Name = "dgvCocktailsListPrice";
			this.dgvCocktailsListPrice.ReadOnly = true;
			// 
			// dgvCocktailsListPopularity
			// 
			this.dgvCocktailsListPopularity.Frozen = true;
			this.dgvCocktailsListPopularity.HeaderText = "Популярность";
			this.dgvCocktailsListPopularity.Name = "dgvCocktailsListPopularity";
			this.dgvCocktailsListPopularity.ReadOnly = true;
			// 
			// groupBox6
			// 
			this.groupBox6.Controls.Add(this.tableLayoutPanel21);
			this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox6.Location = new System.Drawing.Point(498, 3);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(325, 400);
			this.groupBox6.TabIndex = 1;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Очередь";
			// 
			// tableLayoutPanel21
			// 
			this.tableLayoutPanel21.ColumnCount = 1;
			this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel21.Controls.Add(this.dataGridViewQueue, 0, 1);
			this.tableLayoutPanel21.Controls.Add(this.panel7, 0, 0);
			this.tableLayoutPanel21.Controls.Add(this.tableLayoutPanel22, 0, 2);
			this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel21.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel21.Name = "tableLayoutPanel21";
			this.tableLayoutPanel21.RowCount = 3;
			this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
			this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.tableLayoutPanel21.Size = new System.Drawing.Size(319, 381);
			this.tableLayoutPanel21.TabIndex = 0;
			// 
			// dataGridViewQueue
			// 
			this.dataGridViewQueue.AllowUserToAddRows = false;
			this.dataGridViewQueue.AllowUserToDeleteRows = false;
			this.dataGridViewQueue.AllowUserToResizeRows = false;
			dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.dataGridViewQueue.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
			this.dataGridViewQueue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewQueue.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvOrderId,
            this.dgvOrderName,
            this.dgvOrderPrice});
			this.dataGridViewQueue.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewQueue.Location = new System.Drawing.Point(0, 45);
			this.dataGridViewQueue.Margin = new System.Windows.Forms.Padding(0);
			this.dataGridViewQueue.MultiSelect = false;
			this.dataGridViewQueue.Name = "dataGridViewQueue";
			this.dataGridViewQueue.RowHeadersVisible = false;
			this.dataGridViewQueue.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewQueue.Size = new System.Drawing.Size(319, 296);
			this.dataGridViewQueue.TabIndex = 2;
			this.dataGridViewQueue.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewQueue_CellClick);
			this.dataGridViewQueue.SelectionChanged += new System.EventHandler(this.dataGridViewQueue_SelectionChanged);
			// 
			// dgvOrderId
			// 
			this.dgvOrderId.Frozen = true;
			this.dgvOrderId.HeaderText = "Id";
			this.dgvOrderId.Name = "dgvOrderId";
			this.dgvOrderId.ReadOnly = true;
			this.dgvOrderId.Visible = false;
			// 
			// dgvOrderName
			// 
			this.dgvOrderName.Frozen = true;
			this.dgvOrderName.HeaderText = "Наименование";
			this.dgvOrderName.Name = "dgvOrderName";
			this.dgvOrderName.ReadOnly = true;
			this.dgvOrderName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
			this.dgvOrderName.Width = 180;
			// 
			// dgvOrderPrice
			// 
			this.dgvOrderPrice.Frozen = true;
			this.dgvOrderPrice.HeaderText = "Цена";
			this.dgvOrderPrice.Name = "dgvOrderPrice";
			this.dgvOrderPrice.ReadOnly = true;
			this.dgvOrderPrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
			// 
			// panel7
			// 
			this.panel7.Controls.Add(this.currentCocktailLabel);
			this.panel7.Controls.Add(this.label3);
			this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel7.Location = new System.Drawing.Point(3, 3);
			this.panel7.Name = "panel7";
			this.panel7.Size = new System.Drawing.Size(313, 39);
			this.panel7.TabIndex = 0;
			// 
			// currentCocktailLabel
			// 
			this.currentCocktailLabel.BackColor = System.Drawing.Color.Gainsboro;
			this.currentCocktailLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.currentCocktailLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.currentCocktailLabel.Location = new System.Drawing.Point(0, 18);
			this.currentCocktailLabel.Name = "currentCocktailLabel";
			this.currentCocktailLabel.Size = new System.Drawing.Size(313, 21);
			this.currentCocktailLabel.TabIndex = 1;
			this.currentCocktailLabel.Text = "Коктейль \"Коктейль\"";
			this.currentCocktailLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.Dock = System.Windows.Forms.DockStyle.Top;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(313, 18);
			this.label3.TabIndex = 0;
			this.label3.Text = "Текущий заказ";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tableLayoutPanel22
			// 
			this.tableLayoutPanel22.ColumnCount = 2;
			this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel22.Controls.Add(this.undoOrder, 0, 0);
			this.tableLayoutPanel22.Controls.Add(this.infoOrder, 1, 0);
			this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel22.Location = new System.Drawing.Point(3, 344);
			this.tableLayoutPanel22.Name = "tableLayoutPanel22";
			this.tableLayoutPanel22.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
			this.tableLayoutPanel22.RowCount = 1;
			this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel22.Size = new System.Drawing.Size(313, 34);
			this.tableLayoutPanel22.TabIndex = 1;
			// 
			// undoOrder
			// 
			this.undoOrder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.undoOrder.Enabled = false;
			this.undoOrder.Location = new System.Drawing.Point(13, 3);
			this.undoOrder.Name = "undoOrder";
			this.undoOrder.Size = new System.Drawing.Size(140, 28);
			this.undoOrder.TabIndex = 0;
			this.undoOrder.Text = "Отменить заказ";
			this.undoOrder.UseVisualStyleBackColor = true;
			// 
			// infoOrder
			// 
			this.infoOrder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.infoOrder.Enabled = false;
			this.infoOrder.Location = new System.Drawing.Point(159, 3);
			this.infoOrder.Name = "infoOrder";
			this.infoOrder.Size = new System.Drawing.Size(141, 28);
			this.infoOrder.TabIndex = 1;
			this.infoOrder.Text = "Информация";
			this.infoOrder.UseVisualStyleBackColor = true;
			// 
			// tabCocktails
			// 
			this.tabCocktails.Controls.Add(this.tableLayoutPanel8);
			this.tabCocktails.Location = new System.Drawing.Point(4, 22);
			this.tabCocktails.Name = "tabCocktails";
			this.tabCocktails.Size = new System.Drawing.Size(832, 462);
			this.tabCocktails.TabIndex = 0;
			this.tabCocktails.Text = "Коктейли";
			this.tabCocktails.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel8
			// 
			this.tableLayoutPanel8.ColumnCount = 2;
			this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
			this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
			this.tableLayoutPanel8.Controls.Add(this.panel3, 0, 0);
			this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel7, 1, 0);
			this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel8.Name = "tableLayoutPanel8";
			this.tableLayoutPanel8.RowCount = 1;
			this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel8.Size = new System.Drawing.Size(832, 462);
			this.tableLayoutPanel8.TabIndex = 1;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.dataGridViewCocktails);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Margin = new System.Windows.Forms.Padding(0);
			this.panel3.Name = "panel3";
			this.panel3.Padding = new System.Windows.Forms.Padding(10);
			this.panel3.Size = new System.Drawing.Size(332, 462);
			this.panel3.TabIndex = 1;
			// 
			// dataGridViewCocktails
			// 
			this.dataGridViewCocktails.AllowUserToAddRows = false;
			this.dataGridViewCocktails.AllowUserToDeleteRows = false;
			this.dataGridViewCocktails.AllowUserToResizeRows = false;
			dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.dataGridViewCocktails.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
			this.dataGridViewCocktails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewCocktails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvCocktailsId,
            this.dgvCocktailsName});
			this.dataGridViewCocktails.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewCocktails.Location = new System.Drawing.Point(10, 10);
			this.dataGridViewCocktails.Margin = new System.Windows.Forms.Padding(0);
			this.dataGridViewCocktails.MultiSelect = false;
			this.dataGridViewCocktails.Name = "dataGridViewCocktails";
			this.dataGridViewCocktails.RowHeadersVisible = false;
			this.dataGridViewCocktails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewCocktails.Size = new System.Drawing.Size(312, 442);
			this.dataGridViewCocktails.TabIndex = 0;
			this.dataGridViewCocktails.SelectionChanged += new System.EventHandler(this.dataGridViewCocktails_SelectionChanged);
			// 
			// dgvCocktailsId
			// 
			this.dgvCocktailsId.Frozen = true;
			this.dgvCocktailsId.HeaderText = "Id";
			this.dgvCocktailsId.Name = "dgvCocktailsId";
			this.dgvCocktailsId.ReadOnly = true;
			this.dgvCocktailsId.Visible = false;
			// 
			// dgvCocktailsName
			// 
			this.dgvCocktailsName.Frozen = true;
			this.dgvCocktailsName.HeaderText = "Наименование";
			this.dgvCocktailsName.Name = "dgvCocktailsName";
			this.dgvCocktailsName.ReadOnly = true;
			this.dgvCocktailsName.Width = 280;
			// 
			// tableLayoutPanel7
			// 
			this.tableLayoutPanel7.ColumnCount = 1;
			this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel9, 0, 0);
			this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel10, 0, 1);
			this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel7.Location = new System.Drawing.Point(335, 3);
			this.tableLayoutPanel7.Name = "tableLayoutPanel7";
			this.tableLayoutPanel7.RowCount = 2;
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86F));
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel7.Size = new System.Drawing.Size(494, 456);
			this.tableLayoutPanel7.TabIndex = 2;
			// 
			// tableLayoutPanel9
			// 
			this.tableLayoutPanel9.ColumnCount = 3;
			this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel9.Controls.Add(this.addCocktail, 2, 0);
			this.tableLayoutPanel9.Controls.Add(this.editCocktail, 1, 0);
			this.tableLayoutPanel9.Controls.Add(this.deleteCocktail, 0, 0);
			this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel9.Name = "tableLayoutPanel9";
			this.tableLayoutPanel9.Padding = new System.Windows.Forms.Padding(5);
			this.tableLayoutPanel9.RowCount = 1;
			this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel9.Size = new System.Drawing.Size(488, 57);
			this.tableLayoutPanel9.TabIndex = 3;
			// 
			// addCocktail
			// 
			this.addCocktail.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addCocktail.Location = new System.Drawing.Point(326, 8);
			this.addCocktail.Name = "addCocktail";
			this.addCocktail.Size = new System.Drawing.Size(154, 41);
			this.addCocktail.TabIndex = 3;
			this.addCocktail.Text = "Добавить";
			this.addCocktail.UseVisualStyleBackColor = true;
			this.addCocktail.Click += new System.EventHandler(this.addCocktail_Click);
			// 
			// editCocktail
			// 
			this.editCocktail.Dock = System.Windows.Forms.DockStyle.Fill;
			this.editCocktail.Location = new System.Drawing.Point(167, 8);
			this.editCocktail.Name = "editCocktail";
			this.editCocktail.Size = new System.Drawing.Size(153, 41);
			this.editCocktail.TabIndex = 2;
			this.editCocktail.Text = "Редактировать";
			this.editCocktail.UseVisualStyleBackColor = true;
			this.editCocktail.Click += new System.EventHandler(this.editCocktail_Click);
			// 
			// deleteCocktail
			// 
			this.deleteCocktail.Dock = System.Windows.Forms.DockStyle.Fill;
			this.deleteCocktail.Location = new System.Drawing.Point(8, 8);
			this.deleteCocktail.Name = "deleteCocktail";
			this.deleteCocktail.Size = new System.Drawing.Size(153, 41);
			this.deleteCocktail.TabIndex = 1;
			this.deleteCocktail.Text = "Удалить";
			this.deleteCocktail.UseVisualStyleBackColor = true;
			this.deleteCocktail.Click += new System.EventHandler(this.deleteCocktail_Click);
			// 
			// tableLayoutPanel10
			// 
			this.tableLayoutPanel10.ColumnCount = 2;
			this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel10.Controls.Add(this.groupBox4, 1, 0);
			this.tableLayoutPanel10.Controls.Add(this.groupBox3, 0, 0);
			this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 63);
			this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel10.Name = "tableLayoutPanel10";
			this.tableLayoutPanel10.RowCount = 1;
			this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel10.Size = new System.Drawing.Size(494, 393);
			this.tableLayoutPanel10.TabIndex = 4;
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.descriptionLabel);
			this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.groupBox4.Location = new System.Drawing.Point(250, 3);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(241, 387);
			this.groupBox4.TabIndex = 1;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Описание";
			// 
			// descriptionLabel
			// 
			this.descriptionLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.descriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.descriptionLabel.Location = new System.Drawing.Point(3, 17);
			this.descriptionLabel.Name = "descriptionLabel";
			this.descriptionLabel.Size = new System.Drawing.Size(235, 367);
			this.descriptionLabel.TabIndex = 0;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.processLabel);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.groupBox3.Location = new System.Drawing.Point(3, 3);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(241, 387);
			this.groupBox3.TabIndex = 0;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Компоненты";
			// 
			// processLabel
			// 
			this.processLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.processLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.processLabel.Location = new System.Drawing.Point(3, 17);
			this.processLabel.Name = "processLabel";
			this.processLabel.Size = new System.Drawing.Size(235, 367);
			this.processLabel.TabIndex = 0;
			// 
			// tabProducts
			// 
			this.tabProducts.Controls.Add(this.tableLayoutPanel4);
			this.tabProducts.Location = new System.Drawing.Point(4, 22);
			this.tabProducts.Name = "tabProducts";
			this.tabProducts.Size = new System.Drawing.Size(832, 462);
			this.tabProducts.TabIndex = 4;
			this.tabProducts.Text = "Продукты";
			this.tabProducts.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel4
			// 
			this.tableLayoutPanel4.ColumnCount = 2;
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
			this.tableLayoutPanel4.Controls.Add(this.panel1, 0, 0);
			this.tableLayoutPanel4.Controls.Add(this.panel2, 1, 0);
			this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel4.Name = "tableLayoutPanel4";
			this.tableLayoutPanel4.RowCount = 1;
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel4.Size = new System.Drawing.Size(832, 462);
			this.tableLayoutPanel4.TabIndex = 3;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.dataGridViewProducts);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Margin = new System.Windows.Forms.Padding(0);
			this.panel1.Name = "panel1";
			this.panel1.Padding = new System.Windows.Forms.Padding(10);
			this.panel1.Size = new System.Drawing.Size(540, 462);
			this.panel1.TabIndex = 2;
			// 
			// dataGridViewProducts
			// 
			this.dataGridViewProducts.AllowUserToAddRows = false;
			this.dataGridViewProducts.AllowUserToDeleteRows = false;
			this.dataGridViewProducts.AllowUserToResizeRows = false;
			dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.dataGridViewProducts.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
			this.dataGridViewProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvProductId,
            this.dgvProductTypeProductId,
            this.dgvProductName,
            this.dgvProductType,
            this.dgvProductQuantity,
            this.dgvProductPrice});
			this.dataGridViewProducts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewProducts.Location = new System.Drawing.Point(10, 10);
			this.dataGridViewProducts.MultiSelect = false;
			this.dataGridViewProducts.Name = "dataGridViewProducts";
			this.dataGridViewProducts.RowHeadersVisible = false;
			this.dataGridViewProducts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewProducts.Size = new System.Drawing.Size(520, 442);
			this.dataGridViewProducts.TabIndex = 1;
			this.dataGridViewProducts.SelectionChanged += new System.EventHandler(this.dataGridViewProducts_SelectionChanged);
			// 
			// dgvProductId
			// 
			this.dgvProductId.Frozen = true;
			this.dgvProductId.HeaderText = "Id";
			this.dgvProductId.Name = "dgvProductId";
			this.dgvProductId.ReadOnly = true;
			this.dgvProductId.Visible = false;
			// 
			// dgvProductTypeProductId
			// 
			this.dgvProductTypeProductId.Frozen = true;
			this.dgvProductTypeProductId.HeaderText = "TypeProductId";
			this.dgvProductTypeProductId.Name = "dgvProductTypeProductId";
			this.dgvProductTypeProductId.ReadOnly = true;
			this.dgvProductTypeProductId.Visible = false;
			// 
			// dgvProductName
			// 
			this.dgvProductName.Frozen = true;
			this.dgvProductName.HeaderText = "Наименование";
			this.dgvProductName.Name = "dgvProductName";
			this.dgvProductName.ReadOnly = true;
			this.dgvProductName.Width = 200;
			// 
			// dgvProductType
			// 
			this.dgvProductType.Frozen = true;
			this.dgvProductType.HeaderText = "Тип продукта";
			this.dgvProductType.Name = "dgvProductType";
			this.dgvProductType.ReadOnly = true;
			// 
			// dgvProductQuantity
			// 
			this.dgvProductQuantity.Frozen = true;
			this.dgvProductQuantity.HeaderText = "Количество";
			this.dgvProductQuantity.Name = "dgvProductQuantity";
			this.dgvProductQuantity.ReadOnly = true;
			// 
			// dgvProductPrice
			// 
			dataGridViewCellStyle5.Format = "N2";
			dataGridViewCellStyle5.NullValue = null;
			this.dgvProductPrice.DefaultCellStyle = dataGridViewCellStyle5;
			this.dgvProductPrice.Frozen = true;
			this.dgvProductPrice.HeaderText = "Цена";
			this.dgvProductPrice.Name = "dgvProductPrice";
			this.dgvProductPrice.ReadOnly = true;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.panelEditProducts);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(543, 3);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(286, 456);
			this.panel2.TabIndex = 3;
			// 
			// panelEditProducts
			// 
			this.panelEditProducts.ColumnCount = 1;
			this.panelEditProducts.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.panelEditProducts.Controls.Add(this.saveProduct, 0, 6);
			this.panelEditProducts.Controls.Add(this.tableLayoutPanelProduct, 0, 5);
			this.panelEditProducts.Controls.Add(this.groupBoxProductQuantity, 0, 4);
			this.panelEditProducts.Controls.Add(this.groupBoxProductTypeProduct, 0, 3);
			this.panelEditProducts.Controls.Add(this.groupBoxProductPrice, 0, 2);
			this.panelEditProducts.Controls.Add(this.tableLayoutPanel5, 0, 0);
			this.panelEditProducts.Controls.Add(this.groupBoxProductName, 0, 1);
			this.panelEditProducts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelEditProducts.Location = new System.Drawing.Point(0, 0);
			this.panelEditProducts.Name = "panelEditProducts";
			this.panelEditProducts.RowCount = 7;
			this.panelEditProducts.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
			this.panelEditProducts.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
			this.panelEditProducts.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
			this.panelEditProducts.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
			this.panelEditProducts.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18F));
			this.panelEditProducts.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
			this.panelEditProducts.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12F));
			this.panelEditProducts.Size = new System.Drawing.Size(286, 456);
			this.panelEditProducts.TabIndex = 3;
			// 
			// saveProduct
			// 
			this.saveProduct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.saveProduct.Enabled = false;
			this.saveProduct.Location = new System.Drawing.Point(10, 407);
			this.saveProduct.Margin = new System.Windows.Forms.Padding(10);
			this.saveProduct.Name = "saveProduct";
			this.saveProduct.Size = new System.Drawing.Size(266, 39);
			this.saveProduct.TabIndex = 8;
			this.saveProduct.Text = "Сохранить изменения";
			this.saveProduct.UseVisualStyleBackColor = true;
			this.saveProduct.Click += new System.EventHandler(this.saveProduct_Click);
			// 
			// tableLayoutPanelProduct
			// 
			this.tableLayoutPanelProduct.ColumnCount = 2;
			this.tableLayoutPanelProduct.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanelProduct.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanelProduct.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanelProduct.Controls.Add(this.undoProduct, 0, 0);
			this.tableLayoutPanelProduct.Controls.Add(this.importProduct, 0, 0);
			this.tableLayoutPanelProduct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanelProduct.Location = new System.Drawing.Point(3, 337);
			this.tableLayoutPanelProduct.Name = "tableLayoutPanelProduct";
			this.tableLayoutPanelProduct.Padding = new System.Windows.Forms.Padding(5);
			this.tableLayoutPanelProduct.RowCount = 1;
			this.tableLayoutPanelProduct.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanelProduct.Size = new System.Drawing.Size(280, 57);
			this.tableLayoutPanelProduct.TabIndex = 7;
			// 
			// undoProduct
			// 
			this.undoProduct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.undoProduct.Enabled = false;
			this.undoProduct.Location = new System.Drawing.Point(8, 8);
			this.undoProduct.Name = "undoProduct";
			this.undoProduct.Size = new System.Drawing.Size(129, 41);
			this.undoProduct.TabIndex = 2;
			this.undoProduct.Text = "Отмена изменений";
			this.undoProduct.UseVisualStyleBackColor = true;
			this.undoProduct.Click += new System.EventHandler(this.undoProduct_Click);
			// 
			// importProduct
			// 
			this.importProduct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.importProduct.Location = new System.Drawing.Point(143, 8);
			this.importProduct.Name = "importProduct";
			this.importProduct.Size = new System.Drawing.Size(129, 41);
			this.importProduct.TabIndex = 1;
			this.importProduct.Text = "Импорт с CSV";
			this.importProduct.UseVisualStyleBackColor = true;
			this.importProduct.Click += new System.EventHandler(this.importProduct_Click);
			// 
			// groupBoxProductQuantity
			// 
			this.groupBoxProductQuantity.Controls.Add(this.tableLayoutPanel17);
			this.groupBoxProductQuantity.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBoxProductQuantity.Enabled = false;
			this.groupBoxProductQuantity.Location = new System.Drawing.Point(3, 255);
			this.groupBoxProductQuantity.Name = "groupBoxProductQuantity";
			this.groupBoxProductQuantity.Size = new System.Drawing.Size(280, 76);
			this.groupBoxProductQuantity.TabIndex = 6;
			this.groupBoxProductQuantity.TabStop = false;
			this.groupBoxProductQuantity.Text = "Количество";
			// 
			// tableLayoutPanel17
			// 
			this.tableLayoutPanel17.ColumnCount = 1;
			this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel17.Controls.Add(this.textQuantityProduct, 0, 1);
			this.tableLayoutPanel17.Controls.Add(this.quantityTypeLabel, 0, 2);
			this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel17.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel17.Name = "tableLayoutPanel17";
			this.tableLayoutPanel17.RowCount = 3;
			this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel17.Size = new System.Drawing.Size(274, 57);
			this.tableLayoutPanel17.TabIndex = 0;
			// 
			// textQuantityProduct
			// 
			this.textQuantityProduct.DecimalPlaces = 2;
			this.textQuantityProduct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textQuantityProduct.Location = new System.Drawing.Point(3, 18);
			this.textQuantityProduct.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
			this.textQuantityProduct.Name = "textQuantityProduct";
			this.textQuantityProduct.Size = new System.Drawing.Size(268, 20);
			this.textQuantityProduct.TabIndex = 1;
			// 
			// quantityTypeLabel
			// 
			this.quantityTypeLabel.AutoSize = true;
			this.quantityTypeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.quantityTypeLabel.Location = new System.Drawing.Point(3, 41);
			this.quantityTypeLabel.Name = "quantityTypeLabel";
			this.quantityTypeLabel.Size = new System.Drawing.Size(268, 16);
			this.quantityTypeLabel.TabIndex = 2;
			this.quantityTypeLabel.Text = "Единиц";
			// 
			// groupBoxProductTypeProduct
			// 
			this.groupBoxProductTypeProduct.Controls.Add(this.tableLayoutPanel16);
			this.groupBoxProductTypeProduct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBoxProductTypeProduct.Enabled = false;
			this.groupBoxProductTypeProduct.Location = new System.Drawing.Point(3, 192);
			this.groupBoxProductTypeProduct.Name = "groupBoxProductTypeProduct";
			this.groupBoxProductTypeProduct.Size = new System.Drawing.Size(280, 57);
			this.groupBoxProductTypeProduct.TabIndex = 5;
			this.groupBoxProductTypeProduct.TabStop = false;
			this.groupBoxProductTypeProduct.Text = "Тип продукта";
			// 
			// tableLayoutPanel16
			// 
			this.tableLayoutPanel16.ColumnCount = 1;
			this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel16.Controls.Add(this.textTypeProduct, 0, 1);
			this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel16.Name = "tableLayoutPanel16";
			this.tableLayoutPanel16.RowCount = 3;
			this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel16.Size = new System.Drawing.Size(274, 38);
			this.tableLayoutPanel16.TabIndex = 0;
			// 
			// textTypeProduct
			// 
			this.textTypeProduct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textTypeProduct.FormattingEnabled = true;
			this.textTypeProduct.Location = new System.Drawing.Point(3, 8);
			this.textTypeProduct.Name = "textTypeProduct";
			this.textTypeProduct.Size = new System.Drawing.Size(268, 21);
			this.textTypeProduct.TabIndex = 0;
			this.textTypeProduct.SelectedIndexChanged += new System.EventHandler(this.textTypeProduct_SelectedIndexChanged);
			// 
			// groupBoxProductPrice
			// 
			this.groupBoxProductPrice.Controls.Add(this.tableLayoutPanel15);
			this.groupBoxProductPrice.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBoxProductPrice.Enabled = false;
			this.groupBoxProductPrice.Location = new System.Drawing.Point(3, 129);
			this.groupBoxProductPrice.Name = "groupBoxProductPrice";
			this.groupBoxProductPrice.Size = new System.Drawing.Size(280, 57);
			this.groupBoxProductPrice.TabIndex = 4;
			this.groupBoxProductPrice.TabStop = false;
			this.groupBoxProductPrice.Text = "Цена продукта";
			// 
			// tableLayoutPanel15
			// 
			this.tableLayoutPanel15.ColumnCount = 1;
			this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel15.Controls.Add(this.textPriceProduct, 0, 1);
			this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel15.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel15.Name = "tableLayoutPanel15";
			this.tableLayoutPanel15.RowCount = 3;
			this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel15.Size = new System.Drawing.Size(274, 38);
			this.tableLayoutPanel15.TabIndex = 0;
			// 
			// textPriceProduct
			// 
			this.textPriceProduct.DecimalPlaces = 2;
			this.textPriceProduct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textPriceProduct.Location = new System.Drawing.Point(3, 9);
			this.textPriceProduct.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
			this.textPriceProduct.Name = "textPriceProduct";
			this.textPriceProduct.Size = new System.Drawing.Size(268, 20);
			this.textPriceProduct.TabIndex = 0;
			this.toolTipForm.SetToolTip(this.textPriceProduct, "Цена продукта за его единицу количества");
			// 
			// tableLayoutPanel5
			// 
			this.tableLayoutPanel5.ColumnCount = 2;
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel5.Controls.Add(this.addProduct, 1, 0);
			this.tableLayoutPanel5.Controls.Add(this.deleteProduct, 0, 0);
			this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel5.Name = "tableLayoutPanel5";
			this.tableLayoutPanel5.Padding = new System.Windows.Forms.Padding(5);
			this.tableLayoutPanel5.RowCount = 1;
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel5.Size = new System.Drawing.Size(280, 57);
			this.tableLayoutPanel5.TabIndex = 2;
			// 
			// addProduct
			// 
			this.addProduct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addProduct.Location = new System.Drawing.Point(143, 8);
			this.addProduct.Name = "addProduct";
			this.addProduct.Size = new System.Drawing.Size(129, 41);
			this.addProduct.TabIndex = 3;
			this.addProduct.Text = "Новый продукт";
			this.addProduct.UseVisualStyleBackColor = true;
			this.addProduct.Click += new System.EventHandler(this.addProduct_Click);
			// 
			// deleteProduct
			// 
			this.deleteProduct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.deleteProduct.Enabled = false;
			this.deleteProduct.Location = new System.Drawing.Point(8, 8);
			this.deleteProduct.Name = "deleteProduct";
			this.deleteProduct.Size = new System.Drawing.Size(129, 41);
			this.deleteProduct.TabIndex = 1;
			this.deleteProduct.Text = "Удалить";
			this.deleteProduct.UseVisualStyleBackColor = true;
			this.deleteProduct.Click += new System.EventHandler(this.deleteProduct_Click);
			// 
			// groupBoxProductName
			// 
			this.groupBoxProductName.Controls.Add(this.tableLayoutPanel14);
			this.groupBoxProductName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBoxProductName.Enabled = false;
			this.groupBoxProductName.Location = new System.Drawing.Point(3, 66);
			this.groupBoxProductName.Name = "groupBoxProductName";
			this.groupBoxProductName.Size = new System.Drawing.Size(280, 57);
			this.groupBoxProductName.TabIndex = 3;
			this.groupBoxProductName.TabStop = false;
			this.groupBoxProductName.Text = "Название продукта";
			// 
			// tableLayoutPanel14
			// 
			this.tableLayoutPanel14.ColumnCount = 1;
			this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel14.Controls.Add(this.textNameProduct, 0, 1);
			this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel14.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel14.Name = "tableLayoutPanel14";
			this.tableLayoutPanel14.RowCount = 3;
			this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel14.Size = new System.Drawing.Size(274, 38);
			this.tableLayoutPanel14.TabIndex = 0;
			// 
			// textNameProduct
			// 
			this.textNameProduct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textNameProduct.Location = new System.Drawing.Point(3, 9);
			this.textNameProduct.MaxLength = 50;
			this.textNameProduct.Name = "textNameProduct";
			this.textNameProduct.Size = new System.Drawing.Size(268, 20);
			this.textNameProduct.TabIndex = 0;
			// 
			// tabResurse
			// 
			this.tabResurse.Controls.Add(this.tableLayoutPanel1);
			this.tabResurse.Location = new System.Drawing.Point(4, 22);
			this.tabResurse.Name = "tabResurse";
			this.tabResurse.Padding = new System.Windows.Forms.Padding(3);
			this.tabResurse.Size = new System.Drawing.Size(832, 462);
			this.tabResurse.TabIndex = 1;
			this.tabResurse.Text = "Ресурсы";
			this.tabResurse.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
			this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.groupBox2, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(826, 456);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.tableLayoutPanel12);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(489, 450);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Тара";
			// 
			// tableLayoutPanel12
			// 
			this.tableLayoutPanel12.ColumnCount = 1;
			this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel12.Controls.Add(this.panelEditTares, 0, 0);
			this.tableLayoutPanel12.Controls.Add(this.dataGridViewTare, 0, 1);
			this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel12.Name = "tableLayoutPanel12";
			this.tableLayoutPanel12.RowCount = 2;
			this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel12.Size = new System.Drawing.Size(483, 431);
			this.tableLayoutPanel12.TabIndex = 2;
			// 
			// panelEditTares
			// 
			this.panelEditTares.ColumnCount = 3;
			this.panelEditTares.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.panelEditTares.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.panelEditTares.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.panelEditTares.Controls.Add(this.addTare, 2, 0);
			this.panelEditTares.Controls.Add(this.editTare, 1, 0);
			this.panelEditTares.Controls.Add(this.deleteTare, 0, 0);
			this.panelEditTares.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelEditTares.Location = new System.Drawing.Point(3, 3);
			this.panelEditTares.Name = "panelEditTares";
			this.panelEditTares.Padding = new System.Windows.Forms.Padding(5);
			this.panelEditTares.RowCount = 1;
			this.panelEditTares.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.panelEditTares.Size = new System.Drawing.Size(477, 54);
			this.panelEditTares.TabIndex = 1;
			// 
			// addTare
			// 
			this.addTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addTare.Location = new System.Drawing.Point(318, 8);
			this.addTare.Name = "addTare";
			this.addTare.Size = new System.Drawing.Size(151, 38);
			this.addTare.TabIndex = 3;
			this.addTare.Text = "Добавить";
			this.addTare.UseVisualStyleBackColor = true;
			this.addTare.Click += new System.EventHandler(this.addTare_Click);
			// 
			// editTare
			// 
			this.editTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.editTare.Location = new System.Drawing.Point(163, 8);
			this.editTare.Name = "editTare";
			this.editTare.Size = new System.Drawing.Size(149, 38);
			this.editTare.TabIndex = 2;
			this.editTare.Text = "Редактировать";
			this.editTare.UseVisualStyleBackColor = true;
			this.editTare.Click += new System.EventHandler(this.editTare_Click);
			// 
			// deleteTare
			// 
			this.deleteTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.deleteTare.Location = new System.Drawing.Point(8, 8);
			this.deleteTare.Name = "deleteTare";
			this.deleteTare.Size = new System.Drawing.Size(149, 38);
			this.deleteTare.TabIndex = 1;
			this.deleteTare.Text = "Удалить";
			this.deleteTare.UseVisualStyleBackColor = true;
			this.deleteTare.Click += new System.EventHandler(this.deleteTare_Click);
			// 
			// dataGridViewTare
			// 
			this.dataGridViewTare.AllowUserToAddRows = false;
			this.dataGridViewTare.AllowUserToDeleteRows = false;
			this.dataGridViewTare.AllowUserToResizeRows = false;
			this.dataGridViewTare.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewTare.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvTareId,
            this.dgvTareType,
            this.dgvTareQuantity,
            this.dgvTarePrice,
            this.dgvTareReloading});
			this.dataGridViewTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewTare.Location = new System.Drawing.Point(3, 63);
			this.dataGridViewTare.Name = "dataGridViewTare";
			this.dataGridViewTare.RowHeadersVisible = false;
			this.dataGridViewTare.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewTare.Size = new System.Drawing.Size(477, 365);
			this.dataGridViewTare.TabIndex = 0;
			// 
			// dgvTareId
			// 
			this.dgvTareId.Frozen = true;
			this.dgvTareId.HeaderText = "Id";
			this.dgvTareId.Name = "dgvTareId";
			this.dgvTareId.ReadOnly = true;
			this.dgvTareId.Visible = false;
			// 
			// dgvTareType
			// 
			this.dgvTareType.Frozen = true;
			this.dgvTareType.HeaderText = "Тип";
			this.dgvTareType.Name = "dgvTareType";
			this.dgvTareType.ReadOnly = true;
			this.dgvTareType.Width = 150;
			// 
			// dgvTareQuantity
			// 
			this.dgvTareQuantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
			this.dgvTareQuantity.Frozen = true;
			this.dgvTareQuantity.HeaderText = "Количество";
			this.dgvTareQuantity.Name = "dgvTareQuantity";
			this.dgvTareQuantity.ReadOnly = true;
			this.dgvTareQuantity.Width = 91;
			// 
			// dgvTarePrice
			// 
			dataGridViewCellStyle6.Format = "N2";
			dataGridViewCellStyle6.NullValue = null;
			this.dgvTarePrice.DefaultCellStyle = dataGridViewCellStyle6;
			this.dgvTarePrice.Frozen = true;
			this.dgvTarePrice.HeaderText = "Цена";
			this.dgvTarePrice.Name = "dgvTarePrice";
			this.dgvTarePrice.ReadOnly = true;
			this.dgvTarePrice.Width = 80;
			// 
			// dgvTareReloading
			// 
			this.dgvTareReloading.Frozen = true;
			this.dgvTareReloading.HeaderText = "Возвращение (мин)";
			this.dgvTareReloading.Name = "dgvTareReloading";
			this.dgvTareReloading.ReadOnly = true;
			this.dgvTareReloading.Width = 130;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.tableLayoutPanel13);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(498, 3);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(325, 450);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Типы продуктов";
			// 
			// tableLayoutPanel13
			// 
			this.tableLayoutPanel13.ColumnCount = 1;
			this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel13.Controls.Add(this.panelEditTypeProducts, 0, 0);
			this.tableLayoutPanel13.Controls.Add(this.dataGridViewTypeProducts, 0, 1);
			this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel13.Name = "tableLayoutPanel13";
			this.tableLayoutPanel13.RowCount = 2;
			this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel13.Size = new System.Drawing.Size(319, 431);
			this.tableLayoutPanel13.TabIndex = 3;
			// 
			// panelEditTypeProducts
			// 
			this.panelEditTypeProducts.ColumnCount = 3;
			this.panelEditTypeProducts.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.panelEditTypeProducts.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.panelEditTypeProducts.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.panelEditTypeProducts.Controls.Add(this.addType, 2, 0);
			this.panelEditTypeProducts.Controls.Add(this.editType, 1, 0);
			this.panelEditTypeProducts.Controls.Add(this.deleteType, 0, 0);
			this.panelEditTypeProducts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelEditTypeProducts.Location = new System.Drawing.Point(3, 3);
			this.panelEditTypeProducts.Name = "panelEditTypeProducts";
			this.panelEditTypeProducts.Padding = new System.Windows.Forms.Padding(5);
			this.panelEditTypeProducts.RowCount = 1;
			this.panelEditTypeProducts.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.panelEditTypeProducts.Size = new System.Drawing.Size(313, 54);
			this.panelEditTypeProducts.TabIndex = 2;
			// 
			// addType
			// 
			this.addType.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addType.Location = new System.Drawing.Point(210, 8);
			this.addType.Name = "addType";
			this.addType.Size = new System.Drawing.Size(95, 38);
			this.addType.TabIndex = 3;
			this.addType.Text = "Добавить";
			this.addType.UseVisualStyleBackColor = true;
			this.addType.Click += new System.EventHandler(this.addType_Click);
			// 
			// editType
			// 
			this.editType.Dock = System.Windows.Forms.DockStyle.Fill;
			this.editType.Location = new System.Drawing.Point(109, 8);
			this.editType.Name = "editType";
			this.editType.Size = new System.Drawing.Size(95, 38);
			this.editType.TabIndex = 2;
			this.editType.Text = "Редактировать";
			this.editType.UseVisualStyleBackColor = true;
			this.editType.Click += new System.EventHandler(this.editType_Click);
			// 
			// deleteType
			// 
			this.deleteType.Dock = System.Windows.Forms.DockStyle.Fill;
			this.deleteType.Location = new System.Drawing.Point(8, 8);
			this.deleteType.Name = "deleteType";
			this.deleteType.Size = new System.Drawing.Size(95, 38);
			this.deleteType.TabIndex = 1;
			this.deleteType.Text = "Удалить";
			this.deleteType.UseVisualStyleBackColor = true;
			this.deleteType.Click += new System.EventHandler(this.deleteType_Click);
			// 
			// dataGridViewTypeProducts
			// 
			this.dataGridViewTypeProducts.AllowUserToAddRows = false;
			this.dataGridViewTypeProducts.AllowUserToDeleteRows = false;
			this.dataGridViewTypeProducts.AllowUserToResizeRows = false;
			dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.dataGridViewTypeProducts.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
			this.dataGridViewTypeProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewTypeProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvTypeProductId,
            this.dgvTypeProductName,
            this.dgvTypeProductUnit});
			this.dataGridViewTypeProducts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewTypeProducts.Location = new System.Drawing.Point(3, 63);
			this.dataGridViewTypeProducts.MultiSelect = false;
			this.dataGridViewTypeProducts.Name = "dataGridViewTypeProducts";
			this.dataGridViewTypeProducts.RowHeadersVisible = false;
			this.dataGridViewTypeProducts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewTypeProducts.Size = new System.Drawing.Size(313, 365);
			this.dataGridViewTypeProducts.TabIndex = 0;
			this.dataGridViewTypeProducts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTypeProducts_CellClick);
			// 
			// dgvTypeProductId
			// 
			this.dgvTypeProductId.Frozen = true;
			this.dgvTypeProductId.HeaderText = "Id";
			this.dgvTypeProductId.Name = "dgvTypeProductId";
			this.dgvTypeProductId.ReadOnly = true;
			this.dgvTypeProductId.Visible = false;
			// 
			// dgvTypeProductName
			// 
			this.dgvTypeProductName.Frozen = true;
			this.dgvTypeProductName.HeaderText = "Наименование";
			this.dgvTypeProductName.Name = "dgvTypeProductName";
			this.dgvTypeProductName.ReadOnly = true;
			this.dgvTypeProductName.Width = 150;
			// 
			// dgvTypeProductUnit
			// 
			this.dgvTypeProductUnit.Frozen = true;
			this.dgvTypeProductUnit.HeaderText = "Единица измерения";
			this.dgvTypeProductUnit.Name = "dgvTypeProductUnit";
			this.dgvTypeProductUnit.ReadOnly = true;
			this.dgvTypeProductUnit.Width = 150;
			// 
			// tabAdmin
			// 
			this.tabAdmin.Controls.Add(this.groupBox7);
			this.tabAdmin.Location = new System.Drawing.Point(4, 22);
			this.tabAdmin.Name = "tabAdmin";
			this.tabAdmin.Size = new System.Drawing.Size(832, 462);
			this.tabAdmin.TabIndex = 3;
			this.tabAdmin.Text = "Администрирование";
			this.tabAdmin.UseVisualStyleBackColor = true;
			// 
			// groupBox7
			// 
			this.groupBox7.Controls.Add(this.tableLayoutPanel23);
			this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox7.Location = new System.Drawing.Point(0, 0);
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Size = new System.Drawing.Size(832, 462);
			this.groupBox7.TabIndex = 0;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "Бармены";
			// 
			// tableLayoutPanel23
			// 
			this.tableLayoutPanel23.ColumnCount = 2;
			this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
			this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
			this.tableLayoutPanel23.Controls.Add(this.tableLayoutPanel24, 1, 0);
			this.tableLayoutPanel23.Controls.Add(this.dataGridViewListBarmen, 0, 0);
			this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel23.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel23.Name = "tableLayoutPanel23";
			this.tableLayoutPanel23.RowCount = 1;
			this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 443F));
			this.tableLayoutPanel23.Size = new System.Drawing.Size(826, 443);
			this.tableLayoutPanel23.TabIndex = 0;
			// 
			// tableLayoutPanel24
			// 
			this.tableLayoutPanel24.ColumnCount = 1;
			this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel24.Controls.Add(this.groupBoxBarmanSkill, 0, 5);
			this.tableLayoutPanel24.Controls.Add(this.groupBoxBarmanTip, 0, 4);
			this.tableLayoutPanel24.Controls.Add(this.groupBoxBarmanSalary, 0, 3);
			this.tableLayoutPanel24.Controls.Add(this.groupBoxBarmanPassword, 0, 2);
			this.tableLayoutPanel24.Controls.Add(this.tableLayoutPanel29, 0, 0);
			this.tableLayoutPanel24.Controls.Add(this.groupBoxBarmanName, 0, 1);
			this.tableLayoutPanel24.Controls.Add(this.tableLayoutPanel25, 0, 6);
			this.tableLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel24.Location = new System.Drawing.Point(498, 3);
			this.tableLayoutPanel24.Name = "tableLayoutPanel24";
			this.tableLayoutPanel24.RowCount = 7;
			this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
			this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
			this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
			this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
			this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
			this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel24.Size = new System.Drawing.Size(325, 437);
			this.tableLayoutPanel24.TabIndex = 4;
			// 
			// groupBoxBarmanSkill
			// 
			this.groupBoxBarmanSkill.Controls.Add(this.tableLayoutPanel31);
			this.groupBoxBarmanSkill.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBoxBarmanSkill.Enabled = false;
			this.groupBoxBarmanSkill.Location = new System.Drawing.Point(3, 312);
			this.groupBoxBarmanSkill.Name = "groupBoxBarmanSkill";
			this.groupBoxBarmanSkill.Size = new System.Drawing.Size(319, 55);
			this.groupBoxBarmanSkill.TabIndex = 8;
			this.groupBoxBarmanSkill.TabStop = false;
			this.groupBoxBarmanSkill.Text = "Уровень квалификации (1-3)";
			// 
			// tableLayoutPanel31
			// 
			this.tableLayoutPanel31.ColumnCount = 1;
			this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel31.Controls.Add(this.textBarmanSkill, 0, 1);
			this.tableLayoutPanel31.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel31.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel31.Name = "tableLayoutPanel31";
			this.tableLayoutPanel31.RowCount = 3;
			this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel31.Size = new System.Drawing.Size(313, 36);
			this.tableLayoutPanel31.TabIndex = 0;
			// 
			// textBarmanSkill
			// 
			this.textBarmanSkill.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBarmanSkill.Location = new System.Drawing.Point(3, 8);
			this.textBarmanSkill.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
			this.textBarmanSkill.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.textBarmanSkill.Name = "textBarmanSkill";
			this.textBarmanSkill.Size = new System.Drawing.Size(307, 20);
			this.textBarmanSkill.TabIndex = 1;
			this.textBarmanSkill.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// groupBoxBarmanTip
			// 
			this.groupBoxBarmanTip.Controls.Add(this.tableLayoutPanel26);
			this.groupBoxBarmanTip.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBoxBarmanTip.Enabled = false;
			this.groupBoxBarmanTip.Location = new System.Drawing.Point(3, 251);
			this.groupBoxBarmanTip.Name = "groupBoxBarmanTip";
			this.groupBoxBarmanTip.Size = new System.Drawing.Size(319, 55);
			this.groupBoxBarmanTip.TabIndex = 6;
			this.groupBoxBarmanTip.TabStop = false;
			this.groupBoxBarmanTip.Text = "Чаевые";
			// 
			// tableLayoutPanel26
			// 
			this.tableLayoutPanel26.ColumnCount = 1;
			this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel26.Controls.Add(this.textBarmanTip, 0, 1);
			this.tableLayoutPanel26.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel26.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel26.Name = "tableLayoutPanel26";
			this.tableLayoutPanel26.RowCount = 3;
			this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel26.Size = new System.Drawing.Size(313, 36);
			this.tableLayoutPanel26.TabIndex = 0;
			// 
			// textBarmanTip
			// 
			this.textBarmanTip.DecimalPlaces = 2;
			this.textBarmanTip.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBarmanTip.Location = new System.Drawing.Point(3, 8);
			this.textBarmanTip.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
			this.textBarmanTip.Name = "textBarmanTip";
			this.textBarmanTip.Size = new System.Drawing.Size(307, 20);
			this.textBarmanTip.TabIndex = 0;
			// 
			// groupBoxBarmanSalary
			// 
			this.groupBoxBarmanSalary.Controls.Add(this.tableLayoutPanel27);
			this.groupBoxBarmanSalary.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBoxBarmanSalary.Enabled = false;
			this.groupBoxBarmanSalary.Location = new System.Drawing.Point(3, 190);
			this.groupBoxBarmanSalary.Name = "groupBoxBarmanSalary";
			this.groupBoxBarmanSalary.Size = new System.Drawing.Size(319, 55);
			this.groupBoxBarmanSalary.TabIndex = 5;
			this.groupBoxBarmanSalary.TabStop = false;
			this.groupBoxBarmanSalary.Text = "Зарплата";
			// 
			// tableLayoutPanel27
			// 
			this.tableLayoutPanel27.ColumnCount = 1;
			this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel27.Controls.Add(this.textBarmanSalary, 0, 1);
			this.tableLayoutPanel27.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel27.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel27.Name = "tableLayoutPanel27";
			this.tableLayoutPanel27.RowCount = 3;
			this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel27.Size = new System.Drawing.Size(313, 36);
			this.tableLayoutPanel27.TabIndex = 0;
			// 
			// textBarmanSalary
			// 
			this.textBarmanSalary.DecimalPlaces = 2;
			this.textBarmanSalary.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBarmanSalary.Location = new System.Drawing.Point(3, 8);
			this.textBarmanSalary.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
			this.textBarmanSalary.Name = "textBarmanSalary";
			this.textBarmanSalary.Size = new System.Drawing.Size(307, 20);
			this.textBarmanSalary.TabIndex = 0;
			// 
			// groupBoxBarmanPassword
			// 
			this.groupBoxBarmanPassword.Controls.Add(this.tableLayoutPanel28);
			this.groupBoxBarmanPassword.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBoxBarmanPassword.Enabled = false;
			this.groupBoxBarmanPassword.Location = new System.Drawing.Point(3, 129);
			this.groupBoxBarmanPassword.Name = "groupBoxBarmanPassword";
			this.groupBoxBarmanPassword.Size = new System.Drawing.Size(319, 55);
			this.groupBoxBarmanPassword.TabIndex = 4;
			this.groupBoxBarmanPassword.TabStop = false;
			this.groupBoxBarmanPassword.Text = "Пароль";
			// 
			// tableLayoutPanel28
			// 
			this.tableLayoutPanel28.ColumnCount = 1;
			this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel28.Controls.Add(this.textBarmanPassword, 0, 1);
			this.tableLayoutPanel28.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel28.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel28.Name = "tableLayoutPanel28";
			this.tableLayoutPanel28.RowCount = 3;
			this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel28.Size = new System.Drawing.Size(313, 36);
			this.tableLayoutPanel28.TabIndex = 0;
			// 
			// textBarmanPassword
			// 
			this.textBarmanPassword.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBarmanPassword.Location = new System.Drawing.Point(3, 8);
			this.textBarmanPassword.MaxLength = 50;
			this.textBarmanPassword.Name = "textBarmanPassword";
			this.textBarmanPassword.Size = new System.Drawing.Size(307, 20);
			this.textBarmanPassword.TabIndex = 0;
			// 
			// tableLayoutPanel29
			// 
			this.tableLayoutPanel29.ColumnCount = 2;
			this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel29.Controls.Add(this.newBarman, 1, 0);
			this.tableLayoutPanel29.Controls.Add(this.deleteBarman, 0, 0);
			this.tableLayoutPanel29.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel29.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel29.Name = "tableLayoutPanel29";
			this.tableLayoutPanel29.Padding = new System.Windows.Forms.Padding(5);
			this.tableLayoutPanel29.RowCount = 1;
			this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel29.Size = new System.Drawing.Size(319, 59);
			this.tableLayoutPanel29.TabIndex = 2;
			// 
			// newBarman
			// 
			this.newBarman.Dock = System.Windows.Forms.DockStyle.Fill;
			this.newBarman.Location = new System.Drawing.Point(162, 8);
			this.newBarman.Name = "newBarman";
			this.newBarman.Size = new System.Drawing.Size(149, 43);
			this.newBarman.TabIndex = 3;
			this.newBarman.Text = "Новый бармен";
			this.newBarman.UseVisualStyleBackColor = true;
			this.newBarman.Click += new System.EventHandler(this.newBarman_Click);
			// 
			// deleteBarman
			// 
			this.deleteBarman.Dock = System.Windows.Forms.DockStyle.Fill;
			this.deleteBarman.Enabled = false;
			this.deleteBarman.Location = new System.Drawing.Point(8, 8);
			this.deleteBarman.Name = "deleteBarman";
			this.deleteBarman.Size = new System.Drawing.Size(148, 43);
			this.deleteBarman.TabIndex = 1;
			this.deleteBarman.Text = "Удалить";
			this.deleteBarman.UseVisualStyleBackColor = true;
			this.deleteBarman.Click += new System.EventHandler(this.deleteBarman_Click);
			// 
			// groupBoxBarmanName
			// 
			this.groupBoxBarmanName.Controls.Add(this.tableLayoutPanel30);
			this.groupBoxBarmanName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBoxBarmanName.Enabled = false;
			this.groupBoxBarmanName.Location = new System.Drawing.Point(3, 68);
			this.groupBoxBarmanName.Name = "groupBoxBarmanName";
			this.groupBoxBarmanName.Size = new System.Drawing.Size(319, 55);
			this.groupBoxBarmanName.TabIndex = 3;
			this.groupBoxBarmanName.TabStop = false;
			this.groupBoxBarmanName.Text = "Логин";
			// 
			// tableLayoutPanel30
			// 
			this.tableLayoutPanel30.ColumnCount = 1;
			this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel30.Controls.Add(this.textBarmanName, 0, 1);
			this.tableLayoutPanel30.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel30.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel30.Name = "tableLayoutPanel30";
			this.tableLayoutPanel30.RowCount = 3;
			this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel30.Size = new System.Drawing.Size(313, 36);
			this.tableLayoutPanel30.TabIndex = 0;
			// 
			// textBarmanName
			// 
			this.textBarmanName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBarmanName.Location = new System.Drawing.Point(3, 8);
			this.textBarmanName.MaxLength = 50;
			this.textBarmanName.Name = "textBarmanName";
			this.textBarmanName.Size = new System.Drawing.Size(307, 20);
			this.textBarmanName.TabIndex = 0;
			// 
			// tableLayoutPanel25
			// 
			this.tableLayoutPanel25.ColumnCount = 2;
			this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel25.Controls.Add(this.saveBarman, 1, 0);
			this.tableLayoutPanel25.Controls.Add(this.undoBarman, 0, 0);
			this.tableLayoutPanel25.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel25.Location = new System.Drawing.Point(3, 373);
			this.tableLayoutPanel25.Name = "tableLayoutPanel25";
			this.tableLayoutPanel25.Padding = new System.Windows.Forms.Padding(5);
			this.tableLayoutPanel25.RowCount = 1;
			this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel25.Size = new System.Drawing.Size(319, 61);
			this.tableLayoutPanel25.TabIndex = 7;
			// 
			// saveBarman
			// 
			this.saveBarman.Dock = System.Windows.Forms.DockStyle.Fill;
			this.saveBarman.Enabled = false;
			this.saveBarman.Location = new System.Drawing.Point(162, 8);
			this.saveBarman.Name = "saveBarman";
			this.saveBarman.Size = new System.Drawing.Size(149, 45);
			this.saveBarman.TabIndex = 3;
			this.saveBarman.Text = "Сохранить изменения";
			this.saveBarman.UseVisualStyleBackColor = true;
			this.saveBarman.Click += new System.EventHandler(this.saveBarman_Click);
			// 
			// undoBarman
			// 
			this.undoBarman.Dock = System.Windows.Forms.DockStyle.Fill;
			this.undoBarman.Enabled = false;
			this.undoBarman.Location = new System.Drawing.Point(8, 8);
			this.undoBarman.Name = "undoBarman";
			this.undoBarman.Size = new System.Drawing.Size(148, 45);
			this.undoBarman.TabIndex = 1;
			this.undoBarman.Text = "Отмена изменений";
			this.undoBarman.UseVisualStyleBackColor = true;
			this.undoBarman.Click += new System.EventHandler(this.undoBarman_Click);
			// 
			// dataGridViewListBarmen
			// 
			this.dataGridViewListBarmen.AllowUserToAddRows = false;
			this.dataGridViewListBarmen.AllowUserToDeleteRows = false;
			this.dataGridViewListBarmen.AllowUserToResizeRows = false;
			dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.dataGridViewListBarmen.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
			this.dataGridViewListBarmen.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
			this.dataGridViewListBarmen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewListBarmen.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvBarmanId,
            this.dgvBarmanName,
            this.dgvBarmanPassword,
            this.dgvBarmanSalary,
            this.dgvBarmanTip,
            this.dgvBarmanSkill});
			this.dataGridViewListBarmen.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewListBarmen.Location = new System.Drawing.Point(3, 3);
			this.dataGridViewListBarmen.MultiSelect = false;
			this.dataGridViewListBarmen.Name = "dataGridViewListBarmen";
			this.dataGridViewListBarmen.ReadOnly = true;
			this.dataGridViewListBarmen.RowHeadersVisible = false;
			this.dataGridViewListBarmen.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewListBarmen.Size = new System.Drawing.Size(489, 437);
			this.dataGridViewListBarmen.TabIndex = 0;
			this.dataGridViewListBarmen.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewListBarmen_CellClick);
			// 
			// dgvBarmanId
			// 
			this.dgvBarmanId.Frozen = true;
			this.dgvBarmanId.HeaderText = "Id";
			this.dgvBarmanId.Name = "dgvBarmanId";
			this.dgvBarmanId.ReadOnly = true;
			this.dgvBarmanId.Visible = false;
			// 
			// dgvBarmanName
			// 
			this.dgvBarmanName.Frozen = true;
			this.dgvBarmanName.HeaderText = "Логин";
			this.dgvBarmanName.Name = "dgvBarmanName";
			this.dgvBarmanName.ReadOnly = true;
			// 
			// dgvBarmanPassword
			// 
			this.dgvBarmanPassword.Frozen = true;
			this.dgvBarmanPassword.HeaderText = "Пароль";
			this.dgvBarmanPassword.Name = "dgvBarmanPassword";
			this.dgvBarmanPassword.ReadOnly = true;
			// 
			// dgvBarmanSalary
			// 
			this.dgvBarmanSalary.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
			this.dgvBarmanSalary.Frozen = true;
			this.dgvBarmanSalary.HeaderText = "Зарплата";
			this.dgvBarmanSalary.Name = "dgvBarmanSalary";
			this.dgvBarmanSalary.ReadOnly = true;
			this.dgvBarmanSalary.Width = 80;
			// 
			// dgvBarmanTip
			// 
			this.dgvBarmanTip.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
			this.dgvBarmanTip.Frozen = true;
			this.dgvBarmanTip.HeaderText = "Чаевые";
			this.dgvBarmanTip.Name = "dgvBarmanTip";
			this.dgvBarmanTip.ReadOnly = true;
			this.dgvBarmanTip.Width = 72;
			// 
			// dgvBarmanSkill
			// 
			this.dgvBarmanSkill.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
			this.dgvBarmanSkill.Frozen = true;
			this.dgvBarmanSkill.HeaderText = "Мастерство";
			this.dgvBarmanSkill.Name = "dgvBarmanSkill";
			this.dgvBarmanSkill.ReadOnly = true;
			this.dgvBarmanSkill.Width = 93;
			// 
			// tableLayoutPanel20
			// 
			this.tableLayoutPanel20.ColumnCount = 1;
			this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel20.Controls.Add(this.tabControl1, 0, 1);
			this.tableLayoutPanel20.Controls.Add(this.panel6, 0, 0);
			this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel20.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel20.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel20.Name = "tableLayoutPanel20";
			this.tableLayoutPanel20.RowCount = 2;
			this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel20.Size = new System.Drawing.Size(840, 518);
			this.tableLayoutPanel20.TabIndex = 1;
			// 
			// panel6
			// 
			this.panel6.Controls.Add(this.reloadData);
			this.panel6.Controls.Add(this.userLabel);
			this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel6.Location = new System.Drawing.Point(3, 3);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(834, 24);
			this.panel6.TabIndex = 1;
			// 
			// reloadData
			// 
			this.reloadData.Dock = System.Windows.Forms.DockStyle.Right;
			this.reloadData.Location = new System.Drawing.Point(741, 0);
			this.reloadData.Name = "reloadData";
			this.reloadData.Size = new System.Drawing.Size(93, 24);
			this.reloadData.TabIndex = 1;
			this.reloadData.Text = "Обновить";
			this.reloadData.UseVisualStyleBackColor = true;
			this.reloadData.Click += new System.EventHandler(this.reloadData_Click);
			// 
			// userLabel
			// 
			this.userLabel.AutoSize = true;
			this.userLabel.Dock = System.Windows.Forms.DockStyle.Left;
			this.userLabel.Location = new System.Drawing.Point(0, 0);
			this.userLabel.Margin = new System.Windows.Forms.Padding(5);
			this.userLabel.Name = "userLabel";
			this.userLabel.Size = new System.Drawing.Size(174, 13);
			this.userLabel.TabIndex = 0;
			this.userLabel.Text = "Пользователь: Владелец склада";
			// 
			// CocktailsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(840, 518);
			this.Controls.Add(this.tableLayoutPanel20);
			this.MinimumSize = new System.Drawing.Size(856, 556);
			this.Name = "CocktailsForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Коктейли";
			this.Load += new System.EventHandler(this.CoctailsForm_Load);
			this.tabControl1.ResumeLayout(false);
			this.tabOrders.ResumeLayout(false);
			this.tableLayoutPanel11.ResumeLayout(false);
			this.tableLayoutPanel19.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel5.ResumeLayout(false);
			this.panel5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.textCountOrders)).EndInit();
			this.tableLayoutPanel18.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewListCocktails)).EndInit();
			this.groupBox6.ResumeLayout(false);
			this.tableLayoutPanel21.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewQueue)).EndInit();
			this.panel7.ResumeLayout(false);
			this.tableLayoutPanel22.ResumeLayout(false);
			this.tabCocktails.ResumeLayout(false);
			this.tableLayoutPanel8.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewCocktails)).EndInit();
			this.tableLayoutPanel7.ResumeLayout(false);
			this.tableLayoutPanel9.ResumeLayout(false);
			this.tableLayoutPanel10.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.tabProducts.ResumeLayout(false);
			this.tableLayoutPanel4.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panelEditProducts.ResumeLayout(false);
			this.tableLayoutPanelProduct.ResumeLayout(false);
			this.groupBoxProductQuantity.ResumeLayout(false);
			this.tableLayoutPanel17.ResumeLayout(false);
			this.tableLayoutPanel17.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.textQuantityProduct)).EndInit();
			this.groupBoxProductTypeProduct.ResumeLayout(false);
			this.tableLayoutPanel16.ResumeLayout(false);
			this.groupBoxProductPrice.ResumeLayout(false);
			this.tableLayoutPanel15.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.textPriceProduct)).EndInit();
			this.tableLayoutPanel5.ResumeLayout(false);
			this.groupBoxProductName.ResumeLayout(false);
			this.tableLayoutPanel14.ResumeLayout(false);
			this.tableLayoutPanel14.PerformLayout();
			this.tabResurse.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.tableLayoutPanel12.ResumeLayout(false);
			this.panelEditTares.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewTare)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.tableLayoutPanel13.ResumeLayout(false);
			this.panelEditTypeProducts.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewTypeProducts)).EndInit();
			this.tabAdmin.ResumeLayout(false);
			this.groupBox7.ResumeLayout(false);
			this.tableLayoutPanel23.ResumeLayout(false);
			this.tableLayoutPanel24.ResumeLayout(false);
			this.groupBoxBarmanSkill.ResumeLayout(false);
			this.tableLayoutPanel31.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.textBarmanSkill)).EndInit();
			this.groupBoxBarmanTip.ResumeLayout(false);
			this.tableLayoutPanel26.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.textBarmanTip)).EndInit();
			this.groupBoxBarmanSalary.ResumeLayout(false);
			this.tableLayoutPanel27.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.textBarmanSalary)).EndInit();
			this.groupBoxBarmanPassword.ResumeLayout(false);
			this.tableLayoutPanel28.ResumeLayout(false);
			this.tableLayoutPanel28.PerformLayout();
			this.tableLayoutPanel29.ResumeLayout(false);
			this.groupBoxBarmanName.ResumeLayout(false);
			this.tableLayoutPanel30.ResumeLayout(false);
			this.tableLayoutPanel30.PerformLayout();
			this.tableLayoutPanel25.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewListBarmen)).EndInit();
			this.tableLayoutPanel20.ResumeLayout(false);
			this.panel6.ResumeLayout(false);
			this.panel6.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabCocktails;
		private System.Windows.Forms.TabPage tabResurse;
		private System.Windows.Forms.TabPage tabAdmin;
		private System.Windows.Forms.DataGridView dataGridViewCocktails;
		private System.Windows.Forms.TabPage tabProducts;
		private System.Windows.Forms.DataGridView dataGridViewProducts;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.DataGridView dataGridViewTare;
		private System.Windows.Forms.DataGridView dataGridViewTypeProducts;
		private System.Windows.Forms.TableLayoutPanel panelEditTares;
		private System.Windows.Forms.Button addTare;
		private System.Windows.Forms.Button editTare;
		private System.Windows.Forms.Button deleteTare;
		private System.Windows.Forms.TableLayoutPanel panelEditTypeProducts;
		private System.Windows.Forms.Button addType;
		private System.Windows.Forms.Button editType;
		private System.Windows.Forms.Button deleteType;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
		private System.Windows.Forms.Button addProduct;
		private System.Windows.Forms.Button deleteProduct;
		private System.Windows.Forms.TableLayoutPanel panelEditProducts;
		private System.Windows.Forms.GroupBox groupBoxProductQuantity;
		private System.Windows.Forms.GroupBox groupBoxProductTypeProduct;
		private System.Windows.Forms.GroupBox groupBoxProductPrice;
		private System.Windows.Forms.GroupBox groupBoxProductName;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanelProduct;
		private System.Windows.Forms.Button importProduct;
		private System.Windows.Forms.ComboBox textTypeProduct;
		private System.Windows.Forms.NumericUpDown textPriceProduct;
		private System.Windows.Forms.NumericUpDown textQuantityProduct;
		private System.Windows.Forms.TextBox textNameProduct;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
		private System.Windows.Forms.Button addCocktail;
		private System.Windows.Forms.Button editCocktail;
		private System.Windows.Forms.Button deleteCocktail;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.TabPage tabOrders;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
		private System.Windows.Forms.Label quantityTypeLabel;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Button completeOrder;
		private System.Windows.Forms.Button inQueue;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox comboBoxSearch;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.DataGridView dataGridViewListCocktails;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.Button reloadData;
		private System.Windows.Forms.Label userLabel;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
		private System.Windows.Forms.Panel panel7;
		private System.Windows.Forms.Label currentCocktailLabel;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
		private System.Windows.Forms.Button undoOrder;
		private System.Windows.Forms.Button infoOrder;
		private System.Windows.Forms.DataGridView dataGridViewQueue;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
		private System.Windows.Forms.DataGridView dataGridViewListBarmen;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
		private System.Windows.Forms.GroupBox groupBoxBarmanSkill;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel31;
		private System.Windows.Forms.GroupBox groupBoxBarmanTip;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
		private System.Windows.Forms.GroupBox groupBoxBarmanSalary;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
		private System.Windows.Forms.NumericUpDown textBarmanTip;
		private System.Windows.Forms.GroupBox groupBoxBarmanPassword;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
		private System.Windows.Forms.NumericUpDown textBarmanSalary;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel29;
		private System.Windows.Forms.Button newBarman;
		private System.Windows.Forms.Button deleteBarman;
		private System.Windows.Forms.GroupBox groupBoxBarmanName;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel30;
		private System.Windows.Forms.TextBox textBarmanName;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
		private System.Windows.Forms.Button saveBarman;
		private System.Windows.Forms.Button undoBarman;
		private System.Windows.Forms.TextBox textBarmanPassword;
		private System.Windows.Forms.NumericUpDown textCountOrders;
		private System.Windows.Forms.NumericUpDown textBarmanSkill;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvBarmanId;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvBarmanName;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvBarmanPassword;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvBarmanSalary;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvBarmanTip;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvBarmanSkill;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvTypeProductId;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvTypeProductName;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvTypeProductUnit;
		private System.Windows.Forms.Label descriptionLabel;
		private System.Windows.Forms.Label processLabel;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvCocktailsListId;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvCocktailsListName;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvCocktailsListPrice;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvCocktailsListPopularity;
		private System.Windows.Forms.Button saveProduct;
		private System.Windows.Forms.Button undoProduct;
		private System.Windows.Forms.ToolTip toolTipForm;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvProductId;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvProductTypeProductId;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvProductName;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvProductType;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvProductQuantity;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvProductPrice;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvTareId;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvTareType;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvTareQuantity;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvTarePrice;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvTareReloading;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvCocktailsId;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvCocktailsName;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvOrderId;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvOrderName;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvOrderPrice;

	}
}

