﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Linq;
using System.IO;
using System.Xml.Linq;

namespace Cocktails
{
	public partial class CocktailsForm : Form
	{

		#region Variables

			#region general var

		DataBaseWork database;

		int userId = 0;
		int barmanSkill = 3; // уровень мастерства бармена от 1 до 3

		bool isEndLoading = false;

		#endregion

			#region Cocktails var

		public static ProcessingCocktail currentCocktail;

		Dictionary<int, Dictionary<int, decimal>> productReplaceList = 
			new Dictionary<int, Dictionary<int, decimal>>();
		Dictionary<int, Dictionary<int, int>> tareReplaceList =
			new Dictionary<int, Dictionary<int, int>>();

		int currentCocktailId = 0;

		#endregion

			#region barman var

		int idEditBarman = 0;
		string editNameBarman = String.Empty;
		bool isBarmanAddState = false;

		#endregion

			#region Products var

		int idEditProduct = 0;
		string editTypeProductProduct = String.Empty;
		string editNameProduct = String.Empty;
		bool isProductAddState = false;

		#endregion

			#region TypeProduct var



		#endregion

			#region Tare var



		#endregion

		#endregion

		#region Initialization

		public CocktailsForm()
		{
			InitializeComponent();
		}

		private void CoctailsForm_Load(object sender, EventArgs e)
		{
			database = new DataBaseWork();
			AuthorizationForm authorizationForm = new AuthorizationForm(database.GetDataContext());
			authorizationForm.ShowDialog();
			if (authorizationForm.DialogResult == System.Windows.Forms.DialogResult.OK)
			{
				userId = authorizationForm.GetUserId();
				if (authorizationForm.IsWarehouseman())
				{
					userLabel.Text = "Пользователь Владелец склада: " + database.GetUserWarehouseman(userId).name;
				}
				else
				{
					panelEditProducts.Enabled = false;
					panelEditTares.Enabled = false;
					panelEditTypeProducts.Enabled = false;
					tabControl1.TabPages.RemoveAt(4);
					barmanSkill = database.GetUserBarman(userId).skill;
					userLabel.Text = "Пользователь Бармен: " + database.GetUserBarman(userId).name;
				}
			}
			else
			{
				this.Close();
			}
			ReloadDataFromDatabase();
		}

		#endregion

		#region GeneralFunc

		private void ReloadDataFromDatabase()
		{
			ReloadBarmanData();
			ReloadProductsData();
			ReloadTypeProductsData();
			ReloadTareData();
			ReloadRecipeData();
		}

		static public void ShowMessage(string message)
		{
			string caption = "Предупреждение!";
			MessageBox.Show(message, caption, MessageBoxButtons.OK);
		}

		static public DialogResult ShowMessageQuestion(string message)
		{
			string caption = "Внимание!";
			return MessageBox.Show(message, caption, MessageBoxButtons.YesNo);
		}

		private void reloadData_Click(object sender, EventArgs e)
		{
			ReloadDataFromDatabase();
		}

		#endregion

		#region Coctails

		private void addCocktail_Click(object sender, EventArgs e)
		{
			AddCocktailForm addCocktailForm = new AddCocktailForm(database);
			addCocktailForm.ShowDialog();
		}

		private void editCocktail_Click(object sender, EventArgs e)
		{

		}

		private void deleteCocktail_Click(object sender, EventArgs e)
		{

		}

		private void ReloadRecipeData()
		{
			isEndLoading = false;
			List<Recipe> recipeList = database.GetRecipeList();
			int numberRecipe = 0;
			dataGridViewCocktails.Rows.Clear();
			dataGridViewListCocktails.Rows.Clear();
			foreach (Recipe recipe in recipeList)
			{
				dataGridViewCocktails.Rows.Add();
				dataGridViewListCocktails.Rows.Add();

				dataGridViewCocktails.Rows[numberRecipe].Cells[dgvCocktailsId.Name].Value = recipe.Id;
				dataGridViewCocktails.Rows[numberRecipe].Cells[dgvCocktailsName.Name].Value =
					recipe.name.TrimEnd();
				dataGridViewListCocktails.Rows[numberRecipe].Cells[dgvCocktailsListId.Name].Value = recipe.Id;
				dataGridViewListCocktails.Rows[numberRecipe].Cells[dgvCocktailsListName.Name].Value =
					recipe.name.TrimEnd();
				dataGridViewListCocktails.Rows[numberRecipe].Cells[dgvCocktailsListPopularity.Name].Value =
					recipe.popularity;
				dataGridViewListCocktails.Rows[numberRecipe].Cells[dgvCocktailsListPrice.Name].Value =
					decimal.Round((new ProcessingCocktail(recipe.Id).GetCocktailPrice()), 2);

				numberRecipe++;
			}
			if (recipeList.Count == 0)
			{
				SetEnabledRecipeElements(false);
			}
			else
			{
				SetEnabledRecipeElements(true);
			}
			isEndLoading = true;
		}

		private void SetEnabledRecipeElements(bool bit)
		{
			editCocktail.Enabled = bit;
			deleteCocktail.Enabled = bit;
		}

		private void dataGridViewCocktails_SelectionChanged(object sender, EventArgs e)
		{
			if (dataGridViewCocktails.SelectedRows.Count != 0 && isEndLoading == true)
			{
				ProcessingCocktail processingCocktail = new ProcessingCocktail(
					(int)dataGridViewCocktails.SelectedRows[0].Cells[dgvCocktailsId.Name].Value);
				processLabel.Text = processingCocktail.GetRecipeString() + "\n\n"
					+ processingCocktail.GetCocktailPriceString();
				descriptionLabel.Text = processingCocktail.GetCocktailName() + "\n\n" 
					+ processingCocktail.GetFullDiscription();
			}
		}

		private void completeOrder_Click(object sender, EventArgs e)
		{
			//currentCocktail = new ProcessingCocktail(currentCocktailId);
			if (currentCocktailId != 0)
			{
				currentCocktail = new ProcessingCocktail(currentCocktailId, productReplaceList, tareReplaceList);
				foreach (int productId in currentCocktail.GetPrepareProductToReplace())
				{
					ReplaceElementForm replaceForm = new ReplaceElementForm(typeof(Product), productId);
					replaceForm.ShowDialog();
					if (replaceForm.DialogResult == System.Windows.Forms.DialogResult.OK)
					{

					}
					else
					{
						Dictionary<int, decimal> temp = new Dictionary<int, decimal>();
						temp.Add(0, 0.0M);
						productReplaceList.Add(productId, temp);
					}
				}
				TransparentForm doOrderForm = new TransparentForm();
				doOrderForm.ShowDialog();
			}
		}

		#endregion

		#region Barman

		private void ReloadBarmanData() // Added Barman to table
		{
			List<BarmanClass> barmanList = database.GetBarmanList();
			int numberBarman = 0;
			dataGridViewListBarmen.Rows.Clear();
			foreach (BarmanClass barman in barmanList)
			{
				dataGridViewListBarmen.Rows.Add();
				dataGridViewListBarmen.Rows[numberBarman].Cells[dgvBarmanId.Name].Value = barman.Id;
				dataGridViewListBarmen.Rows[numberBarman].Cells[dgvBarmanName.Name].Value = barman.name.Trim();
				dataGridViewListBarmen.Rows[numberBarman].Cells[dgvBarmanPassword.Name].Value = barman.password.TrimEnd();
				dataGridViewListBarmen.Rows[numberBarman].Cells[dgvBarmanSalary.Name].Value = barman.salary;
				dataGridViewListBarmen.Rows[numberBarman].Cells[dgvBarmanSkill.Name].Value = barman.skill;
				dataGridViewListBarmen.Rows[numberBarman].Cells[dgvBarmanTip.Name].Value = barman.tip;
				numberBarman++;
			}
			if (barmanList.Count == 0)
			{
				SetEnabledBarmanElements(false);
			}
		}

		private void SetEnabledBarmanElements(bool bit)
		{
			groupBoxBarmanName.Enabled = bit;
			groupBoxBarmanPassword.Enabled = bit;
			groupBoxBarmanSalary.Enabled = bit;
			groupBoxBarmanSkill.Enabled = bit;
			groupBoxBarmanTip.Enabled = bit;
			undoBarman.Enabled = bit;
			saveBarman.Enabled = bit;
			deleteBarman.Enabled = bit;
		}

		private void BarmanDataFromTableToForm(int rowIndex)
		{
			textBarmanName.Text =
				((string)dataGridViewListBarmen[dgvBarmanName.Name, rowIndex].Value).Trim();
			textBarmanPassword.Text =
				((string)dataGridViewListBarmen[dgvBarmanPassword.Name, rowIndex].Value).Trim();
			textBarmanSalary.Value = (decimal)dataGridViewListBarmen[dgvBarmanSalary.Name, rowIndex].Value;
			textBarmanSkill.Value = (int)dataGridViewListBarmen[dgvBarmanSkill.Name, rowIndex].Value;
			textBarmanTip.Value = (decimal)dataGridViewListBarmen[dgvBarmanTip.Name, rowIndex].Value;
		}

		private void dataGridViewListBarmen_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex >= 0)
			{
				idEditBarman = (int)dataGridViewListBarmen[dgvBarmanId.Name, e.RowIndex].Value;
				SetEnabledBarmanElements(true);
				isBarmanAddState = false;
				BarmanDataFromTableToForm(e.RowIndex);
				undoBarman.Text = "Сброс";
				saveBarman.Text = "Сохранить изменения";
			}
		}

		private void newBarman_Click(object sender, EventArgs e)
		{
			idEditBarman = 0;
			isBarmanAddState = true;
			textBarmanName.Text = string.Empty;
			textBarmanPassword.Text = string.Empty;
			textBarmanSalary.Value = textBarmanSalary.Minimum;
			textBarmanTip.Value = textBarmanTip.Minimum;
			textBarmanSkill.Value = textBarmanSkill.Maximum;
			undoBarman.Text = "Отмена создания";
			saveBarman.Text = "Добавить бармена";
			SetEnabledBarmanElements(true);
		}

		private bool IsFullBarmanForm()
		{
			return (
				(textBarmanName.Text.Length != 0) &&
				(textBarmanPassword.Text.Length != 0));
		}

		private BarmanClass GetBarmanFromForm()
		{
			BarmanClass barman = new BarmanClass();
			barman.name = textBarmanName.Text;
			barman.password = textBarmanPassword.Text;
			barman.salary = textBarmanSalary.Value;
			barman.tip = textBarmanTip.Value;
			barman.skill = (int)textBarmanSkill.Value;
			return barman;
		}

		private void saveBarman_Click(object sender, EventArgs e)
		{
			if (IsFullBarmanForm())
			{
				if (isBarmanAddState == true)
				{
					if (database.IsExistBarmenNameInDatabase(textBarmanName.Text) == false)
					{
						database.AddBarmanToDatabase(GetBarmanFromForm());
					}
					else
					{
						ShowMessage("Такой логин уже существует!");
					}
				}
				else
				{
					if (database.IsExistBarmenNameInDatabase(textBarmanName.Text) == false)
					{
						BarmanClass barman = GetBarmanFromForm();
						barman.Id = idEditBarman;
						database.UpdateBarmanToDatabase(barman);
					}
					else
					{
						ShowMessage("Такой логин уже существует!");
					}
				}
				ReloadBarmanData();
			}
			else
			{
				ShowMessage("Не все поля заполнены!");
			}
		}

		private void undoBarman_Click(object sender, EventArgs e)
		{
			if (isBarmanAddState == false)
			{
				BarmanDataFromTableToForm(dataGridViewListBarmen.SelectedRows[0].Index);
			}
		}

		private void deleteBarman_Click(object sender, EventArgs e)
		{
			if (isBarmanAddState == false)
			{
				BarmanClass barman = GetBarmanFromForm();
				barman.Id = idEditBarman;
				database.DeleteBarmanInDatabase(barman);
				ReloadBarmanData();
			}
		}

		#endregion

		#region Products

		private void ReloadProductsData()
		{
			isEndLoading = false;
			List<Product> productsList = database.GetProductsList();
			int numberProduct = 0;
			dataGridViewProducts.Rows.Clear();
			foreach (Product product in productsList)
			{
				dataGridViewProducts.Rows.Add();
				dataGridViewProducts.Rows[numberProduct].Cells[dgvProductId.Name].Value = product.Id;
				dataGridViewProducts.Rows[numberProduct].Cells[dgvProductName.Name].Value = product.name.Trim();
				dataGridViewProducts.Rows[numberProduct].Cells[dgvProductPrice.Name].Value = product.price;
				dataGridViewProducts.Rows[numberProduct].Cells[dgvProductQuantity.Name].Value = 
					product.quantity + " " + product.TypeProduct.unit.Trim();
				dataGridViewProducts.Rows[numberProduct].Cells[dgvProductType.Name].Value = 
					product.TypeProduct.name.Trim();
				dataGridViewProducts.Rows[numberProduct].Cells[dgvProductTypeProductId.Name].Value = 
					product.typeId;
				textTypeProduct.Items.Clear();
				foreach (TypeProduct typeProduct in database.GetTypeProductsList())
				{
					textTypeProduct.Items.Add(typeProduct.name.Trim());
				}
				textTypeProduct.Text = String.Empty;
				numberProduct++;
			}
			if (productsList.Count == 0)
			{
				SetEnabledProductElements(false);
			}
			isEndLoading = true;
		}

		private void SetEnabledProductElements(bool bit)
		{
			groupBoxProductName.Enabled = bit;
			groupBoxProductPrice.Enabled = bit;
			groupBoxProductQuantity.Enabled = bit;
			groupBoxProductTypeProduct.Enabled = bit;
			addProduct.Enabled = bit;
			deleteProduct.Enabled = bit;
			saveProduct.Enabled = bit;
			undoProduct.Enabled = bit;
		}

		private void ProductDataFromTableToForm(int rowIndex)
		{
			editNameProduct = textNameProduct.Text =
				((string)dataGridViewProducts[dgvProductName.Name, rowIndex].Value).Trim();
			textPriceProduct.Value =
				(decimal)dataGridViewProducts[dgvProductPrice.Name, rowIndex].Value;
			TypeProduct typeProduct = database.GetTypeProduct(
					(int)dataGridViewProducts[dgvProductTypeProductId.Name, rowIndex].Value);
			textTypeProduct.SelectedItem = 
				editTypeProductProduct = typeProduct.name.Trim();

			string[] mas = ((string)dataGridViewProducts[dgvProductQuantity.Name, rowIndex].Value).Split(new char[] { ' ' }, 2);
			textQuantityProduct.Value = decimal.Parse(mas[0]);
		}

		public void SetProductInTable(int rowIndex)
		{
			if (rowIndex >= 0)
			{
				idEditProduct = (int)dataGridViewProducts[dgvProductId.Name, rowIndex].Value;
				SetEnabledProductElements(true);
				isProductAddState = false;
				ProductDataFromTableToForm(rowIndex);
				undoProduct.Text = "Сброс";
				saveProduct.Text = "Сохранить изменения";
			}
		}

		private void dataGridViewProducts_SelectionChanged(object sender, EventArgs e)
		{
			if (dataGridViewProducts.SelectedRows.Count != 0 && isEndLoading == true)
			{
				SetProductInTable(dataGridViewProducts.SelectedRows[0].Index);
			}
		}

		private Product GetProductFromForm()
		{
			Product product = new Product();
			product.name = textNameProduct.Text;
			product.typeId = database.GetTypeProductId(textTypeProduct.SelectedItem.ToString());
			product.quantity = textQuantityProduct.Value;
			product.price = textPriceProduct.Value;
			return product;
		}

		private void deleteProduct_Click(object sender, EventArgs e)
		{
			if (isProductAddState == false)
			{
				Product product = GetProductFromForm();
				product.Id = idEditProduct;
				database.DeleteProductInDatabase(product);
				ReloadProductsData();
			}
		}

		private void addProduct_Click(object sender, EventArgs e)
		{
			idEditProduct = 0;
			isProductAddState = true;
			textNameProduct.Text = string.Empty;
			textTypeProduct.SelectedIndex = -1;
			textPriceProduct.Value = textPriceProduct.Minimum;
			textQuantityProduct.Value = textQuantityProduct.Minimum;
			undoProduct.Text = "Отмена создания";
			saveProduct.Text = "Добавить продукт";
			SetEnabledProductElements(true);
		}

		private void undoProduct_Click(object sender, EventArgs e)
		{
			if (isProductAddState == false)
			{
				ProductDataFromTableToForm(dataGridViewProducts.SelectedRows[0].Index);
			}
			else
			{
				if (dataGridViewProducts.SelectedRows.Count != 0)
				{
					SetProductInTable(dataGridViewProducts.SelectedRows[0].Index);
				}
				else
				{
					SetEnabledProductElements(false);
				}
			}
		}

		private void importProduct_Click(object sender, EventArgs e)
		{
			// Form for import products in CSV files
		}

		private bool IsFullProductForm()
		{
			return (
				(textNameProduct.Text.Length != 0) &&
				(textTypeProduct.SelectedIndex != -1));
		}

		private void saveProduct_Click(object sender, EventArgs e)
		{
			if (IsFullProductForm())
			{
				if (isProductAddState == true)
				{
					if (database.IsExistProductInDatabase(GetProductFromForm()) == false)
					{
						database.AddProductToDatabase(GetProductFromForm());
					}
					else
					{
						ShowMessage("Такой продукт уже существует!");
					}
				}
				else
				{
					if (editNameProduct.ToLower() != textNameProduct.Text.ToLower() ||
						editTypeProductProduct != textTypeProduct.Text)
					{
						if (database.IsExistProductInDatabase(GetProductFromForm()) == false)
						{
							Product product = GetProductFromForm();
							product.Id = idEditProduct;
							database.UpdateProductToDatabase(product);
						}
						else
						{
							ShowMessage("Такой продукт уже существует!");
						}
					}
					else
					{
						Product product = GetProductFromForm();
						product.Id = idEditProduct;
						database.UpdateProductToDatabase(product);
					}
				}
				ReloadProductsData();
			}
			else
			{
				ShowMessage("Не все поля заполнены!");
			}
		}

		private void textTypeProduct_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (textTypeProduct.SelectedIndex != -1)
			{
				quantityTypeLabel.Text =
					database.GetTypeProduct(textTypeProduct.SelectedItem.ToString()).unit.Trim();
			}
		}

		#endregion

		#region TypeProduct

		private void ReloadTypeProductsData()
		{
			List<TypeProduct> typeProductsList = database.GetTypeProductsList();
			int numberTypeProduct = 0;
			dataGridViewTypeProducts.Rows.Clear();
			foreach (TypeProduct typeProduct in typeProductsList)
			{
				dataGridViewTypeProducts.Rows.Add();

				dataGridViewTypeProducts.Rows[numberTypeProduct].Cells[dgvTypeProductId.Name].Value =
					typeProduct.Id;
				dataGridViewTypeProducts.Rows[numberTypeProduct].Cells[dgvTypeProductName.Name].Value =
					typeProduct.name.Trim();
				dataGridViewTypeProducts.Rows[numberTypeProduct].Cells[dgvTypeProductUnit.Name].Value =
					typeProduct.unit.Trim();
				numberTypeProduct++;
			}
			if (typeProductsList.Count == 0)
			{
				SetEnabledTypeProductElements(false);
			}
			else
			{
				SetEnabledTypeProductElements(true);
			}
		}

		private void SetEnabledTypeProductElements(bool bit)
		{
			editType.Enabled = bit;
			deleteType.Enabled = bit;
		}

		private void addType_Click(object sender, EventArgs e)
		{
			AddTypeProductForm addTypeProductForm =
				new AddTypeProductForm(database);
			addTypeProductForm.ShowDialog();
			if (addTypeProductForm.DialogResult == System.Windows.Forms.DialogResult.OK)
			{
				ReloadTypeProductsData();
				ReloadProductsData();
			}
		}

		private void editType_Click(object sender, EventArgs e)
		{
			if (dataGridViewTypeProducts.SelectedRows.Count != 0)
			{
				int indexTypeProduct = dataGridViewTypeProducts.SelectedRows[0].Index;

				String tempNameTypeProduct = dataGridViewTypeProducts[dgvTypeProductName.Name,
					indexTypeProduct].Value.ToString();
				String tempUnitTypeProduct = dataGridViewTypeProducts[dgvTypeProductUnit.Name,
					indexTypeProduct].Value.ToString();

				AddTypeProductForm addTypeProductForm =
					new AddTypeProductForm(database,
						tempNameTypeProduct,
						tempUnitTypeProduct,
						(int)dataGridViewTypeProducts[
							dgvTypeProductId.Name, indexTypeProduct].Value,
						false); // disable Add mode, enable Edit mode
				addTypeProductForm.ShowDialog();
				if (addTypeProductForm.DialogResult == System.Windows.Forms.DialogResult.OK)
				{
					ReloadTypeProductsData();
					ReloadProductsData();
				}
			}
		}

		private void deleteType_Click(object sender, EventArgs e)
		{
			if (dataGridViewTypeProducts.SelectedRows.Count != 0)
			{
				TypeProduct typeProduct = new TypeProduct();

				int indexTypeProduct = dataGridViewTypeProducts.SelectedRows[0].Index;

				typeProduct.Id = (int)dataGridViewTypeProducts[dgvTypeProductId.Name,
					indexTypeProduct].Value;
				typeProduct.name = dataGridViewTypeProducts[dgvTypeProductName.Name,
					indexTypeProduct].Value.ToString();
				typeProduct.unit = dataGridViewTypeProducts[dgvTypeProductUnit.Name,
					indexTypeProduct].Value.ToString();
				database.DeleteTypeProductInDatabase(typeProduct);
				ReloadTypeProductsData();
				ReloadProductsData();
			}
		}

		private void dataGridViewTypeProducts_CellClick(object sender, DataGridViewCellEventArgs e)
		{

		}

		#endregion

		#region Tare

		private void ReloadTareData()
		{
			List<Tare> tareList = database.GetTareList();
			int numberTare = 0;
			dataGridViewTare.Rows.Clear();
			foreach (Tare tare in tareList)
			{
				dataGridViewTare.Rows.Add();

				dataGridViewTare.Rows[numberTare].Cells[dgvTareId.Name].Value = tare.Id;
				dataGridViewTare.Rows[numberTare].Cells[dgvTareType.Name].Value = tare.type.TrimEnd();
				dataGridViewTare.Rows[numberTare].Cells[dgvTarePrice.Name].Value = tare.price;
				dataGridViewTare.Rows[numberTare].Cells[dgvTareQuantity.Name].Value = tare.quantity;
				dataGridViewTare.Rows[numberTare].Cells[dgvTareReloading.Name].Value = tare.reloading;
				numberTare++;
			}
			if (tareList.Count == 0)
			{
				SetEnabledTareElements(false);
			}
			else
			{
				SetEnabledTareElements(true);
			}
		}

		private void SetEnabledTareElements(bool bit)
		{
			editTare.Enabled = bit;
			deleteTare.Enabled = bit;
		}

		private void addTare_Click(object sender, EventArgs e)
		{
			AddTareForm addTareForm = new AddTareForm(database);
			addTareForm.ShowDialog();
			if (addTareForm.DialogResult == System.Windows.Forms.DialogResult.OK)
			{
				ReloadTareData();
			}
		}

		private void editTare_Click(object sender, EventArgs e)
		{
			if (dataGridViewTare.SelectedRows.Count != 0)
			{
				int indexTare = dataGridViewTare.SelectedRows[0].Index;

				String tempTypeTare = dataGridViewTare[dgvTareType.Name,
					indexTare].Value.ToString();
				decimal tempPriceTare = (decimal)dataGridViewTare[dgvTarePrice.Name,
					indexTare].Value;
				int tempReloadingTare = (int)dataGridViewTare[dgvTareReloading.Name,
					indexTare].Value;
				int tempQuantityTare = (int)dataGridViewTare[dgvTareQuantity.Name,
					indexTare].Value;

				AddTareForm addTareForm =
					new AddTareForm(database,
						tempTypeTare,
						tempPriceTare,
						tempQuantityTare,
						tempReloadingTare,
						(int)dataGridViewTare[dgvTareId.Name, indexTare].Value,
						false); // disable Add mode, enable Edit mode
				addTareForm.ShowDialog();
				if (addTareForm.DialogResult == System.Windows.Forms.DialogResult.OK)
				{
					ReloadTareData();
				}
			}
		}

		private void deleteTare_Click(object sender, EventArgs e)
		{
			if (dataGridViewTare.SelectedRows.Count != 0)
			{
				Tare tare = new Tare();

				int indexTare = dataGridViewTare.SelectedRows[0].Index;

				tare.price = (decimal)dataGridViewTare[dgvTarePrice.Name, indexTare].Value;
				tare.quantity = (int)dataGridViewTare[dgvTareQuantity.Name, indexTare].Value;
				tare.reloading = (int)dataGridViewTare[dgvTareReloading.Name, indexTare].Value;
				tare.type = dataGridViewTare[dgvTareType.Name, indexTare].Value.ToString();

				database.DeleteTareInDatabase(tare);
				ReloadTareData();
			}
		}

		#endregion

		private void AddCocktailToQueue()
		{
			if (true)
			{
				if (dataGridViewListCocktails.SelectedRows.Count != 0)
				{
					dataGridViewQueue.Rows.Add(
							dataGridViewListCocktails.SelectedRows[0].Cells[dgvCocktailsListId.Name].Value,
							dataGridViewListCocktails.SelectedRows[0].Cells[dgvCocktailsListName.Name].Value,
							decimal.Parse(dataGridViewListCocktails.SelectedRows[0].Cells[
								dgvCocktailsListPrice.Name].Value.ToString())
						);
					dataGridViewQueue.Sort(
						dataGridViewQueue.Columns[dgvOrderPrice.Name], ListSortDirection.Descending);
					currentCocktailId = (int)dataGridViewQueue.Rows[0].Cells[dgvOrderId.Name].Value;
					currentCocktailLabel.Text =
						dataGridViewQueue.Rows[0].Cells[dgvOrderName.Name].Value.ToString();
					dataGridViewQueue.ClearSelection();
				}
			}
			else
			{

			}
		}

		private void dataGridViewListCocktails_CellClick(object sender, DataGridViewCellEventArgs e)
		{

		}

		private void dataGridViewListCocktails_SelectionChanged(object sender, EventArgs e)
		{

		}

		private void inQueue_Click(object sender, EventArgs e)
		{
			AddCocktailToQueue();
		}

		private void dataGridViewQueue_CellClick(object sender, DataGridViewCellEventArgs e)
		{

		}

		private void dataGridViewQueue_SelectionChanged(object sender, EventArgs e)
		{
			if (dataGridViewQueue.SelectedRows.Count == 0)
			{
				undoOrder.Enabled = false;
				infoOrder.Enabled = false;
			}
			else
			{
				undoOrder.Enabled = true;
				infoOrder.Enabled = true;
			}
		}

	}
}