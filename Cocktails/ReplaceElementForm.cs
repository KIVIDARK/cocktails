﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cocktails
{
	public partial class ReplaceElementForm : Form
	{
		DataBaseWork database = new DataBaseWork();

		Type type;

		int elemId = 0;

		public ReplaceElementForm(Type type, int elemId)
		{
			InitializeComponent();
			this.type = type;
			this.elemId = elemId;
		}

		private void buttonExecute_Click(object sender, EventArgs e)
		{

		}

		private void AddElementToReady()
		{
			if (dataGridViewListElements.SelectedRows.Count != 0)
			{
				if (type == typeof(Product))
				{
					dataGridViewReadyElements.Rows.Add(
						dataGridViewListElements.SelectedRows[0].Cells["dgvProductListId"].Value,
						dataGridViewListElements.SelectedRows[0].Cells["dgvProductListName"].Value,
						dataGridViewListElements.SelectedRows[0].Cells["dgvProductListType"].Value,
						dataGridViewListElements.SelectedRows[0].Cells["dgvProductListPrice"].Value,
						dataGridViewListElements.SelectedRows[0].Cells["dgvProductListQuantity"].Value
						);
				}
			}
		}

		private void buttonAddElement_Click(object sender, EventArgs e)
		{

		}

		private void buttonDeleteElement_Click(object sender, EventArgs e)
		{

		}

		private void dataGridViewListElements_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{

		}

		private void dataGridViewReadyElements_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{

		}

		private void LoadDataToTable()
		{
			if (type == typeof(Product))
			{
				Product currentProduct = database.GetProduct(elemId);
				labelMissingElement.Text = "Отсутствующий продукт: "
					+ currentProduct.TypeProduct.name + " " + currentProduct.name;
				this.Name = "Замена отсутствуещего продукта | Коктейль : " +
					CocktailsForm.currentCocktail.GetCocktailName();
				List<Product> productList = database.GetProductsList(currentProduct.typeId);
				int currentRow = 0;

				dataGridViewListElements.Rows.Clear();
				foreach (Product product in productList)
				{
					dataGridViewListElements.Rows.Add();
					dataGridViewListElements.Rows[currentRow].Cells["dgvProductListId"].Value =
						product.Id;
					dataGridViewListElements.Rows[currentRow].Cells["dgvProductListName"].Value =
						product.name.TrimEnd();
					dataGridViewListElements.Rows[currentRow].Cells["dgvProductListType"].Value =
						product.TypeProduct.name.TrimEnd();
					dataGridViewListElements.Rows[currentRow].Cells["dgvProductListPrice"].Value =
						product.price;
					dataGridViewListElements.Rows[currentRow].Cells["dgvProductListQuantity"].Value =
						product.quantity.ToString() + " " + product.TypeProduct.unit.TrimEnd();
					currentRow++;
				}
			}
		}

		private void ReplaceElementForm_Load(object sender, EventArgs e)
		{
			if (type == typeof(Product))
			{
				dataGridViewListElements.Columns.Add("dgvProductListId", "id");
				dataGridViewListElements.Columns.Add("dgvProductListName", "Название");
				dataGridViewListElements.Columns.Add("dgvProductListType", "Тип");
				dataGridViewListElements.Columns.Add("dgvProductListPrice", "Цена");
				dataGridViewListElements.Columns.Add("dgvProductListQuantity", "Количество");
				dataGridViewListElements.Columns["dgvProductListId"].Visible = false;
				dataGridViewListElements.Columns["dgvProductListPrice"].Visible = false;

				dataGridViewReadyElements.Columns.Add("dgvProductReadyId", "id");
				dataGridViewReadyElements.Columns.Add("dgvProductReadyName", "Название");
				dataGridViewReadyElements.Columns.Add("dgvProductReadyType", "Тип");
				dataGridViewReadyElements.Columns.Add("dgvProductReadyPrice", "Цена");
				dataGridViewReadyElements.Columns.Add("dgvProductReadyQuantity", "Количество");
				dataGridViewReadyElements.Columns["dgvProductReadyId"].Visible = false;
				dataGridViewReadyElements.Columns["dgvProductReadyPrice"].Visible = false;
				dataGridViewReadyElements.Columns["dgvProductReadyQuantity"].ReadOnly = false;
			}
		}
	}
}
