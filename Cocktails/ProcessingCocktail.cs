﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Cocktails
{
	public class ProcessingCocktail
	{

		#region Variable

		int idCocktail = 0;

		DataBaseWork database = new DataBaseWork();

		Dictionary<int, Dictionary<int, decimal>> productReplaceList;
		Dictionary<int, Dictionary<int, int>> tareReplaceList;

		List<Product> listProducts = new List<Product>();
		List<Tare> listTare = new List<Tare>();
		List<int> prepareProductToReplace = new List<int>();
		List<int> prepareTareToReplace = new List<int>();

		Recipe recipe;

		decimal priceCocktail = 0.0M;

		#endregion

		#region LoadCocktail

		private void LoadBaseDataIdentifyMissing()
		{
			recipe = database.GetRecipe(idCocktail);
			XElement xMainElement = recipe.process;
			Product product;
			decimal productQuantity = 0.0M;
			foreach (XElement xElement in xMainElement.Element("products").Elements("product"))
			{
				product = database.GetProduct(int.Parse(xElement.Attribute("id").Value));
				productQuantity = decimal.Parse(xElement.Attribute("quantity").Value);
				if (product.quantity < productQuantity)
				{
					if (productReplaceList.ContainsKey(product.Id) == false)
					{
						prepareProductToReplace.Add(product.Id);
						priceCocktail += productQuantity * product.price;
					}
					else
					{
						Product tempProduct;
						int counter = 0;
						decimal addedPrice = 0.0M;
						foreach (KeyValuePair<int, decimal> repProduct in productReplaceList[product.Id])
						{
							tempProduct = database.GetProduct(repProduct.Key);
							if (tempProduct.quantity < repProduct.Value)
							{
								prepareProductToReplace.Add(product.Id);
								productReplaceList.Remove(product.Id);
								if (product.price != 0)
								{
									priceCocktail += productQuantity * product.price;
								}
								listProducts.RemoveRange(listProducts.Count - (counter + 1), counter);
								break;
							}
							else
							{
								if (repProduct.Key != 0)
								{
									tempProduct.quantity = repProduct.Value;
									if (tempProduct.price != 0)
									{
										addedPrice += tempProduct.quantity * tempProduct.price;
									}
									listProducts.Add(tempProduct);
								}
							}
						}
						priceCocktail += addedPrice;
					}
				}
				else
				{
					if (productReplaceList.ContainsKey(product.Id))
					{
						productReplaceList.Remove(product.Id);
					}
					product.quantity = productQuantity;
					if (product.price != 0)
					{
						priceCocktail += product.quantity * product.price;
					} 
					listProducts.Add(product);
				}
			}
			Tare tare;
			int tareQuantity = 0;
			foreach (XElement xElement in xMainElement.Element("tares").Elements("tare"))
			{
				tare = database.GetTare(int.Parse(xElement.Attribute("id").Value));
				tareQuantity = int.Parse(xElement.Attribute("quantity").Value);
				if (tare.quantity < tareQuantity)
				{
					if (tareReplaceList.ContainsKey(tare.Id) == false)
					{
						prepareTareToReplace.Add(tare.Id);
						priceCocktail += tareQuantity * tare.price;
					}
					else
					{
						Tare temptare;
						int counter = 0;
						decimal addedPrice = 0.0M;
						foreach (KeyValuePair<int, int> repTare in tareReplaceList[tare.Id])
						{
							temptare = database.GetTare(repTare.Key);
							if (temptare.quantity < repTare.Value)
							{
								prepareTareToReplace.Add(tare.Id);
								tareReplaceList.Remove(tare.Id);
								if (tare.price != 0)
								{
									priceCocktail += tareQuantity * tare.price;
								}
								listTare.RemoveRange(listTare.Count - (counter + 1), counter);
								break;
							}
							else
							{
								if (repTare.Key != 0)
								{
									temptare.quantity = repTare.Value;
									if (temptare.price != 0)
									{
										addedPrice += temptare.quantity * temptare.price;
									}
									listTare.Add(temptare);
								}
							}
						}
						priceCocktail += addedPrice;
					}
				}
				else
				{
					if (tareReplaceList.ContainsKey(tare.Id))
					{
						tareReplaceList.Remove(tare.Id);
					}
					tare.quantity = tareQuantity;
					if (tare.price != 0)
					{
						priceCocktail += tare.quantity * tare.price;
					}
					listTare.Add(tare);
				}
			}
			priceCocktail += recipe.additional_charges;
		}

		private void LoadBaseData()
		{
			recipe = database.GetRecipe(idCocktail);
			XElement xMainElement = recipe.process;
			Product product;
			foreach (XElement xElement in xMainElement.Element("products").Elements("product"))
			{
				product = database.GetProduct(int.Parse(xElement.Attribute("id").Value));
				product.quantity = decimal.Parse(xElement.Attribute("quantity").Value);
				if (product.price != 0)
				{
					priceCocktail += product.quantity * product.price;
				} 
				listProducts.Add(product);
			}
			Tare tare;
			foreach (XElement xElement in xMainElement.Element("tares").Elements("tare"))
			{
				tare = database.GetTare(int.Parse(xElement.Attribute("id").Value));
				tare.quantity = int.Parse(xElement.Attribute("quantity").Value);
				if (tare.price != 0)
				{
					priceCocktail += tare.price * tare.quantity;
				} 
				listTare.Add(tare);
			}
			priceCocktail += recipe.additional_charges;
		}

		public ProcessingCocktail(int idCocktail)
		{
			this.idCocktail = idCocktail;
			LoadBaseData();
		}

		public ProcessingCocktail(
			int idCocktail, 
			Dictionary<int, Dictionary<int, decimal>> productReplaceList,
			Dictionary<int, Dictionary<int, int>> tareReplaceList)
		{
			this.idCocktail = idCocktail;
			this.productReplaceList = productReplaceList;
			this.tareReplaceList = tareReplaceList;
			LoadBaseDataIdentifyMissing();
		}

		#endregion

		#region Functions

		public Dictionary<int, Dictionary<int, decimal>> GetProductReplaceList()
		{
			return productReplaceList;
		}

		public Dictionary<int, Dictionary<int, int>> GetTareReplaceList()
		{
			return tareReplaceList;
		}

		public List<int> GetPrepareProductToReplace()
		{
			return prepareProductToReplace;
		}

		public List<int> GetPrepareTareToReplace()
		{
			return prepareTareToReplace;
		}

		public string GetCocktailName()
		{
			return "Коктейль: " + recipe.name.TrimEnd();
		}

		public decimal GetCocktailPrice()
		{
			return priceCocktail;
		}

		public string GetCocktailPriceString()
		{
			return  "Цена: " + priceCocktail.ToString("#0.00");
		}

		public string GetRecipeString()
		{
			string recipeString = String.Empty;
			recipeString += "Продукты:\n\n";
			foreach (Product product in listProducts)
			{
				recipeString += product.TypeProduct.name.TrimEnd() + " ";
				recipeString += product.name.TrimEnd() + " - ";
				recipeString += product.quantity.ToString("#0.##") + " ";
				recipeString += product.TypeProduct.unit + "\n";
			}
			recipeString += "\n\nТара:\n\n";
			foreach (Tare tare in listTare)
			{
				recipeString += tare.type.TrimEnd() + " - ";
				recipeString += tare.quantity.ToString() + " штук\n";
			}
			return recipeString;
		}

		public string GetFullDiscription()
		{
			string result = String.Empty;
			result += recipe.description + "\n\n";
			result += "Выполнено заказов: " + recipe.popularity.ToString() + "\n\n";
			result += "Уровень сложности (1-3): " + recipe.complication.ToString();
			return result;
		}

		#endregion
	}
}
